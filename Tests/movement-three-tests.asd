(asdf:defsystem :movement-three-tests
  :depends-on (:movement-three :parachute)
  :serial t
  :components ((:file "package")
               (:file "query")
               (:file "tests")))
