(in-package :movement-three-tests)

(parachute:define-test movement-three)

(defun run-tests (&key (report 'parachute:plain) (mumble nil))
  (mt:reset-meters)
  (let ((mt::*mumble*
          (if mumble
              (lambda (ticks pass &rest info) (format *debug-io* "~&[~3@A ~12A] ~{~A~^~}" (or ticks "") pass info))
              nil)))
    (parachute:test 'movement-three :report report))
  (mt:print-meters))

(parachute:define-test smoke-tests
  :parent movement-three
  (parachute:finish (compile-and-optimise "(lambda (x) x)"))
  (parachute:finish (compile-and-optimise "(let ((y 42)) #'y)"))
  (parachute:finish (compile-and-optimise "#'(let (((loop1) (loop2)) ((loop2) (loop1))) (loop1))"))
  ;; This breaks the READ-SLOT of PHI rule as inlining LOOP produces
  ;; two mutually cyclic PHIs.
  (parachute:finish (compile-and-optimise "#'(let ((:mutable x 42) ((loop1 _) (loop2 x)) ((loop2 _) (loop1 x))) (loop1 x))")))

(parachute:define-test immutable-read-forwarding
  :parent movement-three
  (parachute:true (typep (returned-node "(lambda (x) x)") 'mt::argument))
  (parachute:true (typep (returned-node "(lambda (x) (let (((id y) y)) (id x)))") 'mt::argument)))

(parachute:define-test mutable-read-forwarding
  :parent movement-three
  ;; Through the mutable prototype made during construction.
  (parachute:true (returns-constant-p "#'(let ((c (class (new) (define :public x 42)))) (x: (new: c)))"))
  ;; Distinguish writes to X and Y.
  (parachute:true (returns-constant-p "#'(let ((c (class (new) (define :public x 42) (define :public y 123)))) (x: (new: c)))"))
  ;; Find the last write to X.
  (parachute:true (returns-constant-p "(lambda (a b) (let ((:mutable x a) (:mutable y b)) (set! x 42) (set! y a) y x))"))
  ;; Distinguish writes to different layouts.
  (parachute:true (returns-constant-p "(lambda (x) (let ((:mutable y 1)) (let ((:mutable z 2)) (set! z x) y)))")))

(parachute:define-test mutable-write-elimination
  :parent movement-three
  ;; The write of +UNBOUND+ should be eliminated.
  (let ((*stage* :higher))
    (parachute:is = 1 (length (instructions-of-type "(lambda (x) (object :mutable (define :mutable a x)))" 'mt::write-slot)))))

(parachute:define-test tail-calls
  :parent movement-three
  (let ((send-type '(or mt::known-send mt::unknown-send)))
    (parachute:true (endp (instructions-of-type "#'(loop (recur) (recur))" send-type)))
    (parachute:true (endp (instructions-of-type "#'(let (((loop1) (loop2) (loop1)) ((loop2) (loop2))) (loop2))" send-type)))))

(parachute:define-test safepoint-elimination
  :parent movement-three
  (let ((*stage* :higher))
    (expect-instruction-count mt::safepoint 1 "(lambda :rec () (value))")
    (expect-instruction-count mt::safepoint 1 "(lambda :rec (n) (if (<=: n 2) n (+: (value (-: n 1)) (value (-: n 2)))))")))

(parachute:define-test allocation-folding
  :parent movement-three
  ;; Allocation folding before scheduling this leads to broken
  ;; memory flow, leading to failure to schedule.
  (parachute:finish
   (compile-and-optimise
    "
(lambda (a b c)
  (let ((x (object :mutable (define :mutable y #f)))
        (y (if a b c)))
    (set! (y: x) y)
    x))")))

(parachute:define-test global-value-numbering
  :parent movement-three
  (expect-instruction-count mt::fixnum-binary-op 1 "(lambda (f x) (value: f (+: 1 x) (+: 1 x)))"))


(parachute:define-test test-elimination
  :parent movement-three
  (expect-instruction-count mt::fixnum-test 1 "(lambda (x) (if (<=: 0 x) (<=: 0 x) #f))")
  (expect-instruction-count mt::fixnum-test 1 "(lambda (x) (if (<=: 0 x) (>: x 0) #f))"))

(parachute:define-test write-barrier-elimination
  :parent movement-three
  (parachute:is = 1 (length (assembly-instructions-of-type "(object :mutable (define :mutable x 0) (define :mutable y 0) (define (value z) (set! x z) (when z (set! y z))))"
                                                           'movement-three.arm:write-barrier))))
