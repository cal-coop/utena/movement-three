(in-package :movement-three-tests)

(defvar *stage* :assembly)
(defvar *target* :arm)
(defun compile-and-optimise (code)
  (mt:compile (mt:parse-method-of code)
                          :stage *stage*
                          :target *target*))

(defun instructions-of-type (code type)
  (when (stringp code) (setf code (compile-and-optimise code)))
  (remove-if-not (lambda (n) (typep n type)) (mt::all-nodes code)))

(defmacro expect-instruction-count (type count code)
  `(parachute:is = ,count (length (instructions-of-type ,code ',type))))

(defun assembly-instructions-of-type (code type)
  (when (stringp code) (setf code (compile-and-optimise code)))
  (loop for b across (mt::cfg-blocks-by-schedule (mt::code-cfg code))
        appending (loop for i in (mt::basic-block-instructions b)
                        when (eq (mt:instruction-name i) type)
                          collect i)))

(defun returned-node (code)
  (destructuring-bind (inst)
      (instructions-of-type (compile-and-optimise code)
                            'mt::ret)
    (mt::ret-value inst)))

(defun constant-value-p (node)
  (typep node '(or mt::literal mt::literal-immediate)))
(defun returns-constant-p (code) (constant-value-p (returned-node code)))
