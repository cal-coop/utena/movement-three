(defpackage :movement-three-tests
  (:use :cl)
  (:export #:run-tests)
  (:local-nicknames (:mt #:movement-three)))
