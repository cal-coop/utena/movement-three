(in-package :movement-three)

;;; The obligatory meter-defining code.
;;; Somewhat simpler than the meters in Cleavir
;;; <https://github.com/s-expressionists/Cleavir/tree/main/Meter>

;;; Metering use counts.
(defvar *meters* '())
(defstruct (meter (:constructor nil)) name)
(defstruct (use-meter (:constructor %make-use-meter) (:include meter)) (count 0))
(defun make-meter (&rest name)
  (let ((m (%make-use-meter :name name)))
    (setf *meters* (remove name *meters* :test #'equal :key #'meter-name))
    (push m *meters*)
    m))

(defmacro define-meter (variable &rest name)
  `(defvar ,variable (make-meter ,@(loop for n in name collect `',n))))

(defun meter-tick (meter &optional (count 1)) (incf (use-meter-count meter) count))

;;; Metering time taken.
(defvar *time-meters* '())
(defstruct (time-meter (:constructor %make-time-meter) (:include meter)) (units 0))
(defun make-time-meter (&rest name)
  (let ((m (%make-time-meter :name name)))
    (setf *meters* (remove name *meters* :test #'equal :key #'meter-name))
    (push m *meters*)
    m))

(defmacro define-time-meter (variable &rest name)
  `(defvar ,variable (make-time-meter ,@(loop for n in name collect `',n))))

(defmacro with-timing ((meter) &body body)
  (alexandria:with-gensyms (start)
    `(let ((,start (get-internal-real-time)))
       (prog1 (progn ,@body)
         (incf (time-meter-units ,meter) (- (get-internal-real-time) ,start))))))

;;; Histogram meters (with logarithmic bucketing).
(defvar *histogram-meters* '())
(defstruct (histogram-meter (:constructor %make-histogram-meter) (:include meter))
  (total 0)
  (histogram (make-array 20 :element-type '(unsigned-byte 32) :initial-element 0)))
(defun make-histogram-meter (&rest name)
  (let ((m (%make-histogram-meter :name name)))
    (setf *meters* (remove name *meters* :test #'equal :key #'meter-name))
    (push m *meters*)
    m))

(defmacro define-histogram-meter (variable &rest name)
  `(defvar ,variable (make-histogram-meter ,@(loop for n in name collect `',n))))

(defun histogram-add (meter n)
  (incf (aref (histogram-meter-histogram meter) (integer-length n)))
  (incf (histogram-meter-total meter) n))

;;; Reports.
(trivia:defun-match reset-meter (meter)
  ((use-meter) (setf (use-meter-count meter) 0))
  ((time-meter) (setf (time-meter-units meter) 0))
  ((histogram-meter)
   (setf (histogram-meter-total meter) 0)
   (fill (histogram-meter-histogram meter) 0)))

(defun reset-meters () (mapc #'reset-meter *meters*) (values))
(defun print-meters (&key (sections :all))
  (when (eq sections :all) (setf sections '(:counts :times :histograms)))
  (dolist (section sections)
    (ecase section
      (:counts
       (format t "~{~&~60A ~8@A~}~%" '("Meter" "Count" "-----" "-----"))
       (mapc #'write-line
             (sort (loop for m in *meters*
                         when (typep m 'use-meter)
                           collect (format nil "~60A ~8D"
                                           (format nil "~{~:(~A~:)~^ → ~}" (meter-name m))
                                           (use-meter-count m)))
                   #'string<)))
      (:times
       (format t "~%~{~&~60A ~8@A~}~%" '("Meter" "Time" "-----" "----"))
       (mapc #'write-line
             (sort (loop for m in *meters*
                         when (typep m 'time-meter)
                           collect (format nil "~60A ~8D ms"
                                           (format nil "~{~:(~A~:)~^ → ~}" (meter-name m))
                                           (floor (* (time-meter-units m) 1000)
                                                  internal-time-units-per-second)))
                   #'string<)))
      (:histograms
       (format t "~%~40A ~7@A ~{  <2^~2:D ~}~%---------~%" "Histogram" "Total" (alexandria:iota 20))
       (mapc #'write-line
             (sort (loop for m in *meters*
                         when (typep m 'histogram-meter)
                           collect (format nil "~40A ~7D ~{~7D ~}"
                                           (format nil "~{~:(~A~:)~^ → ~}" (meter-name m))
                                           (histogram-meter-total m)
                                           (coerce (histogram-meter-histogram m) 'list)))
                   #'string<)))))
  (values))

(defmacro with-meters ((&key (sections ':all)) &body body)
  `(unwind-protect (progn (reset-meters) ,@body) (print-meters :sections ,sections)))
