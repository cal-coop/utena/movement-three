(in-package :movement-three)

(defvar *code* nil)
;; If we are compiling a method M1 and inline a non-tail send to a method
;; M2 with a tail send, we should not emit a tail send as we are not done
;; with M1. In this case we emit a suppressed tail send instead;
;; *EMIT-TAIL-SENDS* may be NIL or T in the easy cases, or :SUPPRESSED
;; in this case.
(defvar *emit-tail-sends* t)

(defvar *callers* '())
;; Note that *inline-depth* isn't the same as (length *callers*),
;; especially when tail sends are involved.
(defvar *inline-depth* 0)

(defgeneric method-argument-count (method))

(defgeneric compile-method-body (method start-control start-memory argument-count get-arg)
  (:documentation "Compile a method, returning a return value, control and memory.")
  (:method :around (method start-control start-memory argument-count get-arg)
    (cond
      ((= (method-argument-count method) argument-count) (call-next-method))
      (t
       (mumble method " takes " (method-argument-count method) " arguments, but " argument-count " were provided")
       (add-inputs (code-stop *code*) (crash start-control 'wrong-argument-count))
       (return-from compile-method-body (values (unreachable nil) (unreachable nil) start-memory))))))

(defgeneric method-size (method)
  (:documentation "A guesstimated value proportional to the size of the method."))

(defgeneric lookup-method (name source type receiver)
  (:documentation "Find a method by name, record source and type, returning either 
- a method and receiver node or
- :DOES-NOT-UNDERSTAND if there is no matching method
- NIL if we can't tell.")
  (:method (name source type receiver)
    (declare (ignore name source type receiver))
    nil))

;;; Compiler entrypoint
(defun compile-method (method argument-types)
  (let* ((return-type
           (method-return-type method (first argument-types) (rest argument-types)))
         (*code* (make-code method argument-types return-type)))
    (multiple-value-bind (value control memory)
        (compile-method-body
         method
         (project nil :control (code-start *code*))
         (project nil :memory (code-start *code*))
         (length argument-types)
         (lambda (i)
           (values (elt (code-arguments *code*) i) (nth i (code-argument-types *code*)))))
      ;; Add a RET instruction if we didn't tail-send away before.
      (unless (typep control 'unreachable)
        (add-inputs (code-stop *code*) (ret control memory value)))
      *code*)))

(defvar *ticks* nil)
(defvar *pass*)
(defvar *inline-discounts*)

(defun call-noting-pass (name code pass)
  (cond
    (*mumble*
     (let* ((*pass* name)
            (start (length (all-nodes code)))
            (result (funcall pass))
            (end (length (all-nodes code))))
       (if result
           (mumble start " → " end " nodes")
           (mumble "did nothing"))
       result))
    (t (funcall pass))))
(defmacro with-pass ((name code) &body body)
  `(call-noting-pass ,name ,code (lambda () ,@body)))

(define-time-meter *higher-time* higher)
(defun optimise-higher (code &key (limit 1000) (last-peep-limit 1000))
  (with-timing (*higher-time*)
    (let ((*ticks* 0)
          (*code* code)
          (*inline-discounts* 0))
      (flet ((tick ()
               (when (>= (incf *ticks*) limit)
                 ;; Would be nicer to see the METHOD-START nodes if I am
                 ;; debugging, so don't trim those.
                 #+(or) (delete-inline-scaffolding code)
                 (return-from optimise-higher code))))
        (macrolet ((pass (name action) `(with-pass (,name code) ,action)))
          (loop
            (loop
              (tick)
              (unless (or (pass "Peephole"
                                (peephole code #'peephole-higher
                                          :limit (if (= *ticks* (1- limit))
                                                     last-peep-limit
                                                     most-positive-fixnum)))
                          (pass "Typecase" (typecase-sends code))
                          (pass "Discount" (inline-discounts code)))
                (return)))
            ;; Delete the instructions for inlining tail-sends as late
            ;; as possible.
            (unless (pass "Unscaffold" (delete-inline-scaffolding code)) (return)))
          code)))))

;;; Backends
(defvar *backends* (make-hash-table))
(defvar *backend*)
(defstruct backend lower-node emit-assembly make-cfg)
(defmacro define-backend (name lower-node emit-assembly make-cfg)
  `(progn
     (setf (gethash ',name *backends*)
           (make-backend
            :lower-node ,lower-node
            :emit-assembly ,emit-assembly
            :make-cfg ,make-cfg))
     ',name))
(defun all-targets () (alexandria:hash-table-keys *backends*))

(define-meter *methods-compiled* compiler methods-compiled)
(defun compile (code &key (stage :assembly) (target :arm) (limit 1000) (last-peep-limit 1000))
  (check-type stage (member :higher :lower :cfg :assembly))
  (meter-tick *methods-compiled*)
  (optimise-higher code :limit limit :last-peep-limit last-peep-limit)
  (let ((*compilation-unit* (if (boundp '*compilation-unit*)
                                *compilation-unit*
                                (make-compilation-unit)))
        (*backend* (or (gethash target *backends*)
                       (error "No target named ~S" target))))
    (block nil
      (when (eq stage :higher) (return))
      (with-pass ("Lower" code) (lower code))
      (when (eq stage :lower) (return))
      (with-pass ("CFG" code) (schedule-into-cfg code))
      (when (eq stage :cfg)
        ;; The visualiser wants CFG-BLOCKS-BY-SCHEDULE.
        (order-basic-blocks code)
        (return))
      (with-pass ("Assembly" code)
        (funcall (backend-emit-assembly *backend*) code)))
    (values code *compilation-unit*)))

;;; Logging
(defvar *mumble* nil)
(defun mumble (&rest information)
  (unless (null *mumble*) (apply *mumble* *ticks* *pass* information)))
