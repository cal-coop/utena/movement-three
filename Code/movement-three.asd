(asdf:defsystem :movement-three
  :depends-on (:haaden-two :sourcerer
               :alexandria :bodge-queue :trivia)
  :serial t
  :components ((:file "package")
               (:file "meter")
               (:file "types")
               (:file "compilation-unit")
               (:file "compiler")
               (:module "Sea-of-nodes"
                :components ((:file "sea-of-nodes")
                             (:file "worklist")
                             (:file "analysis")
                             (:file "types")
                             (:file "control")
                             (:file "tbaa")
                             (:file "peephole")
                             (:file "inline")
                             (:file "typecase")
                             (:file "lower-for-arm")
                             (:file "lower")))
               (:module "Control-flow-graph"
                :components ((:file "schedule-into-cfg")
                             (:file "allocation-folding")
                             (:module "Write-barrier-elimination"
                              :components ((:file "cfg-analysis")
                                           (:file "known-generations")
                                           (:file "emit-write-barriers")))
                             (:file "assembly")
                             (:file "linear-scan")
                             (:file "arm")
                             (:file "cl")))
               (:module "Utena-frontend"
                :components ((:file "type")
                             (:file "compile")
                             (:file "compile-all")
                             (:file "fixnum")
                             (:file "vm")
                             (:file "typecase")))
               (:module "Platform"
                :components ((:file "platform")))))
