(in-package :movement-three)

;; Neither of these handles negation e.g. that ADD ,, -1 should be rewritten to
;; SUB ,, 1 or MOV ,-1 to MOVN ,0. The ADD/SUB at least should be done by peepholes.
(defun immediate-for-add-p (imm) (typep imm '(unsigned-byte 11)))
(defun immediate-for-mov-p (imm) (typep imm '(unsigned-byte 15)))

(defun rewrite-immediate (node test)
  (if (and (literal-p node) (funcall test (literal-value node)))
      (literal-immediate nil (literal-value node))
      node))

(defun rewrite-immediates (nodes test)
  (loop for n in nodes collect (rewrite-immediate n test)))

(trivia:defun-match lower-node-for-arm (node)
  ;; Find immediates which would involve MOVs.
  ((known-send :control c :method method :tail-p tail-p :callers callers
               :state state :memory m :receiver r :arguments a)
   (replace-node node
                 (apply #'known-send c method tail-p callers nil
                        state m r (rewrite-immediates a #'immediate-for-mov-p))))
  ((ret :control c :memory m :value (literal :value v))
   (when (immediate-for-mov-p v)
     (replace-node node (ret c m (literal-immediate nil v)))))
  ;; Find immediates for ADD/SUB.
  ((fixnum-binary-op :name (and n (or '+ '-)) :lhs l :rhs (literal :value r))
   (when (immediate-for-add-p r)
     (replace-node
      node
      (fixnum-binary-op nil n l (literal-immediate nil r)))))
  ;; and CMP is an alias for for SUB, so that has the same immediate size.
  ((fixnum-test :control c :name n :lhs l :rhs (literal :value r))
   (when (immediate-for-add-p r)
     (replace-node
      node
      (fixnum-test c n l (literal-immediate nil r))))))
