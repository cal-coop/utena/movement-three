(in-package :movement-three)

;;; Incremental analysis.
;;; I can't say I understood all of <https://arxiv.org/pdf/2104.01270.pdf>
;;; but hopefully some ideas transferred. We analyse on demand
;;; and invalidate successors when a node is mutated.

;;; An analysis consists of
;;; a bottom value which analysis starts at,
;;; a transfer function to derive the value at a node,
;;; an equality function to detect termination,
;;; a reader function to get the hash table of results, and optionally
;;; a predecessor function which computes the predecessors which might
;;; affect the value at a node.

(defstruct (analysis (:constructor %make-analysis))
  name bottom
  (transfer nil :type symbol)
  (equal nil :type symbol)
  (intersect nil :type symbol)
  (preds nil :type symbol)
  (table nil :type symbol)
  node-histogram
  node-time
  invalidate-histogram)

(defun make-analysis (name bottom transfer equal intersect preds table)
  (%make-analysis
   :name name :bottom bottom :equal equal
   :intersect intersect :transfer transfer :preds preds :table table
   :node-histogram (make-histogram-meter 'analysis name 'compute)
   :node-time (make-time-meter 'analysis name 'compute-time)
   :invalidate-histogram (make-histogram-meter 'analysis name 'invalidate)))

(defun make-analysis-table () (make-hash-table :test 'eq))

(defvar *analyses* '())
(defmacro define-analysis (name bottom transfer equal intersect preds table)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (setf *analyses* (remove ',name *analyses*))
     (push (make-analysis ',name ,bottom ',transfer ',equal
                          ',intersect ',preds ',table)
           *analyses*)))
(defun find-analysis (name)
  (or (find name *analyses* :key #'analysis-name)
      (error "No analysis named ~S" name)))

(defun analysis-results (analysis) (funcall (analysis-table analysis) *code*))

(define-meter *analysis-hit* analysis query hit)
(define-meter *analysis-miss* analysis query miss)

(defmacro result-for (analysis-name node)
  (let ((analysis (find-analysis analysis-name)))
    `(multiple-value-bind (result present)
         (gethash ,node (,(analysis-table analysis) *code*))
       (if present
           (progn (meter-tick *analysis-hit*) result)
           (progn (meter-tick *analysis-miss*)
                  (flow-analysis (find-analysis ',analysis-name) ,node))))))
(defmacro last-result-for (analysis-name node)
  (let ((analysis (find-analysis analysis-name)))
    `(multiple-value-bind (result present)
         (gethash ,node (,(analysis-table analysis) *code*))
       (if present result ',(analysis-bottom analysis)))))

(defun node-preds (node)
  (concatenate 'list (node-%inputs node) (node-%rest-inputs node)))

(define-meter *analysis-starting-points* analysis starting-points nodes)

(defun analysis-starting-points (analysis start)
  "Find all nodes we need to analyse in order to analyse START."
  (let ((seen (make-bit-set))
        (list '())
        (to-visit (bodge-queue:make-queue))
        (table (funcall (analysis-table analysis) *code*)))
    (bodge-queue:queue-push to-visit start)
    (bit-set-add seen start)
    (loop until (bodge-queue:queue-empty-p to-visit)
          do (let ((node (bodge-queue:queue-pop to-visit)))
               (meter-tick *analysis-starting-points*)
               (push node list)
               (dolist (pred (funcall (analysis-preds analysis) node))
                 (unless (or (bit-set-member seen pred)
                             (nth-value 1 (gethash pred table)))
                   (bodge-queue:queue-push to-visit pred)
                   (bit-set-add seen pred)))))
    (values seen (reverse list))))

(defun flow-analysis (analysis node)
  (with-timing ((analysis-node-time analysis))
    (multiple-value-bind (preds list) (analysis-starting-points analysis node)
      (loop with work-list = (make-worklist list)
            with table = (analysis-results analysis)
            for count from 0
            until (worklist-endp work-list)
            do (let* ((node (worklist-pop work-list))
                      (result (funcall (analysis-transfer analysis) node)))
                 (multiple-value-bind (last present) (gethash node table (analysis-bottom analysis))
                   ;; Make sure we store bottom results, to avoid extra pointless analyses.
                   (unless (and (funcall (analysis-equal analysis) result last) present)
                     #+(or) (format *debug-io* "~A ~S: ~S -> ~S~%" (analysis-name analysis) node last result)
                     (setf (gethash node table) result)
                     (dolist (s (node-%users node))
                       (when (bit-set-member preds s) (worklist-push work-list s))))))
            finally (histogram-add (analysis-node-histogram analysis) count)
                    (return (gethash node table))))))

(defun invalidate (nodes)
  "Invalidate analysis results for some nodes and their users."
  (dolist (analysis *analyses*)
    (loop with work-list = nodes
          with preds = (analysis-preds analysis)
          with table = (analysis-results analysis)
          for count from 0
          until (endp work-list)
          do (let ((node (pop work-list)))
               (when (and (nth-value 1 (gethash node table))
                          ;; Don't invalidate nodes which don't care
                          ;; about their inputs.
                          (not (null (funcall preds node))))
                 (remhash node table)
                 (dolist (user (node-%users node)) (pushnew user work-list))))
          finally (histogram-add (analysis-invalidate-histogram analysis) count))))

(defun unify-analyses (from to)
  (dolist (analysis *analyses*)
    (let ((table (funcall (analysis-table analysis) *code*)))
      (multiple-value-bind (result1 present1) (gethash from table)
        (multiple-value-bind (result2 present2) (gethash to table)
          (when (and present1 present2)
            (let ((intersect (funcall (analysis-intersect analysis) result1 result2)))
              (setf (gethash to table) intersect))))))))
