(in-package :movement-three)

;;; Type analysis

(define-analysis type
  +bottom+ new-node-type type= type-intersect
  node-type-preds code-types)
(defun old-node-type (node) (last-result-for type node))

(trivia:defun-match node-type-preds (node)
  "Prune some inputs we don't care about."
  ((or (crash) (fixnum-binary-op) (fixnum-unary-op)
       (memory-merge) (memory-split)
       (method-argument) (method-control) (method-start)
       (new) (region) (ret) (safepoint) (stop)
       (unknown-send) (vm-state) (write-slot))
   '())
  (_ (node-preds node)))

(trivia:defun-ematch new-node-type (node)
  ((node :control :dead) +bottom+)
  ((anti-dependency-fence) +memory+)
  ((argument :index i) (nth i (code-argument-types *code*)))
  ((crash) +bottom+)
  ((construct-fence :value v) (old-node-type v))
  ((dead-new) +top+)
  ((eq-test :lhs lhs :rhs (literal :value v))
   (tuple :yes +control+ :no +control+ :value (exactly v) :no-value (old-node-type lhs)))
  ((eq-test :lhs lhs :rhs rhs)
   (tuple :yes +control+ :no +control+
          :value (type-intersect (old-node-type lhs) (old-node-type rhs))
          :no-value (old-node-type lhs)))
  ((or (fixnum-binary-op) (fixnum-unary-op))
   (constant-tuple :okay +control+ :overflow +control+ :value +fixnum+))
  ((fixnum-test) (constant-tuple :yes +control+ :no +control+))
  ((immutable-memory) +memory+)
  ((known-send :method m :receiver receiver :tail-p tail-p)
   (if (eq tail-p t)
       +bottom+
       (tuple
        :control +control+
        :memory +memory+
        :value (method-return-type
                m (old-node-type receiver)
                (mapcar #'old-node-type (known-send-arguments node))))))
  ((literal :value value) (exactly value))
  ((literal-immediate :value value) (exactly value))
  ((memory-merge) +memory+)
  ((memory-split) +memory+)
  ((method-argument :type type) type)
  ((method-control) +control+)
  ((method-start) (constant-tuple :control +control+ :memory +memory+))
  ((multi-new :types types)
   (%tuple (coerce (alexandria:iota (length types)) 'vector) (coerce types 'vector)))
  ((new :type type) (tuple :memory +memory+ :value type))
  ((pack :keys keys) (%tuple keys (coerce (mapcar #'old-node-type (pack-values node)) 'vector)))
  ((perform-safepoint) (constant-tuple :control +control+ :memory +memory+))
  ((phi) (reduce #'type-union (phi-values node) :key #'old-node-type :initial-value +bottom+))
  ((project :name name :tuple tuple)
   (trivia:match (old-node-type tuple)
     ((tuple) (tuple-lookup (old-node-type tuple) name))
     (_ +bottom+)))
  ((read-slot :index index :instance instance)
   (trivia:match (widen-away-exactly (old-node-type instance))
     ((and record (record)) (slot-definition-type (slot-by-index record index)))
     ((eq +bottom+) +bottom+)
     (weird (error "Can't read from ~S" weird))))
  ((region) +control+)
  ((ret) +bottom+)
  ((safepoint) (constant-tuple :control +control+ :memory +memory+))
  ((start) (constant-tuple :control +control+ :memory +memory+))
  ((stop) +bottom+)
  ((tls) *tls-type*)
  ((type-test :instance i :type type)
   (tuple :yes +control+ :no +control+
          :value type
          :no-value (type-difference (old-node-type i) type)))
  ((unknown-send :tail-p tail-p)
   (if (eq tail-p t) +bottom+ (constant-tuple :control +control+ :memory +memory+ :value +top+)))
  ((unreachable) +bottom+)
  ((vm-state) +state+)
  ((write-slot) +memory+))
