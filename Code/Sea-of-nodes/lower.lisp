(in-package :movement-three)

(defvar *do-allocation-folding* t)

(define-time-meter *lower-time* lower)
(defun lower (code)
  (with-timing (*lower-time*)
    (let ((*code* code))
      (mapc #'lower-node (all-nodes code))
      (mapc (backend-lower-node *backend*) (all-nodes code))
      (install-anti-dependencies code)
      (peephole code #'peephole-cleanup))))

(trivia:defun-match lower-node (node)
  ((safepoint :control c :state s :memory m)
   ;; TODO: Fence? The scheduler doesn't do LICM but if it did
   ;; we'd probably miss safepoints.
   (let* ((test (eq-test c (read-slot
                            nil
                            +tls-safepoint-index+
                            (split-memory *tls-type* +tls-safepoint-index+ m)
                            (tls nil))
                         (literal nil 1)))
          (safepoint (perform-safepoint (project nil :yes test) s m))
          (merge-region (region nil (project nil :control safepoint) (project nil :no test))))
     (replace-node node
                   (pack nil #(:control :memory)
                         merge-region
                         (phi merge-region +memory+ (project nil :memory safepoint) m)))))
  ;; Turn a type test for an EXACTLY type into an EQ test.
  ((type-test :control c :type (exactly :value v) :instance i)
   (replace-node node (eq-test c i (literal nil v))))
  ;; Explode a test for a heap type into a test for heap-ness and then a
  ;; layout test.
  ((type-test :control c :type (and type (not (eq +fixnum+))) :instance i)
   (let* ((fixnum-test (type-test c +fixnum+ i))
          (new-type-test (type-test (project nil :no fixnum-test)
                                    type (project nil :no-value fixnum-test))))
     (replace-node node
                   (pack nil #(:yes :no :value :no-value)
                         (project nil :yes new-type-test)
                         (region nil (project nil :yes fixnum-test) (project nil :no new-type-test))
                         (project nil :value new-type-test)
                         i))))
  ;; Pick a specialisation of KNOWN-SEND and find immediate arguments.
  ((known-send :method m :receiver r :arguments a)
   (setf (known-send-method node)
         (find-function m
                        (loop for n in (cons r a)
                              collect (widen-away-exactly (result-for type n)))
                        :create-if-missing t)))
  ;; Replace UNKNOWN-SEND with a call to a trampoline with the right
  ;; argument count. If the message name is constant we use a dispatch
  ;; function for that name.
  ((unknown-send
    :control c :tail-p tail :callers callers :state state
    :memory m :name (literal :value n) :receiver r :arguments a)
   (replace-node node
                 (apply #'known-send c (find-dispatch-function n (length a))
                        tail callers nil state m r a)))
  ;; Else we use a general dispatch function.
  ((unknown-send
    :control c :tail-p tail :callers callers :state state
    :memory m :name n :receiver r :arguments a)
   (replace-node node
                 (apply #'known-send c (find-unknown-send-function (length a))
                        tail callers nil state m r n a)))
  ((new :type ty :memory m :values v)
   (cond
     ;; Replace immutable inputs to NEW with WRITE-SLOT nodes.
     (*do-allocation-folding*
      (let* ((new-node (apply #'new nil ty m v))
             (value (project nil :value new-node))
             (sets (loop for v in (new-values node) for i from 0
                         collect (write-slot nil i m value v)))
             ;; Initialising writes to immutable slots never alias, so they
             ;; get new alias names.
             (merge (apply #'memory-merge nil
                           (loop for nil in (new-values node) collect (gensym "INIT"))
                           m
                           sets)))
        (replace-node node (pack nil #(:memory :value)
                                 merge
                                 (construct-fence nil merge value)))))
     ;; Replace initialising WRITE-SLOTS to NEW with inputs. Else the
     ;; scheduler can overlap other allocations with this initialisation,
     ;; allowing GC to happen while objects are uninitialised.
     (t
      (dolist (proj (node-%users node))
        (trivia:match proj
          ((project :name :value)
           (let ((seen '()))
             (dolist (u (node-%users proj))
               (trivia:match u
                 ((write-slot :instance (eq proj)) (push u seen))))
             (let ((new-inputs (append v (mapcar #'write-slot-new-value (sort seen #'< :key #'write-slot-index)))))
               (replace-node node (apply #'new nil ty m new-inputs))
               (dolist (s seen) (replace-node s (write-slot-memory s))))))))))))

(defun install-anti-dependencies (code)
  "Make all non-reads to a memory flow depend on reads to the same memory flow, by installing ANTI-DEPENDENCY-FENCE nodes."
  (let ((fences (make-hash-table)))
    (dolist (node (all-nodes code))
      (trivia:match node
        ;; Reads of immutable slots don't count; we can't write to them.
        ;; (Initialising writes to immutable slots got their own names
        ;; in LOWER-NODE, and they are ordered by CONSTRUCT-FENCEs.)
        ((read-slot :memory (and m (not (immutable-memory))))
         (when (gethash m fences) (trivia.next:next))
         (setf (gethash m fences) t)
         (let ((users (node-%users m)))
           (replace-uses (remove-if #'read-slot-p users)
                         m
                         (apply #'anti-dependency-fence nil m
                                (remove-if-not #'read-slot-p users)))))))))
