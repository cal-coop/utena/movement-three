(in-package :movement-three)

;;; A list of functions which pick types to type-case on.
(defvar *typecasers* '())

(defun typecase-cases (name)
  (remove-duplicates
   (loop for caser in *typecasers*
         appending (funcall caser name))))

(defun typecase-send (node cases)
  (trivia:ematch node
    ((unknown-send :control c :access a :tail-p tail
                   :callers callers :state state :memory m
                   :name n :receiver r)
     (let ((arguments (unknown-send-arguments node))
           (last-control c)
           (last-value r)
           (sends '()))
       (dolist (case cases)
         (let* ((test (type-test last-control case last-value))
                (success (apply #'unknown-send
                                (project nil :yes test) a tail callers state
                                m n (project nil :value test)
                                arguments)))
           (push success sends)
           (setf last-control (project nil :no test)
                 last-value (project nil :no-value test))))
       (push (apply #'unknown-send last-control a tail callers
                    state m n r arguments)
             sends)
       (cond
         ((eq tail t)
          (apply #'add-inputs (code-stop *code*) sends)
          (unreachable nil))
         (t
          (flet ((project-all (key) (loop for send in sends collect (project nil key send))))
            (let ((reg (apply #'region nil (project-all :control))))
              (pack nil #(:control :value :memory)
                    reg
                    (apply #'phi reg +top+ (project-all :value))
                    (apply #'phi reg +memory+ (project-all :memory)))))))))))

;; Find if a node is dominated by either arm of a type test.
(defun find-typecase-dominator (node)
  (map-dominators
   (lambda (dom)
     (trivia:match dom
       ((project :tuple (or (type-test :instance (eq (unknown-send-receiver node)))
                            (eq-test :lhs (eq (unknown-send-receiver node)))))
        (return-from find-typecase-dominator dom))))
   node)
  nil)

(defun typecase-sends (code)
  (let ((*code* code)
        (worked nil))
    (dolist (n (all-nodes code) worked)
      (trivia:match n
        ((where (unknown-send :receiver r :name (literal :value name))
                ((eq +top+) (result-for type r))
                ((and cases (not '())) (typecase-cases name))
                (nil (find-typecase-dominator n)))
         (setf worked t)
         (replace-node n (typecase-send n cases)))
        ((where (unknown-send :receiver r)
                ((union-type :types types) (widen-away-exactly (result-for type r))))
         (setf worked t)
         (replace-node n (typecase-send n types)))))))
