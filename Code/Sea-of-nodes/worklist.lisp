(in-package :movement-three)

;; SBCL adjustable array moment
(defstruct (bit-set (:constructor %make-bit-set) (:copier nil))
  (storage nil :type (simple-array bit 1)))

(defun make-bit-set (&key (initial-size (max 64 (1+ (code-next-id *code*)))))
  (%make-bit-set
   :storage (make-array initial-size
                        :element-type 'bit
                        :initial-element 0)))

(defun bit-set-member (bit-set node)
  (let ((storage (bit-set-storage bit-set))
        (id (node-id node)))
    (and (< id (length storage)) (plusp (bit storage id)))))

(defun bit-set-add (bit-set node)
  (let ((id (node-id node))
        (storage (bit-set-storage bit-set)))
    (cond
      ((< id (length storage)) (setf (bit storage id) 1))
      (t
       (let* ((size (max (1+ (node-id node)) (* 2 (length storage))))
              (resized (make-array size :element-type 'bit :initial-element 0)))
         (replace resized (bit-set-storage bit-set))
         (setf (bit-set-storage bit-set) resized
               (bit (bit-set-storage bit-set) id) 1))))))

(defstruct (worklist (:constructor %make-worklist) (:copier nil))
  (bit-set (make-bit-set) :type bit-set)
  (list '()))

(defun make-worklist (initial-elements)
  (let ((w (%make-worklist)))
    (dolist (e initial-elements w)
      (assert (not (bit-set-member (worklist-bit-set w) e))
              () "~S appears twice in INITIAL-ELEMENTS" e)
      (worklist-push w e))))

(defun worklist-push (worklist node)
  (unless (bit-set-member (worklist-bit-set worklist) node)
    (bit-set-add (worklist-bit-set worklist) node)
    (push node (worklist-list worklist))))

(defun worklist-pop (worklist)
  (let ((e (pop (worklist-list worklist))))
    (setf (bit (bit-set-storage (worklist-bit-set worklist)) (node-id e)) 0)
    e))

(defun worklist-endp (worklist) (endp (worklist-list worklist)))
