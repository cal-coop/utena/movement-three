(in-package :movement-three)

(defvar *equivalence-sets* (make-hash-table :test 'equal))
(defun find-equivalence-set (record slot-index)
  (let ((key (cons record slot-index)))
        (or (gethash key *equivalence-sets*)
            (setf (gethash key *equivalence-sets*) key))))
  
(defun split-memory (record slot-index memory)
  (if (< slot-index (length (record-immutable-slots record)))
      (immutable)
      (memory-split nil (find-equivalence-set record slot-index) memory)))

(defmacro with-mutated-split ((split record slot-index memory) &body body)
  (alexandria:once-only (memory)
    (alexandria:with-gensyms (result)
      `(let* ((,split (split-memory ,record ,slot-index ,memory))
              (,result (progn ,@body)))
         (memory-merge nil (list (memory-split-alias ,split)) ,memory ,result)))))

(defun new* (mutable-p type memory &rest slots)
  (let* ((alias (if mutable-p (memory-split nil 'new memory) (immutable)))
         (result (apply #'new nil type alias slots)))
    (values (project nil :value result)
            (if mutable-p
                (memory-merge nil '(new) memory (project nil :memory result))
                memory))))
