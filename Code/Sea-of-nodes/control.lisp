(in-package :movement-three)

;;; Dominators
;;; <https://www.cs.tufts.edu/comp/150FP/archive/keith-cooper/dom14.pdf>
;;; incrementalised the easy way. (There are better incremental algorithms.)
;;; I don't have a reverse post-order index handy, but tracking depth works to
;;; guide the search for the least common ancestor.

(define-analysis dominator
  nil dominators equal dominator-intersect
  node-dominator-preds code-dominators)

(flet ((last-dom (n) (last-result-for dominator n)))
  (trivia:defun-ematch* dominator-intersect (a b)
    ((nil b) b)
    ((a nil) a)
    (((cons 0 _) other) other)
    ((other (cons 0 _)) other)
    (((and a (cons a-depth a-dom)) (and b (cons b-depth b-dom)))
     (cond
       ((eq a-dom b-dom) (cons a-depth a-dom))
       ((= a-depth b-depth) (dominator-intersect (last-dom a-dom) (last-dom b-dom)))
       ((< a-depth b-depth) (dominator-intersect a (last-dom b-dom)))
       ((> a-depth b-depth) (dominator-intersect (last-dom a-dom) b))))))

(trivia:defun-ematch* dominator-adjoin (element dominators)
  ((_ nil) nil)
  ((e (cons depth _)) (cons (1+ depth) e)))

(trivia:defun-match node-dominator-preds (node)
  ((pack) (pack-values node))
  ((project :tuple tuple) (list tuple))
  ((region) (region-predecessors node))
  (_ (if (null (node-control node)) '() (list (node-control node)))))

(defun dominators (node)
  (flet ((propagate (pred)
           (if (null pred)
               nil
               (dominator-adjoin pred (last-result-for dominator pred)))))
    (trivia:match node
      ((start) (cons 0 nil))
      ((pack :keys keys)
       (%tuple keys (coerce (mapcar #'propagate (pack-values node)) 'vector)))
      ((where (project :name name :tuple tuple)
              ((pack) tuple))
       (trivia:ematch (last-result-for dominator tuple)
         (nil nil)
         ((and ds (type tuple)) (tuple-lookup ds name))))
      ((project :tuple tuple) (propagate tuple))
      ((region)
       (reduce #'dominator-intersect (region-predecessors node)
               :key #'propagate :initial-value nil))
      ((method-control) (propagate (node-control node))
       ;; The inliner only makes back-arcs from sends which are dominated
       ;; by the METHOD-CONTROL node, so they can't affect dominators of this
       ;; node.
       #+(or)
       (reduce #'dominator-intersect
               (cons (node-control node) (method-control-back-arcs node))
               :key #'propagate))
      ((node :control c) (propagate c)))))

(defun map-predecessor-chain (function initial-node)
  "Walk node control until we run out of control (returning :HIT-END) or hit a control-flow merge (returning :HIT-MERGE)."
  (let ((node initial-node))
    (loop
      (trivia:match node
        (nil (return :hit-end))
        ((or (region) (method-control))
         (funcall function node)
         (return (values :hit-merge node)))
        ((project :tuple tuple) (funcall function node) (setf node tuple))
        ((start) (funcall function node) (return :hit-end))
        ;; If we explicated natural loops in the IR, this would be a
        ;; natural place to use them to find loop-invariant stuff without
        ;; properly computing dominators.
        ((node :control pred) (funcall function node) (setf node pred))))))

(defun map-dominators-slowly (function node)
  (trivia:ematch (result-for dominator node)
    ((or nil (cons _ nil)))
    ((cons _ dom) (funcall function dom) (map-dominators-slowly function dom))))

(defun map-dominators (function initial-node)
  "Calls FUNCTION with every 'easy' dominating node as found by MAP-PREDECESSOR-CHAIN.
Then calls FUNCTION with every dominator as found by data-flow analysis, if M-P-C hit a merge."
  (multiple-value-bind (cause point) (map-predecessor-chain function initial-node)
    (when (eq :hit-merge cause) (map-dominators-slowly function point))
    (values)))

(defun node-dominates-p (dom sub)
  (map-dominators
   (lambda (n) (when (eq n dom) (return-from node-dominates-p t)))
   sub)
  nil)

;;; Safepoint/back-arc detection

;; The NEEDS-SAFEPOINT analysis determines both if we had safepointed
;; before and if we can reach a node by a loop.
;; A path is a pair (needs-safepoint . predecessors), with predecessors
;; sorted by node ID.

(define-analysis needs-safepoint
  '(nil . nil) paths paths-equal paths-intersect
  node-safepoint-preds code-paths)

(trivia:defun-ematch* insert-into-sorted (element list)
  ((e '()) (list e))
  ((e (cons (eq e) _)) list)
  ((e (cons f r))
   (if (< (node-id e) (node-id f)) (list* e f r) (cons f (insert-into-sorted e r)))))

(trivia:defun-ematch* union-sorted (list1 list2)
  ((l '()) l)
  (('() l) l)
  (((cons h1 t1) (cons h2 t2))
   (cond
     ((eq h1 h2) (cons h1 (union-sorted t1 t2)))
     ((< (node-id h1) (node-id h2)) (cons h1 (union-sorted t1 list2)))
     (t (cons h2 (union-sorted list1 t2))))))

(trivia:defun-ematch* intersect-sorted (list1 list2)
  ((_ '()) '())
  (('() _) '())
  (((cons h1 t1) (cons h2 t2))
   (cond
     ((eq h1 h2) (cons h1 (intersect-sorted t1 t2)))
     ((< (node-id h1) (node-id h2)) (intersect-sorted t1 list2))
     (t (intersect-sorted list1 t2)))))

(trivia:defun-ematch* remove-from-sorted (element list)
  ((_ '()) '())
  ((e (cons (eq e) r)) r)
  ((e (cons h r))
   (if (< (node-id e) (node-id h))
       list
       (cons h (remove-from-sorted e r)))))

(defun paths-union (a b)
  (cons (or (car a) (car b))
        (union-sorted (cdr a) (cdr b))))

(defun paths-intersect (a b)
  (cons (and (car a) (car b))
        (intersect-sorted (cdr a) (cdr b))))

(trivia:defun-ematch* paths-equal (p1 p2)
  ((p1 (eq p1)) t)
  (((and p1 (tuple)) (and p2 (tuple)))
   (and (equalp (tuple-keys p1) (tuple-keys p2))
        (every #'paths-equal (tuple-values p1) (tuple-values p2))))
  ((p1 p2) (and (listp p1) (listp p2) (equal p1 p2))))

(trivia:defun-ematch node-safepoint-preds (node)
  ((or (known-send :memory m)
       (unknown-send :memory m)
       (method-start :memory m)
       (safepoint :memory m))
   (list m))
  ((method-argument) (method-argument-values node))
  ((memory-merge :bulk b) (list b))
  ((pack) (pack-values node))
  ((phi) (phi-values node))
  ((project :tuple tuple) (list tuple))
  (_ '()))

(defun paths (node)
  (flet ((propagate (node) (last-result-for needs-safepoint node)))
    (trivia:match node
      ((start) '(t . ()))
      ((or (safepoint :memory m) (unknown-send :memory m) (known-send :memory m))
       (cons nil (cdr (last-result-for needs-safepoint m))))
      ;; Can't prove anything, we might make a loop later.
      ((or (method-argument) (method-start)) '(t . ()))
      ((phi)
       (let ((prior (reduce #'paths-union (phi-values node)
                            :key #'propagate :initial-value '(nil . nil))))
         (cond
           ((car prior) prior)
           ((member node (cdr prior)) (cons t (cdr prior)))
           ;; We can only form loops at REGION nodes, so we only track
           ;; REGION nodes.
           (t (cons nil (insert-into-sorted node (cdr prior)))))))
      ((pack :keys keys)
       (%tuple keys (coerce (mapcar #'propagate (pack-values node)) 'vector)))
      ((where (project :name name :tuple tuple)
              ((pack) tuple))
       (trivia:match (last-result-for needs-safepoint tuple)
         ((and ds (type tuple)) (tuple-lookup ds name))
         (v v)))
      ((project :tuple tuple) (propagate tuple))
      (_ '(nil . ())))))

(defun needs-safepoint-p (node)
  (let ((r (result-for needs-safepoint node)))
    (if (consp r) (car r) nil)))
