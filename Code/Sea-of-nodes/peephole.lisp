(in-package :movement-three)

(defun node-unused-p (node)
  (and (node-mortal-p node)
       (not (eq (node-control node) :dead))
       (endp (node-%users node))))

(define-histogram-meter *peephole* peephole visits)
(define-histogram-meter *peephole-fired* peephole fired)

(defun peephole (code function &key (limit most-positive-fixnum))
  (loop with *code* = code
        with work-list = (make-worklist (all-nodes code))
        with *constructor-hook* = (lambda (n) (worklist-push work-list n))
        with worked = nil
        ;; Meters
        with nodes = 0
        with fired = 0
        for n below limit
        until (worklist-endp work-list)
        do (let ((node (worklist-pop work-list)))
             (incf nodes)
             ;; Kill unused nodes and propagate.
             (when (node-unused-p node)
               (dolist (i (node-%rest-inputs node))
                 (worklist-push work-list i))
               (loop for i across (node-%inputs node)
                     do (worklist-push work-list i))
               (unless (null (node-control node))
                 (worklist-push work-list (node-control node)))
               (kill node))
             ;; Don't peephole dead nodes.
             (unless (eq :dead (node-control node))
               ;; Peephole.
               ;; The peephole function returns
               ;; - a new node to supersede the node,
               ;; - the same node if it was updated in-place, or
               ;; - NIL if no peepholing can be done.
               (let ((ideal (funcall function node)))
                 (unless (null ideal)
                   (incf fired)
                   (setf worked t)
                   (worklist-push work-list ideal)
                   (dolist (user (if (eq node ideal)
                                     (node-%users node)
                                     (replace-node node ideal)))
                     (worklist-push work-list user))))))
        finally (trim-stop-inputs code)
                (histogram-add *peephole* nodes)
                (histogram-add *peephole-fired* fired)
                (return worked)))

(defun peephole-higher (node) (or (peephole-cleanup node) (peephole-higher-optimisations node)))

;; I can't see a noticeable slowdown from this hack, but here's a
;; feature flag just in case:
#-no-profile-peepholes
(defmacro define-peepholer (name (node) &body cases)
  "Put every peephole body in a local function, so that SB-SPROF can identify individual rules."
  `(trivia:defun-match ,name (,node)
     ,@(loop for (pattern . body) in cases
             for i from 0
             for name = (alexandria:format-symbol nil "PATTERN-~D" i)
             collect `(,pattern (flet ((,name () ,@body)) (declare (notinline ,name)) (,name))))))
#+no-profile-peepholes
(defmacro define-peepholer (name (node) &body cases)
  `(trivia:defun-match ,name (,node) ,@cases))

(defmacro let-match (bindings &body body)
  "Attempt to match patterns in each (PATTERN VALUE) binding, else move onto the next rule."
  `(block let-match
     (trivia:match* ,(mapcar #'second bindings)
       (,(mapcar #'first bindings) (return-from let-match (progn ,@body))))
     (trivia.next:next)))

(define-peepholer peephole-cleanup (node)
  ;; Remove unreachable nodes.
  ((node :control (unreachable)) (unreachable nil))
  ;; Do global value numbering.
  ((node)
   (let ((old (gethash node (code-gvn *code*))))
     (cond
       ((null old)
        (setf (gethash node (code-gvn *code*)) node)
        (trivia.next:next))
       ((eq old node) (trivia.next:next))
       (t old))))
  ;; The flow analysis doesn't yet catch anything interesting.
  ;; I suspect it's only more powerful when we have dead loops;
  ;; nodes would keep themselves live without the optimistic
  ;; analysis to break the cycle.
  #+(or)
  ((node :control (and c (node)))
   (let-match (((eq +bottom+) (result-for type c)))
     (unreachable nil)))
  ((project :tuple (or (crash) (unreachable))) (unreachable nil))
  ;; -- Administrative stuff --
  ;; See through PROJECT -> PACK
  ((where (project :name name :tuple tuple)
          ((pack :keys keys) tuple))
   (nth (position name keys) (pack-values tuple)))
  ;; Remove 0 and 1-ary PHI
  ((phi :values v)
   (case (length v)
     (0 (unreachable nil))
     (1 (first v))
     (t (trivia.next:next))))
  ;; Remove PHI of the same value
  ((phi :control r :values v)
   (let ((alive (remove-duplicates
                 (loop for pred in (region-predecessors r)
                       for value in v
                       unless (or (unreachable-p pred) (eq value node))
                         collect value))))
     (case (length alive)
       (0 (unreachable nil))
       (1 (first alive))
       (otherwise (trivia.next:next)))))
  ;; Prune REGION inputs
  ((region :predecessors preds)
   (let ((unreachable (mapcar #'unreachable-p preds)))
     (unless (some #'identity unreachable) (trivia.next:next))
     (let* ((alive (remove-if #'unreachable-p preds))
            (new (apply #'region nil alive)))
     ;; Remove dead inputs from all PHIs using the REGION
     (dolist (user (node-%users node))
       (trivia:match user
         ((phi :control (eq node) :known-type k)
          (replace-node user
                        (apply #'phi new k
                               (loop for u in unreachable
                                     for v in (phi-values user)
                                     unless u collect v))))))
       new)))
  ;; Remove 0 and 1-ary REGION
  ((region :predecessors preds)
   ;; Don't disappear on PHI nodes though.
   (when (some #'phi-p (node-%users node)) (trivia.next:next))
   (case (length preds)
     (0 (unreachable nil))
     (1 (first preds))
     (otherwise (trivia.next:next))))
  ;; -- Memory --
  ;; See through READ-SLOT -> PROJECT (:VALUE) -> NEW (with immutable slots)
  ((where (read-slot :index index :instance project)
          ((project :name :value :tuple instance) project)
          ((new :values values) instance))
   (when (>= index (length (new-values instance))) (trivia.next:next))
   (nth index values))
  ;; Push READ-SLOT through PHI
  ;; This rule can still loop forever if we end up with a cycle of
  ;; multiple PHIs.
  #+(or)
  ((where (read-slot :index i :memory m :instance phi)
          ((phi :control c) phi))
   ;; We'd reapply this rule forever if we did this to a
   ;; self-referential PHI.
   (when (member phi (phi-values phi)) (trivia.next:next)) 
   (apply #'phi c +top+
          (loop for v in (phi-values phi)
                collect (read-slot nil i m v))))
  ;; This weaker version does always terminate though, and works
  ;; nicely for activation records and loops.
  ((where (read-slot :index i :memory m :instance phi)
          ((phi :control c :values values) phi))
   (unless (loop for v in values always (trivia:match v ((project :tuple (new)) t)))
     (trivia.next:next))
   (apply #'phi c +top+ (loop for v in values collect (read-slot nil i m v))))
  ;; See through READ-SLOT -> WRITE-SLOT to the same slot.
  ((where (read-slot :memory write :index index
                     :instance (or instance (construct-fence :value instance)))
          ((write-slot :index (eql index) :instance (eq instance) :new-value v) write))
   v)
  ;; See through immutable READ-SLOT -> CONSTRUCT-FENCE.
  ((where (read-slot :memory (immutable-memory) :index index :instance instance)
          ((construct-fence :value v) instance))
   (read-slot nil index (immutable) v))
  ;; See through WRITE-SLOT -> WRITE-SLOT to the same slot.
  ((where (write-slot :control c :memory write2 :index index :instance instance :new-value v)
          ((write-slot :control (eq c) :memory prior :index (eql index) :instance (eq instance)) write2))
   (write-slot c index prior instance v))
  ;; See through WRITE-SLOT of LITERAL -> CONSTRUCT-FENCE -> WRITE-SLOT
  ;; to the same slot.
  ((where (write-slot :control c :memory write2 :index index :instance construct :new-value v)
          (t (write-slot-value-movable-p v))
          ((construct-fence :value instance) construct)
          ((write-slot :control (eq c) :memory prior :index (eql index) :instance (eq instance)) write2))
   (let ((new (write-slot c index prior instance v)))
     (replace-node write2 new)
     new))
  ;; See through MEMORY-SPLIT -> MEMORY-MERGE.
  ((where (memory-split :alias alias :memory merge)
          ((memory-merge :aliases aliases :bulk b :sets s) merge)
          (index (position alias aliases)))
   (if (null index)
       ;; This merge isn't relevant; push up to the next memory state.
       (memory-split nil alias b)
       ;; Reuse the prior split.
       (nth index s)))
  ;; Merge back-to-back MEMORY-MERGEs.
  ((memory-merge :aliases '() :bulk m) m)
  ((where (memory-merge :bulk merge2)
          ((memory-merge :bulk bulk) merge2))
   (merge-memory-merges node merge2 bulk))
  ;; Merge back-to-back CONSTRUCT-FENCEs.
  ((construct-fence :memory m :value (construct-fence :value v))
   (construct-fence nil m v))
  ;; Remove dead construct fences.
  ((construct-fence :value v)
   ;; The same rules apply to fences as to allocations - fences should
   ;; only exist so long as something depends on the fence for ordering
   ;; guarantees.
   (when (new-value-has-interesting-uses-p node) (trivia.next:next))
   v)
  ;; Remove pointless MEMORY-SPLITs from MEMORY-MERGEs.
  ((memory-merge) (cull-pointless-splits-from-merge node))
  ;; Remove dead allocations.
  ((project :name :value :tuple (new))
   (when (new-value-has-interesting-uses-p node) (trivia.next:next))
   (dead-new nil))
  ((where (new :memory m)
          ((list (project :name :memory :tuple (eq node))) (node-%users node)))
   (pack nil #(:memory) m))
  ;; Remove dead stores.
  ((write-slot :instance (dead-new) :memory m) m)
  ((construct-fence :value (dead-new)) (dead-new nil)))

(define-peepholer peephole-higher-optimisations (node)
  ;; -- Control flow --
  ;; Remove always passing/failing tests.
  ((type-test :control c :type wanted :instance i)
   (let ((got (result-for type i)))
     (cond
       ((type<= got wanted) ; test always passes
        (pack nil #(:yes :no :value :no-value) c (unreachable nil) i (unreachable nil)))
       ((types-disjoint-p got wanted) ; test never passes
        (pack nil #(:yes :no :value :no-value) (unreachable nil) c (unreachable nil) i))
       (t (trivia.next:next)))))
  ;; Swing around inputs for redundant tests.
  ((where (type-test :control c :type ty :instance i)
          ((and (node) refined) (find-refinement c i)))
   (type-test c ty refined))
  ;; Swing around inputs to phi nodes.
  ((where (phi :control c :known-type (eq +top+) :values values)
          ((region :predecessors preds) c))
   (let ((ref (loop for v in values for p in preds
                    collect (find-refinement p v))))
     (when (every #'null ref) (trivia.next:next))
     (apply #'phi c +top+ (loop for v in values for r in ref collect (if (null r) v r)))))
  ;; Just enough splitting to avoid a pointless test. We need:
  ((where (type-test :control reg :type type :instance i)
          ;; - A REGION with two inputs
          ((region :predecessors (list c1 c2)) reg)
          ;; - A PHI solely used by the type test
          ((phi :values (list v1 v2)) i)
          ((list _) (phi-%users i)))
   ;; - The region to be only used by the type test and the phi
   (dolist (u (node-%users reg)) (unless (or (eq u node) (eq u i)) (trivia.next:next)))
   (flet ((split (yes-c yes-v no-c no-v)
            (pack nil #(:yes :no :value :no-value) yes-c no-c yes-v no-v)))
     ;; - One input always is the type, the other always isn't
     (cond
       ((and (type<= (result-for type v1) type) (types-disjoint-p (result-for type v2) type))
        (split c1 v1 c2 v2))
       ((and (type<= (result-for type v2) type) (types-disjoint-p (result-for type v1) type))
        (split c2 v2 c1 v1))
       (t (trivia.next:next)))))
  ;; Merge back-to-back REGION nodes and their PHIs if the region is only
  ;; around for PHIs.
  ((region :predecessors preds)
   (let* ((flattenable (loop for p in preds collect (region-flattenable-p p node))))
     (unless (some #'identity flattenable) (trivia.next:next))
     (let* ((new-region (apply #'region
                               nil
                               (loop for r in preds
                                     for f in flattenable
                                     if f append (region-predecessors r) else collect r))))
       ;; Update each PHI to have values for the predecessors we just flattened in.
       (dolist (u (node-%users node))
         (trivia:match u
           ((phi :control (eq node) :known-type k :values values)
            (let ((new-phi (apply #'phi new-region k
                                  (loop for v in values
                                        for p in preds
                                        for f in flattenable
                                        append (trivia:match* (f v)
                                                 ((nil _) (list v))
                                                 ((_ (phi :control (eq p) :values vs)) vs)
                                                 ((_ _) (mapcar (constantly v) (region-predecessors p))))))))
              (replace-node u new-phi)))))
       new-region)))
  ;; Turn an EQ-TEST into a type test of an EXACTLY type. This makes the
  ;; type test machinery work for optimising around EQ?. LOWER-NODE does
  ;; the reverse to make code generation slightly neater.
  ((eq-test :control c :lhs l :rhs (literal :value v))
   (type-test c (exactly v) l))
  ;; -- Constant folding --
  ;; Turn nodes which can only produce a constant into LITERALs.
  ;; This seems intuitive enough, and Simple does it, but it is slow
  ;; and matches exactly 0 nodes in the test suite.
  #+(or)
  ((not (literal))
   (let-match (((exactly :value v) (result-for type node)))
     (literal nil v)))
  ;; Fold fixnum operations and tests.
  ((fixnum-binary-op :name n :lhs (literal :value l) :rhs (literal :value r))
   (unless (and (integerp l) (integerp r)) (error "FIXNUM-BINARY-OP with non-integer literal inputs"))
   (let ((value (handler-case (funcall n l r) (division-by-zero () 0))))
     (pack nil #(:okay :overflow :value) (unreachable nil) (unreachable nil) (literal nil value))))
  ((fixnum-unary-op :name n :value (literal :value v))
   (unless (integerp v) (error "FIXNUM-UNARY-OP with non-integer literal value"))
   (let ((value (funcall n v)))
     (pack nil #(:okay :overflow :value) (unreachable nil) (unreachable nil) (literal nil value))))
  ((fixnum-test :control c :name n :lhs (literal :value l) :rhs (literal :value r))
   (unless (and (integerp l) (integerp r)) (error "FIXNUM-TEST with non-integer literal inputs"))
   (let ((value (funcall n l r)))
     (if value
         (pack nil #(:yes :no) c (unreachable nil))
         (pack nil #(:yes :no) (unreachable nil) c))))
  ((where (fixnum-test :control c)
          ((project :name direction) (find-dominating-test node)))
   (if (eq direction :yes)
       (pack nil #(:yes :no) c (unreachable nil))
       (pack nil #(:no :yes) (unreachable nil) c)))
  ;; -- VM stuff --
  ;; Remove back-to-back safepoints.
  ((safepoint :control control :memory mem)
   (when (needs-safepoint-p mem) (trivia.next:next))
   (pack nil #(:control :memory) control mem))
  ;; Turn unknown sends with known receiver types into known sends.
  ((unknown-send :control c :tail-p tail :callers call :state s :memory m
                 :name (literal :value n) :receiver r :arguments args)
   (multiple-value-bind (method receiver)
       (let* ((refined (or (find-refinement c r) r))
              (type (widen-away-exactly (result-for type refined))))
         (lookup-method n (type-source type) type r))
     (case method
       ((nil) (trivia.next:next))
       ((:does-not-understand)
        (mumble "The node " r " does not understand the message " n)
        (let ((c (crash c `(:does-not-understand ,n))))
          (add-inputs (code-stop *code*) c)
          c))
       (otherwise (apply #'known-send c method tail call nil s m receiver args)))))
  ;; Refine arguments to a known send.
  ((known-send :control c :method method :tail-p tail :callers call
               :inline i :state s :memory m :receiver r :arguments args)
   (let ((refined (loop for a in args collect (find-refinement c a)))
         (receiver (find-refinement c r)))
     (unless (or (not (null receiver)) (some #'identity refined)) (trivia.next:next))
     (apply #'known-send c method tail call i s m
            (or receiver r)
            (loop for r in refined for a in args collect (or r a)))))
  ((known-send :inline (or nil :defer)) (let ((*pass* "Inline")) (inline-known-send node)))
  (_ nil))

;; We can't move around all writes before construction fences in general,
;; but LITERAL and ARGUMENT are pure and can't early publish anything;
;; C2 has a more complex test involving a graph traversal which allows
;; it to move more writes before fences.
(defun write-slot-value-movable-p (value)
  (trivia:match value
    ((or (argument) (literal)) t)
    (_ nil)))

(defun trim-inputs (predicate node)
  "Remove rest inputs from a node which satisfy a predicate."
  (dolist (input (node-%rest-inputs node))
    (when (funcall predicate input) (delete-use node input)))
  (setf (node-%rest-inputs node) (remove-if predicate (node-%rest-inputs node)))
  (node-clear-hash node)
  node)

;; This has to be separate to the usual peepholing (but it doesn't
;; need to be in-place) as REPLACE-NODE can't replace the CODE-STOP;
;; it only knows about node users.
(defun trim-stop-inputs (code)
  (trim-inputs (lambda (n) (typep n 'unreachable)) (code-stop code)))

;; Don't count WRITE-SLOT _to_ the object as a use, and don't count
;; CONSTRUCT-FENCE which is also dead as a use.
(defun new-value-has-interesting-uses-p (node)
  (dolist (use (node-%users node) nil)
    (trivia:match use
      ((write-slot :instance (eq node)))
      ((construct-fence :value (eq node))
       (when (new-value-has-interesting-uses-p use)
         (return-from new-value-has-interesting-uses-p t)))
      (_ (return-from new-value-has-interesting-uses-p t)))))

(defun merge-memory-merges (last second-last bulk)
  (let ((new-aliases (memory-merge-aliases last))
        (new-sets (memory-merge-sets last)))
    ;; Add sets from SECOND-LAST which aren't updated by LAST.
    (loop with overwritten-aliases = (memory-merge-aliases last)
          for alias in (memory-merge-aliases second-last)
          for set in (memory-merge-sets second-last)
          unless (member alias overwritten-aliases)
            do (push alias new-aliases)
               (push set new-sets))
    (apply #'memory-merge nil new-aliases bulk new-sets)))

(defun cull-pointless-splits-from-merge (node)
  (loop with worked = nil
        with bulk = (memory-merge-bulk node)
        for alias in (memory-merge-aliases node)
        for set in (memory-merge-sets node)
        if (trivia:match set
             ((memory-split :memory (eq bulk)) t)
             (_ nil))
          do (setf worked t)
        else collect alias into aliases
             and collect set into sets
        finally (return (if worked (apply #'memory-merge nil aliases bulk sets) nil))))

(defun find-refinement (control instance)
  "Find a refined value for INSTANCE preceding CONTROL, returning it or NIL if we can't find a refinement."
  ;; Check that we could possibly find a refinement before we go off
  ;; doing flow analysis.
  (when (loop for u in (node-%users instance)
                thereis (trivia:match u
                          ((type-test) t)))
    (map-dominators
     (trivia:lambda-match
       ((where (project :name name :tuple test)
               ((type-test :instance (eq instance)) test))
        (return-from find-refinement
          (project nil (ecase name (:yes :value) (:no :no-value)) test))))
     control))
  nil)

(defun refine-nodes (control nodes) (loop for n in nodes collect (or (find-refinement control n) n)))

(defun find-dominating-test (node)
  (trivia:ematch node
    ((fixnum-test :control c :name n :lhs l :rhs r)
     (map-dominators
      (lambda (dom)
        (trivia:match dom
          ((project :tuple (fixnum-test :name (eq n) :lhs (eq l) :rhs (eq r)))
           (return-from find-dominating-test dom))))
      c)
     nil)))

;; We can flatten a region R1 into R2 if R1 is only used by R2 and
;; phi nodes, and all phi nodes are only used by phi nodes of R2.
(defun region-flattenable-p (predecessor-region successor-region)
  (trivia:match predecessor-region
    ((eq successor-region) nil)
    ((region)
     (loop for u in (node-%users predecessor-region)
           always (trivia:match u
                    ((eq successor-region) t)
                    ((phi :control (eq predecessor-region))
                     (loop for u2 in (node-%users u)
                           always (trivia:match u2
                                    ((phi :control (eq successor-region)) t)
                                    (_ nil))))
                    (_ nil))))
    (_ nil)))
