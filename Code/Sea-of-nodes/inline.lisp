(in-package :movement-three)

(defvar *inline-limit* 25)
(defvar *discount-repeat-limit* 4)
(defvar *discount-size-limit* 150)

(defun find-dominator-for-tail-call (node)
  (let ((fallback nil))
    (map-dominators
     (lambda (dom)
       (trivia:match dom
         ((method-start
           :method (eq (known-send-method node))
           :callers (equal (known-send-callers node)))
          (cond
            ((loop for start-arg in (method-start-arguments dom)
                   for start-type = (method-argument-type start-arg)
                   for present-arg in (cons (known-send-receiver node)
                                            (refine-nodes (node-control node) (known-send-arguments node)))
                   for present-type = (widen-away-exactly (result-for type present-arg))
                   unless (type<= present-type start-type)
                     do (mumble "Missed back-arcing " node " to " dom ". ")
                        (mumble present-arg " type " (result-for type present-arg) " is incompatible with " start-arg)
                   always (eq start-type present-type))
             (return-from find-dominator-for-tail-call dom))
            ;; Test again later, we might match up the types after doing
            ;; more peepholing.
            (t (setf fallback :defer))))))
     node)
    fallback))

(defun inline-known-send (n &key (force nil))
  (trivia:ematch n
    ((where (known-send :control c :method m :tail-p tail :callers call
                        :state s :memory mem :receiver r)
            (size (method-size m))
            (depth (+ size (vm-state-inline-depth s))))
     (when (member tail '(t :suppressed))
       (let ((dom (find-dominator-for-tail-call n))
             (arguments (cons r (refine-nodes c (known-send-arguments n)))))
         (trivia:ematch dom
           (nil)
           (:defer
             (cond
               ((not force)
                (setf (known-send-inline n) :defer)
                (return-from inline-known-send nil))
               (t (mumble "Forcefully inlining " n))))
           ((method-start :control dom-control :memory dom-memory)
            (mumble "Found " dom " to back-arc " n " to")
            (add-inputs dom-control c)
            (add-inputs dom-memory mem)
            (loop for input in arguments
                  for arg in (method-start-arguments dom)
                  do (add-inputs arg input))
            (return-from inline-known-send (unreachable nil))))))
     ;; Don't try to inline if the receiver is dead.
     (when (eq (result-for type r) +bottom+) (return-from inline-known-send nil))
     ;; Where'd the dominator go?
     (when (and (eq (known-send-inline n) :defer) (not force))
       (mumble "Dominator for deferred " n " disappeared. I'll wait.")
       (return-from inline-known-send nil))
     ;; Only check the limit if we're going to generate more code -
     ;; back-arcs are cheap enough not to care.
     (when (> depth *inline-limit*)
       (mumble n " pushes into depth " depth " > " *inline-limit*)
       (setf (known-send-inline n) :too-large)
       (return-from inline-known-send nil))
     ;; No loop-back, so compile IR for the method and stitch it in.
     (mumble "Inlining " n)
     (let* ((*emit-tail-sends* tail)
            (*environments* (vm-state-environments s))
            (*callers* call)
            (*inline-depth* depth)
            (arguments (cons r (refine-nodes c (known-send-arguments n))))
            (argument-types
              ;; EXACT types are widened as we might add another
              ;; receiver with only the same layout.
              (loop for a in arguments
                    collect (widen-away-exactly (result-for type a)))))
       (multiple-value-bind (value control memory)
           (compile-method-body m c mem
                                (length arguments)
                                (lambda (n)
                                  (values (nth n arguments) (nth n argument-types))))
         (cond
           ((and (eq tail t) (not (typep control 'unreachable)))
            ;; If we replaced a tail send with something other than a tail send,
            ;; add a RET tail.
            (ret control memory value))
           ((typep control 'unreachable) control)
           (t (pack nil #(:control :value :memory) control value memory))))))))

;; We need to put down a safepoint at the start of a method, and when we
;; produce a loop.
(trivia:defun-ematch method-control-needs-safepoint-p (c)
  ((or (method-control :back-arcs (cons _ _))
       (method-control :control (project :tuple (start))))
   t)
  ((method-control) nil))

(defun delete-unused-scaffolding (code)
  (let ((worked nil))
    ;; First try to remove only scaffolding that no KNOWN-SEND would
    ;; back-arc to. Such scaffolding makes type analysis and peepholes
    ;; too conservative to see that a back-arc can be made, but we want
    ;; to keep the scaffolding for methods that could get back-arced. 
    (let ((methods '()))
      (dolist (n (all-nodes code))
        (trivia:match n
          ((known-send :inline :defer :method m) (pushnew m methods))))
      (dolist (n (all-nodes code))
        (trivia:match n
          ((method-start :method m :control c :vm-state v :memory mem)
           (unless (member m methods)
             (setf worked t)
             (let* ((new-control (apply #'region nil (append (method-control-back-arcs c) (list (node-control c)))))
                    (new-memory (apply #'phi new-control +memory+ (method-argument-values mem)))
                    (safepoint-p (method-control-needs-safepoint-p c)))
               (replace-node c new-control)
               (replace-node mem new-memory)
               (dolist (a (method-start-arguments n))
                 (replace-node a
                               (apply #'phi new-control +top+
                                      (method-argument-values a))))
               (replace-node
                n
                (if safepoint-p
                    (safepoint new-control v new-memory)
                    (pack nil #(:control :memory) new-control new-memory)))))))))
    worked))

(defun force-new-inline-specialisations (code)
  (let ((worked nil))
    ;; No dice - create new versions instead.
    (dolist (n (all-nodes code))
      (trivia:match n
        ((known-send :inline :defer)
         (trivia:match (inline-known-send n :force t)
           (nil)
           (new (replace-node n new) (setf worked t))))))
    worked))

(defun delete-all-inline-scaffolding (code)
  ;; No success, so now remove all scaffolding.
  (let ((process-later '())
        (worked nil))
    (dolist (n (all-nodes code))
        (trivia:match n
          ((or (method-start) (method-argument) (method-control)) (push n process-later))))
      ;; Only unscaffold what was there before; we don't want to remove
      ;; scaffolding produced by inlining just yet.
      ;; XX: this is getting silly, split PROCESS-LATER by type?
      (dolist (n process-later)
        (unless (eq (node-control n) :dead)
          (trivia:match n
            ((method-start :control c :memory m :vm-state v)
             (if (method-control-needs-safepoint-p c)
                 (replace-node n (safepoint c v m))
                 (replace-node n (pack nil #(:control :memory) c m)))
             (setf worked t)))))
      ;; We need to preserve VM-STATEs for METHOD-START, so only replace METHOD-CONTROLs
      ;; afterward.
      (dolist (n process-later)
        (unless (eq (node-control n) :dead)
          (trivia:match n
            ((method-control :control c :back-arcs b)
             ;; We PUSH arguments for back-arcs, so the arguments for the
             ;; dominator control end up last.
             (replace-node n (apply #'region nil (append b (list c))))
             (setf worked t)))))
      (dolist (n process-later)
        (unless (eq (node-control n) :dead)
          (trivia:match n
            ((method-argument :control c :type k :values v)
             (replace-node n (apply #'phi c k v))
             (setf worked t)))))
    worked))

(defun delete-inline-scaffolding (code)
  (or (delete-unused-scaffolding code)
      (force-new-inline-specialisations code)
      (delete-all-inline-scaffolding code)))

(defun inline-discounts (code)
  "Reduce inline depths when the IR is small, to cause more inlining."
  (when (and (< *inline-discounts* *discount-repeat-limit*)
             (< (hash-table-count (code-all-nodes code)) *discount-size-limit*))
    (incf *inline-discounts*)
    (let ((worked nil))
      (dolist (n (all-nodes code) worked)
        (trivia:match n
          ((where (known-send :inline :too-large :state state)
                  ((vm-state :inline-depth d) state))
           (setf (vm-state-inline-depth state) (floor d 2)
                 (known-send-inline n) nil
                 worked t)))))))
