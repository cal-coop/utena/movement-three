(in-package :movement-three)

(defvar *node-id* nil)

;;; Mirror for NODE input info.
(defstruct node-structure settings inputs)
(defvar *node-structures* (make-hash-table))
(defvar *constructor-hook* (constantly nil))

(defstruct node
  (id (or *node-id* (incf (code-next-id *code*))) :type fixnum)
  %hash
  (control nil)
  (%settings (error "settings") :type simple-vector)
  (%inputs (error "inputs") :type simple-vector)
  (%rest-inputs '() :type list)
  (%users '() :type list))

;;; Support for global value numbering - we need to check if two nodes
;;; are similar, and need to hash nodes in a manner consistent with
;;; similarity.

;; Skip the CLASS-OF machinery; SB-KERNEL:LAYOUT-OF gets inlined and
;; specialised too.
(declaim (inline layout-of))
(defun layout-of (x) #+sbcl (sb-kernel:layout-of x) #-sbcl (class-of x))
(defun node= (node1 node2)
  ;; Roughly in order of complexity.
  (and (eq (node-control node1) (node-control node2))
       (eq (layout-of node1) (layout-of node2))
       (every #'eq (node-%inputs node1) (node-%inputs node2))
       (every #'eq (node-%rest-inputs node1) (node-%rest-inputs node2))
       (every #'equal (node-%settings node1) (node-%settings node2))))

(defun node-hash (node)
  (or (node-%hash node)
      (setf (node-%hash node) (node-compute-hash node))))
(defun node-compute-hash (node)
  (declare (node node))
  (let ((hash 0))
    (flet ((update (n)
             (setf hash (logand most-positive-fixnum
                                (logior (* hash 31) n)))))
      (declare (inline update))
      (update (sxhash (layout-of node)))
      (unless (null (node-control node)) (update (node-id (node-control node))))
      (loop for s across (node-%settings node) do (update (sxhash s)))
      (loop for i across (node-%inputs node) do (update (node-id i)))
      (loop for i in (node-%rest-inputs node) do (update (node-id i)))
      hash)))

(sb-ext:define-hash-table-test node= node-hash)

;;; Pattern matching nodes.
;; It wonders why (NODE-OP 42) doesn't work.
(setf trivia:*arity-check-by-test-call* nil)
(trivia:defpattern where (pattern &rest bindings)
    (alexandria:with-gensyms (value)
      `(trivia:guard1 ,value t
                      ,value ,pattern
                      ,@(loop for (binding source) in bindings
                              collect source collect binding))))

(defmethod print-object ((n node) stream)
  (print-unreadable-object (n stream :type t)
    #+(or)
    (format stream "~@[~_control: ~S~]~@[~_settings: ~{~S ~}~]~@[~_in: ~{~S~^~_~}~]"
            (node-control n)
            (coerce (node-%settings n) 'list)
            (append (coerce (node-%inputs n) 'list)
                    (node-%rest-inputs n)))
    (format stream "#~D" (node-id n))))

(defstruct (code (:constructor %make-code (method argument-types return-type)))
  (method nil :read-only t)
  (argument-types nil :read-only t)
  (return-type nil :read-only t)
  arguments
  ;; These nodes are singletons, almost always used and thus get pre-allocated.
  (next-id 3 :type fixnum)
  (start (let ((*node-id* 1)) (start nil)))
  (stop (let ((*node-id* 2)) (stop nil)))
  (immutable-memory (let ((*node-id* 3)) (immutable-memory nil)))
  ;; Not all nodes will be in GVN, but they will all be in ALL-NODES. Boggle.
  (all-nodes (make-hash-table))
  (gvn (make-hash-table :test 'node=))
  (cfg nil)
  ;; Analyses.
  (types (make-analysis-table))
  (dominators (make-analysis-table))
  (paths (make-analysis-table)))

(defun make-code (method argument-types return-type)
  (let ((c (%make-code method argument-types return-type)))
    (flet ((mark (node) (setf (gethash node (code-all-nodes c)) t)))
      (mark (code-start c))
      (mark (code-stop c))
      (mark (code-immutable-memory c))
      (let ((*code* c))
        (setf (code-arguments c)
              (loop for n below (length argument-types)
                    collect (argument nil n)))))
    c))

(defun immutable () (code-immutable-memory *code*))

(defun node-clear-hash (node)
  (unless (null (node-%hash node))
    (remhash node (code-gvn *code*))
    (setf (node-%hash node) nil)))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun expand-pattern-body (name patterns presents access)
    (alexandria:with-gensyms (instance-name)
      `(trivia:guard1 (,instance-name :type ,name)
                      (typep ,instance-name ',name)
                      ,@(loop for pattern in patterns
                              for present in presents
                              for a in access
                              when present
                                collect (funcall a instance-name)
                                and collect pattern))))
  (defun expand-defpattern (name settings required-inputs rest-input)
    (let ((control-present (gensym "CONTROL-PRESENT"))
          (settings-present (loop for nil in settings collect (gensym "PRESENT")))
          (inputs-present (loop for nil in required-inputs collect (gensym "PRESENT")))
          (rest-present (gensym "REST-PRESENT")))
      `(trivia:defpattern ,name (&key (control nil ,control-present)
                                      ,@(loop for s in settings
                                              for p in settings-present
                                              collect `(,s nil ,p))
                                      ,@(loop for i in required-inputs
                                              for p in inputs-present
                                              collect `(,i nil ,p))
                                      ,@(if (null rest-input) '()
                                            `((,rest-input nil ,rest-present))))
         (expand-pattern-body ',name
                              (list control ,@settings ,@required-inputs
                                    ,@(if (null rest-input) '() (list rest-input)))
                              (list ,control-present ,@settings-present ,@inputs-present
                                    ,@(if (null rest-input) '() (list rest-present)))
                              (list (lambda (n) `(node-control ,n))
                                    ,@(loop for s in settings
                                            for i from 0
                                            collect `(lambda (n) `(svref (node-%settings ,n) ,,i)))
                                    ,@(loop for s in required-inputs
                                            for i from 0
                                            collect `(lambda (n) `(svref (node-%inputs ,n) ,,i)))
                                    ,@(if (null rest-input) '() `((lambda (n) `(node-%rest-inputs ,n)))))))))
  (defun expand-accessors (type settings required-inputs rest-input)
    (flet ((accessor-name (name)
             (alexandria:format-symbol :movement-three "~A-~A" type name))
           (make-accessors (source names mutable)
             (loop for n from 0
                   for name in names
                   for accessor-name = (alexandria:format-symbol :movement-three "~A-~A" type name)
                   collect `(declaim (inline ,accessor-name (setf ,accessor-name)))
                   collect `(defun ,accessor-name (,type)
                              (check-type ,type ,type)
                              (svref (,source ,type) ,n))
                   when mutable
                     collect `(defun (setf ,accessor-name) (new-value ,type)
                                (check-type ,type ,type)
                                (node-clear-hash ,type)
                                (setf (svref (,source ,type) ,n) new-value)))))
      (append
       (make-accessors 'node-%settings settings t)
       (make-accessors 'node-%inputs required-inputs nil)
       (if (null rest-input)
           '()
           (let ((rest-name (accessor-name rest-input)))
             `((declaim (inline ,rest-name (setf ,rest-name)))
               (defun ,rest-name (,type)
                 (check-type ,type ,type)
                 (node-%rest-inputs ,type))))))))
  (defun expand-constructor (%constructor constructor settings inputs assertions)
    (alexandria:with-gensyms (new rest)
      (multiple-value-bind (required-inputs opt rest-input)
          (alexandria:parse-ordinary-lambda-list inputs)
        (assert (endp opt))
        `(defun ,constructor (control ,@settings ,@inputs)
           ,@assertions
           (let ((,new
                   (,%constructor :control control
                                  :%settings (vector ,@settings)
                                  :%inputs (vector ,@required-inputs)
                                  ,@(if (null rest-input)
                                        '()
                                        `(:%rest-inputs (copy-list ,rest-input))))))
             (unless (null control) (push ,new (node-%users control)))
             ,@(loop for input in required-inputs
                     collect `(unless (null ,input) (push ,new (node-%users ,input))))
             ,(if (null rest-input)
                  '(progn)
                  `(dolist (,rest ,rest-input) (push ,new (node-%users ,rest))))
             ;; Store for easy access by ALL-NODES. How far is this from
             ;; allowing hash consing/GVN?
             (unless (null *code*)
               (setf (gethash ,new (code-all-nodes *code*)) t))
             (funcall *constructor-hook* ,new)
             ,new))))))

(defmacro define-instruction (name settings inputs &body assertions)
  (let ((%constructor (alexandria:format-symbol :movement-three "%~A" name)))
    (multiple-value-bind (required-inputs opt rest-input)
        (alexandria:parse-ordinary-lambda-list inputs)
      (assert (endp opt))
      `(progn
         (defstruct (,name (:include node) (:constructor ,%constructor) (:copier nil)))
         (setf (gethash ',name *node-structures*)
               (make-node-structure :settings ',settings :inputs ',required-inputs))
         ,(expand-constructor %constructor name settings inputs assertions)
         ,@(expand-accessors name settings required-inputs rest-input)
         ,(expand-defpattern name settings required-inputs rest-input)))))

(defmacro with-node-pinned ((node) &body body)
  (alexandria:once-only (node)
    `(progn
       (push :pinned (node-%users ,node))
       (unwind-protect
            (progn ,@body)
         (delete-use :pinned ,node)))))

(defun node-mortal-p (node)
  (not (or (start-p node) (stop-p node) (immutable-memory-p node))))

(defun kill (node)
  (assert (and (endp (node-%users node)) (node-mortal-p node)))
  (remhash node (code-all-nodes *code*))
  (node-clear-hash node)
  (unless (null (node-control node))
    (delete-use node (node-control node)))
  (setf (node-control node) :dead)
  (loop for i across (node-%inputs node) do (delete-use node i))
  (setf (node-%inputs node) #())
  (dolist (i (node-%rest-inputs node)) (delete-use node i))
  (setf (node-%rest-inputs node) '()))

;;; SBCL has SB-INT:DELQ for (DELETE value list :TEST #'EQ) but adding
;;; :COUNT 1 falls back to generic DELETE.
(defun delete-first-eq (value list)
  (cond
    ((endp list) '())
    ;; We can't splice anything if the first element is EQ.
    ((eq (first list) value) (rest list))
    (t
     ;; See SB-INT:DELQ
     (do ((cell list (rest cell))
          (previous nil cell))
         ((endp cell) list)
       (when (eq value (first cell))
         (setf (rest previous) (rest cell))
         (return list))))))

(defun delete-use (user node)
  (unless (null node)
    (setf (node-%users node) (delete-first-eq user (node-%users node)))
    (when (and (endp (node-%users node)) (node-mortal-p node)) (kill node))
    (list node)))

(defun replace-node (old new)
  (assert (not (eq new old)))
  (let ((users (node-%users old)))
    (dolist (user users)
      (node-clear-hash user)
      (when (eq (node-control user) old) (setf (node-control user) new))
      (setf (node-%inputs user) (nsubstitute new old (node-%inputs user) :test #'eq)
            (node-%rest-inputs user) (nsubstitute new old (node-%rest-inputs user) :test #'eq)))
    ;; APPEND copies USERS but not (NODE-%USERS NEW).
    (setf (node-%users new) (append users (node-%users new)))
    ;; Delete the users from OLD only after adding all users to NEW, to
    ;; avoid killing live nodes which would be unused if we didn't use
    ;; this order.
    (invalidate (node-%users old))
    (setf (node-%users old) '())
    (unify-analyses old new)
    (kill old)
    users))

(defun replace-uses (users old new)
  (invalidate users)
  (dolist (user users)
    (setf (node-%inputs user) (nsubstitute new old (node-%inputs user) :test #'eq)
          (node-%rest-inputs user) (nsubstitute new old (node-%rest-inputs user) :test #'eq)))
  (flet ((affected-p (node) (member node users)))
    (setf (node-%users new) (nconc (remove-if-not #'affected-p (node-%users old)) (node-%users new))
          (node-%users old) (remove-if #'affected-p (node-%users old))))
  (when (endp (node-%users old)) (kill old)))

(defun add-inputs (node &rest inputs)
  ;; Adding inputs generally could break monotone analyses; only some
  ;; nodes cannot care if their inputs change (e.g. STOP), or pessimise to
  ;; allow modification (e.g. METHOD-ARGUMENT).
  (check-type node (or stop method-control method-argument))
  ;; PUSH in DOLIST reverses the list, so reverse again to preserve order.
  (dolist (input (reverse inputs))
    (push input (node-%rest-inputs node))
    (push node (node-%users input)))
  (invalidate (node-%users node))
  (node-clear-hash node)
  (list node))

(defun all-nodes (code) (alexandria:hash-table-keys (code-all-nodes code)))

;;; The boring high-level instructions.
(define-instruction argument (index) ())
(define-instruction crash (cause) ())
(define-instruction eq-test () (lhs rhs))
(define-instruction fixnum-binary-op (name) (lhs rhs))
(define-instruction fixnum-unary-op (name) (value))
(define-instruction fixnum-test (name) (lhs rhs))
(define-instruction known-send (method tail-p callers inline) (state memory receiver &rest arguments))
(define-instruction literal (value) ())
(define-instruction pack (keys) (&rest values))
;;; We keep a pessimistic type which is known to be safe, for quickly
;;; checking if we're looking at memory or data flow.
(define-instruction phi (known-type) (&rest values)
  (assert (= (length values) (length (region-predecessors control)))))
(define-instruction project (name) (tuple))
(defmethod print-object ((p project) stream)
  (print-unreadable-object (p stream :type t)
    (format stream "~A ~W" (project-name p) (project-tuple p))))
(define-instruction region () (&rest predecessors))
(define-instruction ret () (memory value))
(define-instruction safepoint () (state memory))
(define-instruction start () ())
(define-instruction stop () (&rest controls))
(define-instruction tls () ())
(define-instruction type-test (type) (instance))
(define-instruction unknown-send (access tail-p callers) (state memory name receiver &rest arguments))
(define-instruction unreachable () ())
(define-instruction vm-state (inline-depth) (&rest environments))

;;; The boring mid-level instructions.
;; Scheduling the sea-of-nodes requires that we fence loads to occur
;; before stores.
(define-instruction anti-dependency-fence () (memory &rest reads))
;; A literal value which can fit as an immediate for an instruction.
(define-instruction literal-immediate (value) ())
;; SAFEPOINT is exploded into an = FIXNUM-TEST on a slot of TLS, then a
;; PERFORM-SAFEPOINT predicated on the safepoint being requested.
(define-instruction perform-safepoint () (state memory))

;;; Memory.

;; Prevent publication of an object with uninitialised slots.
;; We may push instructions from after to before the CONSTRUCT-FENCE,
;; but not vice versa.
(define-instruction construct-fence () (memory value))
;; An allocation which is never used by anything other than writes
;; to the allocation.
(define-instruction dead-new () ())
(define-instruction immutable-memory () ())
;; Alias analysis: a MEMORY-SPLIT peels out an equivalence set from
;; memory, and a MEMORY-MERGE puts the set back if the set is
;; modified.
(define-instruction memory-merge (aliases) (bulk &rest sets))
(define-instruction memory-split (alias) (memory))
;; Multiple NEWs fused by allocation folding. All slots must be
;; initialised by WRITE-SLOT; this node only is introduced in CFG
;; after lowering of immutable NEW is done in Lower.
(define-instruction multi-new (types) (memory))
;; Immutable slot values are inputs to NEW, mutable slots must be
;; initialised by WRITE-SLOT.
(define-instruction new (type) (memory &rest values))
(define-instruction read-slot (index) (memory instance))
(define-instruction write-slot (index) (memory instance new-value))

;;; Support for making loops out of tail calls.
;; We need to stop the compiler from getting too optimistic about
;; arguments, when we might introduce more predecessors later and
;; invalidate such optimism. Instead we fix the types according to the
;; calling convention, until DELETE-INLINE-SCAFFOLDING removes the
;; arguments and starts, which prevents making more loops.
(define-instruction method-argument (type index) (&rest values))
(define-instruction method-control () (&rest back-arcs))
(define-instruction method-start (method callers) (vm-state memory &rest arguments))
