(in-package :movement-three)

(defstruct (compilation-unit
            (:constructor %make-compilation-unit (compile-arguments)))
  (compile-arguments '())
  (functions (make-hash-table :test 'equal))
  (unknown-send-functions (make-hash-table :test 'equal))
  (dispatch-functions (make-hash-table :test 'equal))
  (worklist '()))
(defmethod print-object ((unit compilation-unit) stream)
  (print-unreadable-object (unit stream :type t)
    (format stream "~D function~:P, ~D in worklist"
            (hash-table-count (compilation-unit-functions unit))
            (length (compilation-unit-worklist unit)))))

(defvar *compilation-unit*)

(defun map-functions (function)
  (flet ((map-table (table)
           (maphash (lambda (key info) (declare (ignore key)) (funcall function info))
                    table)))
    (map-table (compilation-unit-functions *compilation-unit*))
    (map-table (compilation-unit-unknown-send-functions *compilation-unit*))
    (map-table (compilation-unit-dispatch-functions *compilation-unit*))))

(defstruct (function-info (:conc-name fun-))
  method
  argument-types
  return-type
  code)

(defstruct (runtime-method (:constructor runtime-method (name))) name)
(defmethod print-object ((m runtime-method) stream)
  (print-unreadable-object (m stream)
    (format stream "Runtime method ~A" (runtime-method-name m))))

(defvar *crash-primitive* (runtime-method 'crash))
(defvar *safepoint-primitive* (runtime-method 'safepoint))
(defvar *unknown-send-primitive* (runtime-method 'unknown-send))

(defun find-function (method argument-types &key (create-if-missing nil))
  (let ((key (list argument-types method)))
    (multiple-value-bind (fun present)
        (gethash key (compilation-unit-functions *compilation-unit*))
      (cond
        (present (values fun :exists))
        (create-if-missing
         (let ((info (make-function-info
                      :method method
                      :argument-types argument-types
                      :return-type (method-return-type method (first argument-types) (rest argument-types)))))
           (setf (gethash key (compilation-unit-functions *compilation-unit*))
                 info)
           (push info (compilation-unit-worklist *compilation-unit*))
           (values info :new)))
        (t (error "No method named ~A with specialisation ~A" method argument-types))))))

(defun find-dispatch-function (name arity)
  (let ((key (list name arity)))
    (or (gethash key (compilation-unit-dispatch-functions *compilation-unit*))
        (setf (gethash key (compilation-unit-dispatch-functions *compilation-unit*))
              (make-function-info
               :method (runtime-method (list 'dispatch name arity))
               :argument-types (make-list (+ arity 1) :initial-element +top+)
               :return-type +top+)))))

(defun find-unknown-send-function (arity)
  (or (gethash arity (compilation-unit-unknown-send-functions *compilation-unit*))
      (setf (gethash arity (compilation-unit-unknown-send-functions *compilation-unit*))
            (make-function-info
             :method (runtime-method (list 'unknown-send arity))
             :argument-types (make-list (+ arity 2) :initial-element +top+)
             :return-type +top+))))

(defun make-compilation-unit (&key (compile-arguments '()))
  (let ((*compilation-unit* (%make-compilation-unit compile-arguments)))
    (flet ((add-primitive (prim)
             (setf (gethash (list '() prim)
                            (compilation-unit-functions *compilation-unit*))
                   (make-function-info :method prim :argument-types '() :return-type +bottom+))))
      (add-primitive *safepoint-primitive*)
      (add-primitive *crash-primitive*))
    (setf (compilation-unit-worklist *compilation-unit*) '())
    *compilation-unit*))

(defun process-work-list (compilation-unit)
  (loop until (endp (compilation-unit-worklist compilation-unit))
        do (let ((f (pop (compilation-unit-worklist compilation-unit))))
             (when (null (fun-code f))
               (setf (fun-code f)
                     (apply #'compile
                            (compile-method (fun-method f) (fun-argument-types f))
                            (compilation-unit-compile-arguments compilation-unit)))))))
