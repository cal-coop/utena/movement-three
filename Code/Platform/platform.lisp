(in-package :movement-three)

(defvar *platform*
  (haaden-two.platform:new
   (asdf:system-relative-pathname :movement-three "Platform/platform.sour")))

(defvar *symbol-class* (haaden-two:send-message 'utena::symbol *platform*))
(defvar *symbol-type* (instance<-class-type (wider-type-of *symbol-class*)))
(defmethod wider-type-of ((s symbol)) *symbol-type*)

(defvar *string-class* (haaden-two:send-message 'utena::string *platform*))
(defvar *string-type* (instance<-class-type (wider-type-of *string-class*)))
(defmethod wider-type-of ((s string)) *string-type*)

(defvar *true* (haaden-two:send-message 'utena::true *platform*))
(defvar *false* (haaden-two:send-message 'utena::false *platform*))

(setf *tls-type* (instance<-class-type (wider-type-of (haaden-two:send-message 'utena::tls *platform*))))
(defconstant +tls-safepoint-index+ 1)

(defmacro with-platform (() &body body)
  `(let ((haaden-two:*true* *true*)
         (haaden-two:*false* *false*))
     ,@body))
