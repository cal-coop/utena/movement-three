(in-package :movement-three)

;;; Write barrier elimination is done on the CFG, which doesn't
;;; quite permit a sparse analysis like we do for sea-of-nodes -
;;; the possible ages of a node can change via control flow,
;;; whereas e.g. the type of a node in SoN is flow-insensitive and
;;; flow-sensitivity is introduced through other nodes which
;;; explicitly refine their inputs.

(defstruct (known-generations (:constructor make-known-generations (old young)))
  (old '() :read-only t) (young '() :read-only t))

(defun merge-known-generations (kg1 kg2)
  (make-known-generations
   (union-sorted (known-generations-old kg1) (known-generations-old kg2))
   (union-sorted (known-generations-young kg1) (known-generations-young kg2))))

(defun known-generations= (kg1 kg2)
  (and (equal (known-generations-old kg1) (known-generations-old kg2))
       (equal (known-generations-young kg1) (known-generations-young kg2))))

(defun age-everything (kg)
  (make-known-generations
   (union-sorted (known-generations-old kg) (known-generations-young kg))
   '()))

(defconstant +unknown-gen+ 0)
(defconstant +young-gen+ 1)
(defconstant +old-gen+ 2)
(defconstant +both-gens+ 3)

(defun lookup-known-generations (node kg)
  (let ((young (member node (known-generations-young kg)))
        (old (member node (known-generations-old kg))))
    (logior (if young +young-gen+ +unknown-gen+)
            (if old +old-gen+ +unknown-gen+))))

(defun extend-known-generations (node env value)
  (flet ((insert (mask list)
           (if (logtest value mask) (insert-into-sorted node list) list)))
    (make-known-generations
     (insert +old-gen+ (remove-from-sorted node (known-generations-old env)))
     (insert +young-gen+ (remove-from-sorted node (known-generations-young env))))))

(defun age-union (age1 age2) (logior age1 age2))
(defun ages-to-string (age)
  (flet ((test (flag result) (if (logtest age flag) result "-")))
    (concatenate 'string (test +young-gen+ "Y") (test +old-gen+ "O"))))

(defun node-initial-generation (node env)
  (trivia:ematch node
    ((argument) +both-gens+)
    ((construct-fence :value v) (lookup-known-generations v env))
    ((project :tuple (eq-test :lhs l :rhs r))
     (age-union (lookup-known-generations l env) (lookup-known-generations r env)))
    ((project :tuple (type-test :instance i)) (lookup-known-generations i env))
    ((project :name :value :tuple (or (fixnum-binary-op) (fixnum-unary-op))) +unknown-gen+)
    ((project :name :value :tuple (known-send)) +both-gens+)
    ((known-send) +unknown-gen+)
    ((literal :value v) (if (integerp v) +unknown-gen+ +both-gens+))
    ((literal-immediate) +unknown-gen+)
    ((or (multi-new) (new)) +unknown-gen+)
    ((project :tuple (or (multi-new) (new))) +young-gen+)
    ((phi :values v) (reduce #'age-union v :key (lambda (v) (lookup-known-generations v env))))
    ((read-slot) +both-gens+)
    ((tls) +both-gens+)
    ((project :tuple (type-test :instance i)) (lookup-known-generations i env))))

(defun node-transfer-write-barrier (node pred)
  (trivia:match node
    ((or (known-send) (multi-new) (new))
     (extend-known-generations node (age-everything pred) (node-initial-generation node pred)))
    (_ (extend-known-generations node pred (node-initial-generation node pred)))))

(defun compute-nodes-involved-in-writes (code)
  (let ((seen (make-hash-table)))
    (labels ((mark (n) (setf (gethash n seen) t))
             (walk (n)
               (unless (gethash n seen)
                 (mark n)
                 (trivia:match n
                   ((construct-fence :value v) (walk v))
                   ((project :tuple (eq-test :lhs l :rhs r)) (walk l) (walk r))
                   ((project :tuple (type-test :instance i)) (walk i))
                   ((phi :values v) (map nil #'walk v))))))
      (dolist (n (all-nodes code))
        (trivia:match n
          ;; We only need to track generations of nodes which
          ;; which will end up involved in WRITE-SLOT nodes.
          ((write-slot :instance i :new-value v)
           (unless (type<= (result-for type v) +fixnum+)
             (walk i) (walk v)))
          ;; These nodes have effects, don't filter them out.
          ((or (known-send) (multi-new) (new)) (mark n))))
      #+(or)
      (format *debug-io* "Filtered ~D of ~D nodes~%"
              (hash-table-count seen)
              (length (all-nodes code)))
      seen)))

(defun compute-write-generations (code)
  (let ((before-writes (make-hash-table))
        (filter (compute-nodes-involved-in-writes code)))
    (cfg-analysis (lambda (i s)
                    (cond
                      ((write-slot-p i)
                       (unless (type<= (result-for type (write-slot-new-value i)) +fixnum+)
                         (setf (gethash i before-writes)
                               (cons (lookup-known-generations (write-slot-instance i) s)
                                     (lookup-known-generations (write-slot-new-value i) s))))
                       s)
                      ((gethash i filter) (node-transfer-write-barrier i s))
                      (t s)))
                  #'merge-known-generations
                  (make-known-generations '() '())
                  #'known-generations=
                  code)
    before-writes))
