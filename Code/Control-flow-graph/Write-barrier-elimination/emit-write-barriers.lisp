(in-package :movement-three)

(defvar *allocate-logged* t)

;;; Track all logged nodes prior to each write.
(defvar *before-writes*)
(defun previously-logged-transfer (n p)
  (trivia:match n
    ((construct-fence :value v) (if (member v p) (insert-into-sorted n p) p))
    ((known-send) '())
    ;; New objects are allocated logged, and allocation
    ;; can cause GC, so nothing else is definitely logged.
    ((multi-new)
     (if *allocate-logged*
         (sort (copy-list (node-%users n)) #'< :key #'node-id)
         '()))
    ((new) (if *allocate-logged* (list n) '()))
    ((write-slot :instance i :new-value v)
     (cond
       ((type<= v +fixnum+) p)
       (t
        (setf (gethash n *before-writes*) p)
        (insert-into-sorted i p))))
    (_ p)))

(defun previously-logged-analysis (code)
  (let ((*before-writes* (make-hash-table)))
    (cfg-analysis #'previously-logged-transfer
                  #'intersect-sorted
                  '()
                  #'alexandria:set-equal
                  code)
    *before-writes*))

(defun previously-logged-per-basic-block (code)
  (let ((*before-writes* (make-hash-table)))
    (bb-analysis #'previously-logged-transfer '() code)
    *before-writes*))

(define-time-meter *write-barrier-meter* assembly emit-write-barriers)
(define-meter *writes-seen* assembly emit-write-barriers seen)
(define-meter *logging-dfa-eliminates* assembly emit-write-barriers logged-before-dfa)
(define-meter *logging-bb-eliminates* assembly emit-write-barriers logged-before-basic-block)
(define-meter *generation-eliminates* assembly emit-write-barriers generations)
(define-meter *bb-eliminates* assembly emit-write-barriers same-basic-block)

(defun emit-write-barriers-from-logging (logged-states)
  (loop with needs-barriers = (make-hash-table)
        for i being the hash-keys of logged-states
        for state being the hash-values of logged-states
        unless (member (write-slot-instance i) state)
          do (setf (gethash i needs-barriers) t)
        finally (return needs-barriers)))

(defun emit-write-barriers-from-generations (write-info)
  (loop with needs-barriers = (make-hash-table)
        for i being the hash-keys of write-info
        for (target-gen . value-gen) being the hash-values of write-info
        do (trivia:ematch i
             ((write-slot :index index :instance target :new-value value)
              ;; We should come up with some generations for all values.
              (assert (/= +unknown-gen+ target-gen))
              (assert (/= +unknown-gen+ value-gen))
              ;; No barrier is needed when we always write to young, or never write
              ;; a young value.
              (if (or (= target-gen +young-gen+)
                      (not (logtest value-gen +young-gen+)))
                  (meter-tick *generation-eliminates*)
                  (setf (gethash i needs-barriers) t))
              #+(or)
              (format *debug-io* "Writing ~A:~D ~A <- ~A ~A~%"
                      target i (ages-to-string target-gen)
                      value (ages-to-string value-gen))
              ;; no (DECLARE (IGNORABLE ...)) in TRIVIA:MATCH, womp womp
              #-(or) (progn index target value)))
        finally (return needs-barriers)))

(defun insert-write-barriers (code)
  (with-timing (*write-barrier-meter*)
    (let ((known-logged-analysis (previously-logged-analysis code))
          (known-logged-bb (previously-logged-per-basic-block code))
          (known-generations (compute-write-generations code)))
      (declare (ignore known-generations))
      (dolist (n (all-nodes code))
        (trivia:match n
          ((write-slot :control c :instance i :new-value v)
           (unless (type<= (result-for type v) +fixnum+)
             (meter-tick *writes-seen*)
             (when (member i (gethash n known-logged-analysis))
               (meter-tick *logging-dfa-eliminates*))
             (when (member i (gethash n known-logged-bb))
               (meter-tick *logging-bb-eliminates*))
             ;; Spot allocations in the same basic block, alike OpenJDK.
             (trivia:match i
               ((or (project :tuple (new :control (eq c))) (project :tuple (multi-new :control (eq c))))
                (meter-tick *bb-eliminates*)))))))
      (setf (cfg-barriers-needed (code-cfg code))
            (emit-write-barriers-from-logging known-logged-analysis)))
    (unless *do-allocation-folding*
      ;; Count immutable-New inputs as eliminated writes.
      (dolist (n (all-nodes code))
        (trivia:match n
          ((new :values v)
           (meter-tick *writes-seen* (length v))
           (dolist (m (list *logging-dfa-eliminates* *logging-bb-eliminates*
                            *bb-eliminates* *generation-eliminates*))
             ;; When allocating-as-logged, all barriers are elided as we
             ;; write to a new object. Else all but the first barrier are elided.
             (meter-tick m (- (length v) (if *allocate-logged* 0 1))))))))))
