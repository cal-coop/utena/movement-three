(in-package :movement-three)
;;; A64 registers
(defvar *registers* (coerce (loop with names = '("tls" "fp" "lr" "sp")
                                  for n below 32
                                  for name = (if (>= n 28) (nth (- n 28) names) (format nil "x~D" n))
                                  collect (%register n name))
                            'vector))
(defconstant +register-count+ 32)
(defun register (n) (aref *registers* n))
;; Stack
(defvar arm:*sp* (register 31))
(defvar arm:*lr* (register 30))
(defvar arm:*fp* (register 29))
(defvar arm:*tls* (register 28))
;; Temporaries for restoring spilled values
(defvar arm:*temporaries* (loop for n from 25 to 27 collect (register n)))
;; GPRs
(defvar arm:*gprs*
  (loop for n upto 24
        ;; Don't allocate IP0, IP1
        unless (member n '(16 17))
          collect (register n) into regs
        finally (return (coerce regs 'vector))))

;;; Store arguments into the virtual registers of ARGUMENT nodes.
;;; ARGUMENTs might not be able to be allocated to the registers used
;;; by the calling convention for arguments.
(defun pre-move-arguments ()
  (let ((start-bb (aref (cfg-blocks-by-schedule (code-cfg *code*)) 0)))
    (loop for a in (code-arguments *code*)
          for n from 0
          do (push (inst 'arm:mov (vreg-for-node a) (aref arm:*gprs* n))
                   (basic-block-instructions start-bb)))))

(defun node-to-instructions (node)
  (labels ((my-vreg () (vreg-for-node node))
           (mov-result (tail inst)
             "Move the result of a call into the right register."
             (if (eq tail 't) (list inst) (list inst (inst 'arm:mov (my-vreg) (aref arm:*gprs* 0)))))
           (mov-arguments (arguments)
             "Move the arguments to a call into the right registers."
             (loop for n from 0 for a in arguments
                   collect (inst 'arm:mov (aref arm:*gprs* n) (vreg-for-node a)))))
    (trivia:ematch node
      ;; no-ops
      ((or (anti-dependency-fence) (argument)
           (immutable-memory) (memory-merge) (memory-split)
           (region) (start) (vm-state))
       '())
      ;; bypassed by VREG-FOR-NODE
      ((or (construct-fence) (literal-immediate)
           (project :name 0 :tuple (multi-new))
           (project :name (or :no-value :value) :tuple (or (eq-test) (type-test)))
           (project :name :value
                    :tuple (or (fixnum-binary-op) (fixnum-unary-op) (unknown-send) (known-send)))
           (tls))
       '())
      ((crash) (list (inst 'arm:b (find-function *crash-primitive* '()))))
      ((eq-test :lhs l :rhs r)
       (list (inst 'arm:cmp (vreg-for-node l) (vreg-for-node r))
             (inst 'arm:test 'arm:beq 'arm:bne)))
      ((fixnum-binary-op :name name :lhs l :rhs r)
       (ecase name
         ;; These operations preserve the tag bit.
         ((+ - logand logior logxor)
          (list (inst (ecase name
                        (+ 'arm:add) (- 'arm:sub)
                        (logand 'arm:and) (logior 'arm:orr) (logxor 'arm:eor))
                      (my-vreg) (vreg-for-node l) (vreg-for-node r))))
         ((*)
          ;; Shift R right - we want to compute 2L * R = 2LR, not 2L * 2R = 4LR
          (list (inst 'arm:asr (my-vreg) (vreg-for-node r) 1)
                (inst 'arm:mul (my-vreg) (vreg-for-node l) (my-vreg))))
         ((floor)
          ;; Shift the result left - we want to compute 2(2L / 2R) = 2(L / R)
          (list (inst 'arm:div (my-vreg) (vreg-for-node l) (vreg-for-node r))
                (inst 'arm:lsl (my-vreg) (my-vreg) 1)))))
      ((fixnum-test :name name :lhs l :rhs r)
       (list (inst 'arm:cmp (vreg-for-node l) (vreg-for-node r))
             (apply #'inst 'arm:test
                    (ecase name
                      (< '(arm:blt arm:bge)) (<= '(arm:ble arm:bgt)) (= '(arm:beq arm:bne))
                      (/= '(arm:bne arm:beq)) (>= '(arm:bge arm:blt)) (> '(arm:bgt arm:ble))))))
      ((fixnum-unary-op :name name :value v)
       (ecase name
         ((lognot)
          ;; NOT will flip the tag bit, so we follow up AND with -2
          ;; i.e. ...111110 to re-clear the tag bit.
          (list (inst 'arm:not (my-vreg) (vreg-for-node v))
                (inst 'arm:and (my-vreg) (my-vreg) -2)))))
      ((known-send :method m :tail-p tail :receiver r)
       (append (mov-arguments (cons r (known-send-arguments node)))
               (mov-result tail (inst (if (eq tail 't) 'arm:b 'arm:bl) m))))
      ((literal :value v) (list (inst 'arm:mov (my-vreg) (immediate v))))
      ((multi-new :types types) (list (apply #'inst 'arm:alloc (my-vreg) types)))
      ((new :type type :values vs)
       (cons (inst 'arm:alloc (my-vreg) type)
             (loop for v in vs
                   for n from 0
                   collect (inst 'arm:str (vreg-for-node v) (my-vreg) (slot-offset n)))))
      ((perform-safepoint) (list (inst 'arm:bl (find-function *safepoint-primitive* '()))))
      ((project :name i :tuple (and multi (multi-new :types types)))
       (list (inst 'arm:add (my-vreg) (vreg-for-node multi) (reduce #'+ (subseq types 0 i) :key #'record-bytes))))
      ;; Other PROJECTs are no-ops
      ((project) '())
      ((read-slot :index n :instance i) (list (inst 'arm:ldr (my-vreg) (vreg-for-node i) (slot-offset n))))
      ((ret :value v) (list (inst 'arm:mov (aref arm:*gprs* 0) (vreg-for-node v)) (inst 'arm:ret)))
      ((type-test :type (eq +fixnum+) :instance i)
       (list (inst 'arm:test 'arm:tbz 'arm:tbnz (vreg-for-node i) 0)))
      ((type-test :type type :instance i)
       ;; LOWER-NODE inserts a fixnum test before, so just check the
       ;; layout ID.
       (list (inst 'arm:test
                   'arm:branch-instance-of 'arm:branch-not-instance-of
                   (vreg-for-node i) type)))
      ((unknown-send :tail-p tail :name n :receiver r)
       (append (mov-arguments (list* n r (unknown-send-arguments node)))
               (mov-result tail (inst (if (eq tail 't) 'arm:b 'arm:bl) 'unknown-send))))
      ((write-slot :index n :instance i :new-value v)
       (append (list (inst 'arm:str (vreg-for-node v) (vreg-for-node i) (slot-offset n)))
               (if (gethash node (cfg-barriers-needed (code-cfg *code*)))
                   (list (inst 'arm:write-barrier (vreg-for-node i)))
                   '()))))))

(defun instruction-uses-defs (instruction)
  (case (instruction-name instruction)
    ((arm:cmp arm:str arm:stp
      arm:tbz arm:tbnz arm:test)
     (values (instruction-arguments instruction) '()))
    ((arm:ret) (values '() '()))
    ((arm:ldp)
     (destructuring-bind (v1 v2 src offset) (instruction-arguments instruction)
       (values (list src offset) (list v1 v2))))
    ;; Kinda? We always generate moves for arguments/return values
    ;; around calls but this seems cheeky.
    ((arm:b arm:bl arm:blt arm:ble arm:beq arm:bne arm:bge arm:bgt) (values '() '()))
    ((arm:write-barrier) (values (instruction-arguments instruction) '()))
    (otherwise
     (destructuring-bind (dest . src) (instruction-arguments instruction)
       (values src (list dest))))))

(defun reload-store-instruction (instruction intervals allocation)
  (labels ((normal ()
             (multiple-value-bind (uses defs) (instruction-uses-defs instruction)
               ;; Load used values before the instruction, and store defined values
               ;; after the instruction.
               (let* ((use-temps (loop with temps = arm:*temporaries*
                                       for u in uses
                                       when (stack-location-p u)
                                         collect (cons u (pop temps))))
                      (def-temps (loop with temps = arm:*temporaries*
                                       for d in defs
                                       when (stack-location-p d)
                                         collect (cons d (pop temps)))))
                 (setf (instruction-arguments instruction)
                       (sublis (append use-temps def-temps)
                               (instruction-arguments instruction)))
                 (append (loop for (loc . temp) in use-temps
                               collect (inst 'arm:ldr temp arm:*sp* (stack-offset loc)))
                         (list instruction)
                         (loop for (loc . temp) in def-temps
                               collect (inst 'arm:str temp arm:*sp* (stack-offset loc))))))))
    (trivia:match instruction
      ((instruction :name 'arm:bl)
       ;; Push and pop values in registers to the stack.
       (let* ((affected-registers
                (loop for i in intervals
                      for allocated = (gethash (interval-vreg i) allocation)
                      when (and (interval-intersects-p i (1- (instruction-index instruction)))
                                (register-p allocated))
                        collect allocated))
              ;; ARM64 requires that SP is 16-byte aligned.
              (push-amount (* 16 (ceiling (length affected-registers) 2))))
         (flet ((move-affected (single pair)
                  (let ((offset 0)
                        (registers affected-registers))
                    (loop collect (case (length registers)
                                    (0 (loop-finish))
                                    (1 (prog1 (inst single (pop registers) arm:*sp* offset) (incf offset 8)))
                                    (otherwise
                                     (prog1 (inst pair (pop registers) (pop registers) arm:*sp* offset)
                                       (incf offset 16))))))))
           (append (if (zerop push-amount) '()
                       (list (inst 'arm:sub arm:*sp* arm:*sp* push-amount)))
                   (move-affected 'arm:str 'arm:stp)
                   (list instruction)
                   (move-affected 'arm:ldr 'arm:ldp)
                   (if (zerop push-amount) '()
                       (list (inst 'arm:add arm:*sp* arm:*sp* push-amount)))))))
      ;; 19,999 engineers could not give you this tail call codegen
      ((or (instruction :name 'arm:b :arguments (list (not (successor-label))))
           (instruction :name 'arm:ret))
       (append (function-epilogue) (normal)))
      ;; Eliminate self-MOV
      ((instruction :name 'arm:mov :arguments (list a (eq a))) '())
      (_ (normal)))))

(defun function-prologue ()
  ;; We add two slots for storing FP and LR at the cold end of the frame.
  (let* ((push-distance (* 16 (ceiling (+ 2 *stack-offset-counter*) 2)))
         (fp-distance (- push-distance 16)))
    (list (inst 'arm:sub arm:*sp* arm:*sp* push-distance)
          (inst 'arm:stp arm:*fp* arm:*lr* arm:*sp* fp-distance)
          (inst 'arm:add arm:*fp* arm:*sp* fp-distance))))

(defun function-epilogue ()
  (list (inst 'arm:ldp arm:*fp* arm:*lr* arm:*sp* (stack-size-immediate 0))
        (inst 'arm:add arm:*sp* arm:*sp* (stack-size-immediate 2))))

(defun fix-branch-targets (cfg)
  (loop for b across (cfg-blocks-by-schedule cfg)
        for index = (basic-block-index b)
        do (setf (basic-block-instructions b)
                 (loop for i in (basic-block-instructions b)
                       appending (trivia:match i
                                   ((instruction :name 'arm:test :arguments (list* true false args))
                                    ;; Pick the right instruction(s) based on fall-through. This can't
                                    ;; be done in NODE-TO-INSTRUCTIONS as the CFG gets manipulated
                                    ;; after N-T-I.
                                    (flet ((fallthrough-p (name)
                                             (= (1+ index)
                                                (basic-block-index
                                                 (basic-block-successor-by-label b name)))))
                                      (cond
                                        ((fallthrough-p :yes)
                                         (list (apply #'inst false (basic-block-successor-by-label b :no) args)))
                                        ((fallthrough-p :no)
                                         (list (apply #'inst true (basic-block-successor-by-label b :yes) args)))
                                        (t (list (apply #'inst true args) (inst 'arm:b false))))))
                                   (_
                                    (setf (instruction-arguments i)
                                          (loop for a in (instruction-arguments i)
                                                collect (trivia:match a
                                                          ((successor-label :label l) (basic-block-successor-by-label b l))
                                                          ((stack-size-immediate :diff d)
                                                           (* 8 (+ (* 2 (ceiling *stack-offset-counter* 2)) d)))
                                                          (_ a))))
                                    (list i)))))
           (trivia:match (basic-block-successors b)
             ((where (list block)
                     ((basic-block :index (not (= (1+ index)))) block))
              (setf (basic-block-instructions b)
                    (append (basic-block-instructions b) (list (inst 'arm:b block))))))))

(define-backend :arm #'lower-node-for-arm 'emit-assembly #'make-cfg)
