(in-package :movement-three)

(defstruct (instruction (:constructor inst (name &rest arguments)))
  ;; INDEX gets picked at the start of COMPUTE-LIVE-INTERVALS.
  name arguments index)

(defvar *virtual-register-counter*)
(defvar *node-to-virtual-register*)
(defstruct (virtual-register (:constructor make-vreg
                                 (&aux (name (incf *virtual-register-counter*)))))
  name)

(defstruct (register (:constructor %register (index name)))
  (name nil :read-only t)
  (index nil :read-only t))
(defmethod print-object ((r register) stream)
  (print-unreadable-object (r stream :type t)
    (write-string (register-name r) stream)))

(defstruct (stack-location (:constructor stack-location))
  ;; INDEX gets picked lazily by STACK-OFFSET; otherwise calling
  ;; STACK-LOCATION willy-nilly would waste stack slots.
  index)
(defvar *stack-offset-counter*)
(defun stack-offset (location)
  (when (null (stack-location-index location))
    (setf (stack-location-index location) *stack-offset-counter*)
    (incf *stack-offset-counter*))
  (* 8 (stack-location-index location)))

(defstruct (immediate (:constructor immediate (value))) (value nil :read-only t))
(defstruct (successor-label (:constructor successor-label (label)))
  (label nil :read-only t))
(defstruct (stack-size-immediate (:constructor stack-size-immediate (diff)))
  (diff nil :read-only t))

(defun see-through-projects (node)
  (trivia:match node
    ;; Forward through some nodes which we don't need after
    ;; erasing memory and control flow.
    ;; TODO: Should this be part of an instruction selection pass?
    ((or (construct-fence :value v)
         (project :name 0 :tuple (and v (multi-new)))
         (project :name :value :tuple (and v (new)))
         (project :name (or :no-value :value) :tuple (or (eq-test :lhs v) (type-test :instance v)))
         (project :name :value
                  :tuple (and v (or (fixnum-binary-op) (fixnum-unary-op)
                                    (unknown-send) (known-send)))))
     (see-through-projects v))
    (_ node)))
(defun vreg-for-node (node)
  (trivia:match node
    ;; TLS gets hard-wired a register and the register will never
    ;; be used for anything else (unlike e.g. an ARGUMENT), so
    ;; don't bother generating moves.
    ((tls) arm:*tls*)
    ;; Immediates are immediate.
    ((literal-immediate :value v) (immediate v))
    (_
     (let ((node (see-through-projects node)))
       (or (gethash node *node-to-virtual-register*)
           (setf (gethash node *node-to-virtual-register*) (make-vreg)))))))

(define-time-meter *assembly-time* assembly)
(define-time-meter *isel-time* assembly instruction-selection)
(define-time-meter *apply-register-allocation* assembly apply-register-allocation)
(defvar *basic-block*)
(defun emit-assembly (code)
  (with-timing (*assembly-time*)
    (let ((*code* code)
          (cfg (code-cfg code))
          (*virtual-register-counter* 0)
          (*stack-offset-counter* 0)
          (*node-to-virtual-register* (make-hash-table)))
      (insert-write-barriers code)
      (eliminate-phis cfg (lambda (to from) (inst 'arm:mov to from))
                      #'vreg-for-node #'make-vreg)
      (order-basic-blocks code)
      (with-timing (*isel-time*)
        (loop for b across (cfg-blocks-by-schedule cfg)
              for *basic-block* = b
              ;; We already generated instructions for fixup blocks in ELIMINATE-PHIs.
              when (eq :uncomputed (basic-block-instructions b))
                do (setf (basic-block-instructions b)
                         (alexandria:mappend #'node-to-instructions (basic-block-nodes b)))))
      (pre-move-arguments)
      (merge-straight-line-blocks cfg)
      (skip-empty-blocks cfg)
      (let ((intervals (compute-live-intervals cfg)))
        (setf (cfg-intervals cfg) intervals)
        (let* ((merged (merge-live-intervals intervals))
               (allocation (allocate-registers merged)))
          (setf (cfg-register-allocation cfg) allocation)
          (with-timing (*apply-register-allocation*)
            (apply-register-allocation cfg merged allocation))))
      (fix-branch-targets cfg))))

;; Both SLOT-OFFSET and RECORD-BYTES account for a single header word.
(defun slot-offset (index) (1- (* 8 (1+ index))))
(defun record-bytes (record)
  (* 8
     (+ 1
        (length (record-immutable-slots record))
        (length (record-mutable-slots record)))))

;;; Ported from "Tilting at windmills with Coq: formal verification of a
;;; compilation algorithm for parallel moves"
;;; <https://xavierleroy.org/publi/parallel-move.pdf>
(defun parallel-move (sources destinations move-function make-temp)
  (let* ((n (length sources))
         (status (make-array n :initial-element :to-move))
         (sources (coerce sources 'vector))
         (destinations (coerce destinations 'vector))
         (instructions '()))
    (labels ((emit-move (to from)
               (push (funcall move-function to from) instructions))
             (move-one (i)
               (unless (eq (aref sources i) (aref destinations i))
                 (setf (svref status i) :being-moved)
                 (dotimes (j n)
                   (when (eq (aref sources j) (aref destinations i))
                     (ecase (aref status j)
                       (:to-move (move-one j))
                       (:being-moved
                        (let ((temp (funcall make-temp)))
                          (emit-move temp (aref sources j))
                          (setf (aref sources j) temp)))
                       (:moved))))
                 (emit-move (aref destinations i) (aref sources i))
                 (setf (aref status i) :moved))))
      (dotimes (i n)
        (when (eq (aref status i) :to-move)
          (move-one i))))
    (reverse instructions)))

(defun eliminate-phis (cfg move-function key make-temp)
  (loop for b being the hash-values of (cfg-blocks-by-start cfg)
        ;; Remove memory phis.
        do (setf (basic-block-phis b)
                 (remove-if (lambda (p) (eq (phi-known-type p) +memory+))
                            (basic-block-phis b)))
        unless (endp (basic-block-phis b))
          do (let* ((regs (mapcar key (basic-block-phis b)))
                    (fixups
                      (loop for p in (basic-block-predecessors b)
                            for values in (apply #'mapcar #'list (mapcar #'phi-values (basic-block-phis b)))
                            collect (make-basic-block
                                     :cold-p (basic-block-cold-p p)
                                     :loop-depth (basic-block-loop-depth p)
                                     :instructions (parallel-move
                                                    (mapcar key values)
                                                    regs
                                                    move-function
                                                    make-temp)
                                     :predecessors (list p)
                                     :successors (list b)))))
               ;; Stitch in the fixup blocks.
               (loop for p in (basic-block-predecessors b)
                     for f in fixups
                     do (setf (basic-block-successors p) (substitute f b (basic-block-successors p))))
               (setf (basic-block-predecessors b) fixups))))

(defun merge-straight-line-blocks (cfg)
  (loop for b across (cfg-blocks-by-schedule cfg)
        ;; Merge basic blocks without intervening splits or merges.
        do (trivia:match (basic-block-successors b)
             ((list s)
              (when (= 1 (length (basic-block-predecessors s)))
                (setf (basic-block-instructions s)
                      (append (basic-block-instructions b) (basic-block-instructions s))
                      (basic-block-instructions b) '()))))))

(defun skip-empty-blocks (cfg)
  (let ((substitution (alexandria:copy-array (cfg-blocks-by-schedule cfg))))
    ;; Find the first non-empty basic block succeeding each basic block.
    (loop for b across (cfg-blocks-by-schedule cfg)
          for index from 0
          do (loop until (not (endp (basic-block-instructions b)))
                   do (setf b (first (basic-block-successors b))))
             (setf (aref substitution index) b))
    ;; Clear predecessors.
    (loop for b across (cfg-blocks-by-schedule cfg)
          do (setf (basic-block-predecessors b) '()))
    ;; Now apply the substitution.
    (labels ((sub (b) (aref substitution (basic-block-index b))))
      (loop for b across (cfg-blocks-by-schedule cfg)
            do (setf (basic-block-successors b) (mapcar #'sub (basic-block-successors b))))))
  ;; Remove empty basic blocks...
  (setf (cfg-blocks-by-schedule cfg)
        (remove-if #'endp (cfg-blocks-by-schedule cfg) :key #'basic-block-instructions))
  ;; and re-build indices and predecessors.
  (loop for b across (cfg-blocks-by-schedule cfg)
        for n from 0
        do (setf (basic-block-index b) n)
           (dolist (s (basic-block-successors b)) (push b (basic-block-predecessors s)))))
