(in-package :movement-three.cl-backend)

;;; Compiling the CFG to Common Lisp.

(defstruct (cl-control-flow-graph
            (:conc-name "CL-CFG-")
            (:constructor make-cl-cfg)
            (:include mt::control-flow-graph))
  (registers '()))

;; A sub-form which we can only work out after emitting it.
(defstruct (fixup (:constructor %make-fixup (stage function)))
  stage function %value)
(defmethod print-object ((f fixup) stream)
  (print-unreadable-object (f stream :type t)
    (format stream "~A~S"
            (if (null (fixup-%value f)) "unfixed " "")
            (fixup-stage f))))
(defmacro fixup (stage form) `(%make-fixup ,stage (lambda () ,form)))
(defun fixup-value (fixup)
  (or (fixup-%value fixup)
      (setf (fixup-%value fixup) (funcall (fixup-function fixup)))))
(defun remove-fixups (code stage)
  (typecase code
    (list (loop for c in code collect (remove-fixups c stage)))
    (fixup (if (eq (fixup-stage code) stage) (fixup-value code) code))
    (t code)))

(defun lower-node-for-cl (node)
  (trivia:match node
    ((mt::perform-safepoint :control c :memory m)
     (mt::replace-node node (mt::pack nil #(:control :memory) c m)))))

;;; Tuple types are lowered to multiple values and variables, with
;;; *VARIABLES* mapping nodes to alists of values. Memory flow and
;;; control flow are erased.
(defvar *variables*)

;; This is a weakened version of SEE-THROUGH-PROJECTS unfortunately -
;; the rest of the CL backend wants single-valued nodes to stay single-valued.
;; Can't this pick the right variables to forward somehow?
;; The rule for the first value of a MULTI-NEW doesn't work though, so maybe
;; we can't reuse it anyway.
(defun forward-node (node)
  (trivia:match node
    ((or (mt::construct-fence :value v)
         (mt::project :name (or :no-value :value)
                      :tuple (or (mt::eq-test :lhs v) (mt::type-test :instance v)))
         (mt::project :name :value
                      :tuple (and v (or (mt::fixnum-binary-op) (mt::fixnum-unary-op)
                                        (mt::unknown-send) (mt::known-send)))))
     (forward-node v))
    (_ node)))

(defun node-variables (node)
  (multiple-value-bind (variables present)
      (gethash node *variables*)
    (cond
      (present variables)
      (t
       (setf (gethash node *variables*)
             (let ((forward (forward-node node)))
               (if (not (eq forward node))
                   (node-variables forward)
                   (trivia:match (mt:result-for mt:type node)
                     ((type:tuple :keys keys :values values)
                      (loop for k across keys for v across values
                            unless (or (eq v type:+memory+) (eq v type:+control+))
                              collect (cons k (gensym))))
                     ((or (eq type:+memory+) (eq type:+control+)) '())
                     (_ (acons :value (gensym) '()))))))))))
(defun node-variable (node)
  (trivia:match (node-variables node)
    ((list (cons :value v)) v)
    (:erased (error "~S was erased, and thus has no variable." node))
    (_ (error "~S has a tuple type, and thus multiple variables." node))))
(defun node-tuple-variable (node name)
  (or (cdr (assoc name (node-variables node)))
      (error "~S does not have a tuple value named ~S" node name)))
(defun erase-node (node)
  (assert (not (nth-value 1 (gethash node *variables*)))
          ()
          "The value of ~S was already used, when it should be erased." node)
  (setf (gethash node *variables*) :erased))
(defun make-variable ()
  (let ((name (gensym)))
    (setf (gethash name *variables*) (acons :value name '()))
    name))

(defun emit-cl (code)
  (let ((cfg (mt::code-cfg code))
        (mt:*code* code)
        (*variables* (make-hash-table))
        (*gensym-counter* 0))
    (mt::eliminate-phis cfg
                        (lambda (to from) `(setq ,to ,from))
                        #'node-variable
                        #'make-variable)
    (mt::order-basic-blocks code)
    (emit-cl-for-cfg cfg)
    (mt::merge-straight-line-blocks cfg)
    (mt::skip-empty-blocks cfg)
    (loop for alist being the hash-values of *variables*
          unless (eq alist :erased)
            do (loop for (nil . variable) in alist
                     do (push variable (cl-cfg-registers cfg))))
    (loop for b across (mt::cfg-blocks-by-schedule cfg)
          do (setf (mt::basic-block-instructions b)
                   (remove-fixups (mt::basic-block-instructions b) :cfg)))))

(defun location-for-type (location index type)
  (etypecase (mt::record-source type)
    (mt::utena-instance
     (if (zerop index)
         ;; The reader HAADEN-TWO:CLASS is only a reader.
         `(slot-value (slot-value ,location 'haaden-two:class) 'haaden-two::enclosing-object)
         `(aref (movement-three::storage ,location) ,(1- index))))
    (mt::utena-class
     (assert (= 0 index))
     `(haaden-two::enclosing-object ,location))))

(defun constructor-for-type (type)
  (let ((source (mt::record-source type)))
    (etypecase source
      (mt::utena-instance
       `(make-instance 'haaden-two::instance
          :class (make-instance 'haaden-two:class
                   :class-definition ',(mt::class-definition source))))
      (mt::utena-class
       `(make-instance 'haaden-two::instance
          :class (make-instance 'haaden-two:class
                   :class-definition ',(mt::class-definition source)))))))

(defvar *basic-block*)
(defun lower-instruction (i)
  (flet ((store (value) (list `(setq ,(node-variable i) ,value)))
         (erase () (erase-node i) '())
         (test (form)
           (let ((b *basic-block*))
             (flet ((succ (label)
                      (mt::basic-block-index (mt::basic-block-successor-by-label b label))))
               `((if ,form (go ,(fixup :cfg (succ :yes))) (go ,(fixup :cfg (succ :no)))))))))
    (trivia:ematch i
      (_ (when (eq (forward-node i) i) (trivia.next:next)) '())
      ((mt::anti-dependency-fence) (erase))
      ((mt::argument :index n) (store `(%argument ,n)))
      ((mt::crash :cause c) `((error "Crash: ~A" ,c)))
      ((mt::eq-test :lhs l :rhs r) (test `(eq ,(node-variable l) ,(node-variable r))))
      ((mt::fixnum-binary-op :name n :lhs l :rhs r)
       (store `(,n ,(node-variable l) ,(node-variable r))))
      ((mt::fixnum-unary-op :name n :value v) (store `(,n ,(node-variable v))))
      ((mt::fixnum-test :name n :lhs l :rhs r)
       (test `(,n ,(node-variable l) ,(node-variable r))))
      ((mt::immutable-memory) (erase))
      ((mt::known-send :method m :receiver r :arguments a :tail-p tail)
       (let ((form `(,(fixup :whole-program
                             (lookup-known-send m))
                     ,(node-variable r) ,@(mapcar #'node-variable a))))
         (list (if tail `(return ,form) `(setq ,(node-tuple-variable i :value) ,form)))))
      ((mt::literal :value v) (store `',v))
      ((mt::memory-merge) (erase))
      ((mt::memory-split) (erase))
      ((mt::multi-new :types types)
       (loop for ty in types
             for (nil . v) in (node-variables i)
             collect `(setq ,v ,(constructor-for-type ty))))
      ((mt::project :name n :tuple tuple)
       (let ((pair (assoc n (node-variables tuple))))
         (if (null pair) (erase) (store (cdr pair)))))
      ((mt::read-slot :index n :instance i)
       (store (location-for-type (node-variable i) n (mt:result-for mt:type i))))
      ((mt::region) (erase))
      ((mt::ret :value v) (list `(return ,(node-variable v))))
      ((mt::start) '())
      ((mt::tls) (store '*tls*))
      ((mt::type-test :instance v :type (eq type:+fixnum+))
       (test `(integerp ,(node-variable v))))
      ((mt::vm-state) '())
      ((mt::write-slot :index n :instance i :new-value v)
       (list `(setf ,(location-for-type (node-variable i) n (mt:result-for mt:type i))
                    ,(node-variable v)))))))

(defun emit-cl-for-cfg (cfg)
  (loop for b across (mt::cfg-blocks-by-schedule cfg)
        when (eq :uncomputed (mt::basic-block-instructions b))
        do (let ((*basic-block* b))
             (setf
              (mt::basic-block-instructions b)
              (loop for i in (mt::basic-block-nodes b)
                    append (lower-instruction i))))))

(mt:define-backend :cl 'lower-node-for-cl 'emit-cl #'make-cl-cfg)
