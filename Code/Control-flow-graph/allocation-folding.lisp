(in-package :movement-three)

;;; Allocation folding <https://research.google/pubs/allocation-folding-based-on-dominance/>
;;; restricted to each basic block. No effects which break folding can
;;; occur in one basic block, as calls terminate basic blocks.

(defun allocation-fold-basic-block (basic-block)
  (let ((news '())
        (new-types '())
        (other '()))
    ;; Get all Project :Value of New to rewrite, drop New, preserve other nodes.
    (dolist (node (basic-block-nodes basic-block))
      (trivia:match node
        ((project :name :value :tuple (new :type type))
         (push node news)
         (push type new-types))
        ((new))
        (_ (push node other))))
    ;; We've already scheduled into a basic block, so the memory arc
    ;; doesn't matter now.
    (unless (null news)
      (let ((folded (multi-new (node-control (first news)) new-types (new-memory (project-tuple (first news))))))
        (setf (basic-block-nodes basic-block) (reverse other))
        (loop for i from 0 for n in news
              for proj = (project nil i folded)
              do (push proj (basic-block-nodes basic-block))
                 (replace-node n proj))
        (push folded (basic-block-nodes basic-block))))))
    
