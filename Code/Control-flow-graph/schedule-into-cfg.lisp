(in-package :movement-three)

(defstruct (control-flow-graph (:conc-name "CFG-") (:constructor make-cfg))
  blocks-by-schedule
  blocks-by-start
  blocks-by-end
  blocks-by-node
  intervals register-allocation
  barriers-needed)

(defmethod print-object ((cfg control-flow-graph) stream)
  (print-unreadable-object (cfg stream :type t)
    (cond
      ((null (cfg-blocks-by-schedule cfg))
       (format stream "~D unscheduled blocks" (hash-table-count (cfg-blocks-by-start cfg))))
      (t
       (format stream "~D scheduled blocks" (length (cfg-blocks-by-schedule cfg)))))))

(defstruct basic-block
  (control '())
  (nodes '())
  (phis '())
  index
  loop-depth
  (instructions :uncomputed)
  predecessors
  (successors '())
  (successor-labels '())
  cold-p)

(defmethod print-object ((bb basic-block) stream)
  (print-unreadable-object (bb stream :type t)
    (unless (null (basic-block-index bb))
      (format stream "#~D~@[, depth ~D~]" (basic-block-index bb) (basic-block-loop-depth bb)))))

(defun basic-block-successor-by-label (basic-block label)
  (loop for s in (basic-block-successors basic-block)
        for l in (basic-block-successor-labels basic-block)
        when (eq l label) do (return s)
          finally (error "~A has no successor labelled ~A" basic-block label)))

(define-time-meter *cfg-time* cfg)
(defun schedule-into-cfg (code)
  (with-timing (*cfg-time*)
    (let ((*code* code))
      (trace-cfg-from-code code)
      (order-basic-blocks code)
      (compute-loop-depths code)
      (schedule-early code)
      (schedule-late code)
      (loop for b being the hash-values of (cfg-blocks-by-start (code-cfg code))
            ;; Allocation folding messes up the memory-flow, making it
            ;; impossible to schedule the nodes; so we schedule and then
            ;; do surgery on the scheduled nodes after.
            do (toposort-nodes b)
               (when *do-allocation-folding*
                 (allocation-fold-basic-block b))))))

(defun trace-cfg-from-code (code)
  (let ((blocks-by-end (make-hash-table))
        (blocks-by-start (make-hash-table))
        (blocks-by-node (make-hash-table)))
    (labels ((node-terminates-basic-block-p (node)
               (trivia:match node
                 ((project
                   :tuple (or (eq-test) (fixnum-test) (known-send)
                              (type-test) (unknown-send)))
                  t)
                 ((region) t)
                 ((start) t)
                 (_ nil)))
             (node-predecessors (node)
               (trivia:ematch node
                 ((project :tuple test) (list test))
                 ((region) (region-predecessors node))
                 ((start) '())))
             (node-label (node)
               (trivia:match node
                 ((project :name name) name)
                 (_ nil)))
             (trace-control (last-node)
               (or (gethash last-node blocks-by-end)
                   (let ((bb (make-basic-block)))
                     (setf (gethash last-node blocks-by-end) bb)
                     (loop for node = last-node then (node-control node)
                           do (push node (basic-block-control bb))
                              (setf (gethash node blocks-by-node) bb)
                              ;; Tie PROJECT control nodes to their tuple.
                              (when (project-p node)
                                (setf (node-control node) (project-tuple node)))
                           until (node-terminates-basic-block-p node))
                     (let* ((start (first (basic-block-control bb)))
                            (preds (node-predecessors start))
                            (blocks (mapcar #'trace-control preds)))
                       (setf (basic-block-cold-p bb)
                             (some #'node-cold-p (basic-block-control bb)))
                       (setf (basic-block-predecessors bb) blocks
                             (gethash start blocks-by-start) bb)
                       (dolist (pred blocks)
                         (push bb (basic-block-successors pred))
                         (push (node-label start) (basic-block-successor-labels pred))))
                     bb))))
      ;; All control nodes are reachable from the STOP node, or in case
      ;; of an infinite loop, a predecessor of a REGION node.
      (mapc #'trace-control (stop-controls (code-stop code)))
      (dolist (n (all-nodes code))
        (trivia:match n
          ((region) (mapc #'trace-control (region-predecessors n))))))
    (setf (code-cfg code)
          (funcall (backend-make-cfg *backend*)
           :blocks-by-start blocks-by-start
           :blocks-by-end blocks-by-end
           :blocks-by-node blocks-by-node))))

(trivia:defun-match node-cold-p (node)
  ((or (perform-safepoint) (crash)) t)
  (_ nil))

;;; Order basic blocks by pre-order traversal.
(defun order-basic-blocks (code)
  (let ((cfg (code-cfg code))
        (seen (make-hash-table))
        (blocks (make-array 8 :adjustable t :fill-pointer 0))
        (cold (make-array 8 :adjustable t :fill-pointer 0)))
    (labels ((walk (b)
               (unless (gethash b seen)
                 (setf (gethash b seen) t)
                 (vector-push-extend b (if (basic-block-cold-p b) cold blocks))
                 (mapc #'walk (basic-block-successors b)))))
      (walk (gethash (code-start code) (cfg-blocks-by-start cfg))))
    (setf (cfg-blocks-by-schedule cfg) (concatenate 'vector blocks cold))
    (loop for i from 0
          for b across (cfg-blocks-by-schedule cfg)
          do (setf (basic-block-index b) i))))

;;; Loop tree computation, following along with the algorithm in
;;; "Linear Scan Register Allocation for the Java HotSpot Client Compiler"
;;; <https://ssw.jku.at/Research/Papers/Wimmer04Master/Wimmer04Master.pdf#page=61>
(define-time-meter *loop-depth* cfg compute-loop-depth)
(defun compute-loop-depths (code)
  (with-timing (*loop-depth*)
    ;; The lazy choice of data structure: a 1:1 mapping of block -> loop membership.
    (let* ((blocks (cfg-blocks-by-schedule (code-cfg code)))
           (visited (make-array (length blocks) :element-type 'bit :initial-element 0))
           (loops (make-array (list (length blocks) (length blocks))
                              :element-type 'bit :initial-element 0))
           (trace-bits (make-array (length blocks) :element-type 'bit :initial-element 0))
           (back-arcs '()))
      ;; Find all back-arcs.
      (labels ((walk (block pred)
                 (when (zerop (aref visited (basic-block-index block)))
                   (cond
                     ((plusp (bit trace-bits (basic-block-index block)))
                      ;; Found a loop.
                      (push (cons block pred) back-arcs))
                     (t
                      (setf (bit trace-bits (basic-block-index block)) 1)
                      (dolist (succ (basic-block-successors block)) (walk succ block))
                      (setf (bit trace-bits (basic-block-index block)) 0)))
                   (setf (aref visited (basic-block-index block)) 1))))
        (walk (aref blocks 0) nil))
      ;; Find all blocks in between back-arc ends and starts.
      ;; This part is the least clear if we can have irreducible loops -
      ;; a naive flood fill would escape and claim more blocks via other
      ;; loop headers. The dominator test entirely ignores irreducible loops.
      (labels ((walk (block target)
                 (when (zerop (aref visited (basic-block-index block)))
                   (setf (aref visited (basic-block-index block)) 1)
                   (when (or (eq block target)
                             (node-dominates-p (first (basic-block-control target))
                                               (first (basic-block-control block))))
                     (setf (aref loops (basic-block-index block) (basic-block-index target)) 1)
                     (unless (eq block target)
                       (dolist (p (basic-block-predecessors block)) (walk p target)))))))
        (loop for (head . tail) in back-arcs
              do (fill visited 0)
                 (walk tail head)))
      ;; Compute depths by counting how many loops each block is a member of.
      (loop for b across blocks
            for index from 0
            for depth = (loop for loop below (length blocks) sum (aref loops index loop))
            do (setf (basic-block-loop-depth b) depth)))))

(defun schedule-early (code)
  (dolist (node (all-nodes code))
    (if (phi-p node)
        (let ((block (gethash (node-control node) (cfg-blocks-by-node (code-cfg code)))))
          (push node (basic-block-phis block)))
        (schedule-node-early code node))))

;; Schedule_Early, p.92 of Cliff's thesis
(defun schedule-node-early (code node)
  (flet ((schedule (input)
           (when (null (node-control input)) (schedule-node-early code input))))
    (map 'nil #'schedule (node-%inputs node))
    (map 'nil #'schedule (node-%rest-inputs node)))
  (unless (null (node-control node)) (return-from schedule-node-early))
  (let ((best-control (code-start code)))
    (flet ((consider (node)
             (assert (not (null (node-control node))))
             (when (< (car (result-for dominator best-control))
                      (car (result-for dominator (node-control node))))
               (setf best-control (node-control node)))))
      (map 'nil #'consider (node-%inputs node))
      (map 'nil #'consider (node-%rest-inputs node)))
    (let ((best-block (gethash best-control (cfg-blocks-by-node (code-cfg code)))))
      (setf (node-control node) (first (basic-block-control best-block))))))

(defun schedule-late (code)
  (let ((visited (make-hash-table))
        (*code* code))
    ;; Pin control and phi nodes.
    (loop for block being the hash-values of (cfg-blocks-by-start (code-cfg code))
          do (dolist (p (basic-block-phis block)) (setf (gethash p visited) t))
             (dolist (c (basic-block-control block)) (setf (gethash c visited) t)))
    (dolist (node (all-nodes code))
      (unless (null (node-%users node))
        (schedule-node-late code visited node)))))

;; Schedule_Late, p.96 of Cliff's thesis
(defun schedule-node-late (code visited node)
  (unless (gethash node visited)
    (setf (gethash node visited) t)
    (dolist (user (node-%users node))
      (schedule-node-late code visited user))
    (let ((lca nil))
      (dolist (user (node-%users node))
        (if (phi-p user)
            (loop for v in (phi-values user)
                  for p in (region-predecessors (node-control user))
                  when (eq v node)
                    do (setf lca (find-lca lca p)))
            (setf lca (find-lca lca (node-control user)))))
      ;; Try to hoist out of loops if we're not in a cold block.
      (let ((blocks-by-node (cfg-blocks-by-node (code-cfg code))))
        (unless (or (basic-block-cold-p (gethash lca blocks-by-node))
                    (literal-p node))
          (block nil
            (map-dominators
             (lambda (n)
               (let ((n-depth (basic-block-loop-depth (gethash n blocks-by-node)))
                     (lca-depth (basic-block-loop-depth (gethash lca blocks-by-node))))
                 (when (< n-depth lca-depth) (setf lca n))
                 ;; Don't hoist past definitions
                 (when (eq n (node-control node)) (return))))
             lca)))
        (let ((best-block (gethash lca blocks-by-node)))
          (setf (node-control node) (first (basic-block-control best-block)))
          (push node (basic-block-nodes best-block)))))))

;; Find_LCA, p.96, compute the lowest common ancestor of two control
;; nodes.
(defun find-lca (a b)
  (cond
    ((null a) b)
    ((eq a b) a)
    (t
     (let ((a-dom (result-for dominator a))
           (b-dom (result-for dominator b)))
       (cond
         ;; We hit the root of the dominator tree.
         ((zerop (car a-dom)) a)
         ((zerop (car b-dom)) b)
         ;; Try to decrease depth as little as possible while climbing
         ;; the tree.
         ((> (car a-dom) (car b-dom)) (find-lca (cdr a-dom) b))
         ((< (car a-dom) (car b-dom)) (find-lca a (cdr b-dom)))
         (t (find-lca (cdr a-dom) (cdr b-dom))))))))

;;; Not exactly the most amazing of instruction scheduling algorithms.
(defun toposort-nodes (basic-block)
  ;; The first control node is always first, the last control node is always
  ;; last. Everything else gets topologically sorted.
  (multiple-value-bind (first-control middle-control last-control)
      (trivia:match (basic-block-control basic-block)
        ((list single-control) (values single-control '() '()))
        ((cons first remaining) (values first (butlast remaining) (last remaining))))
    (let ((sorted (list first-control))
          (members (make-hash-table))
          (unsorted (make-hash-table))
          (ready (bodge-queue:make-queue)))
      (labels ((node-sorted-p (node) (not (gethash node unsorted)))
               (predecessors (node)
                 (concatenate 'list
                              (remove-if #'node-sorted-p (node-%inputs node))
                              (remove-if #'node-sorted-p (node-%rest-inputs node))))
               (include (node)
                 (setf (gethash node members) t
                       (gethash node unsorted) t))
               (check (node)
                 (when (and (gethash node members) (null (predecessors node)))
                   (bodge-queue:queue-push ready node))))
        (mapc #'include (basic-block-nodes basic-block))
        (mapc #'include middle-control)
        (mapc #'check (basic-block-nodes basic-block))
        (mapc #'check middle-control)
        (loop until (bodge-queue:queue-empty-p ready)
              do (let ((node (bodge-queue:queue-pop ready)))
                   (remhash node unsorted)
                   (push node sorted)
                   (map 'nil #'check (node-%users node)))))
      (unless (zerop (hash-table-count unsorted))
        (error "There's a cycle betweeen the nodes ~{~A~^, ~}"
               (alexandria:hash-table-keys unsorted)))
      (setf (basic-block-nodes basic-block) (append (reverse sorted) last-control)))))
