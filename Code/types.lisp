(in-package :movement-three)

;;; The type lattice.
;;; First, some simple types.
(defconstant +top+ '+top+ "Any value.")
(defconstant +bottom+ '+bottom+ "No values.")
(defconstant +fixnum+ '+fixnum+)

;; Types of control and memory flow.
(defconstant +control+ '+control+ "Control flow.")
(defconstant +memory+ '+memory+ "Memory flow.")
(defconstant +state+ '+state+ "A VM state for a safepoint.")

;;; Records
(defstruct (slot-definition (:constructor slot-definition (name type)))
  (name nil :read-only t) (type nil :read-only t))
;; A sequence of slots. Immutable slots precede mutable slots for
;; simplicity; NEW instates slot values for all immutable slots, which
;; are a prefix of all slots. Record types are "nominal": record types are
;; only equal if they are EQ. A record has a source to query when
;; searching for known sends.
(defstruct (record (:constructor record (source immutable-slots mutable-slots)))
  (source nil :read-only t)
  (immutable-slots '() :read-only t)
  (mutable-slots '() :read-only t))

(defun record-slot-count (record)
  (+ (length (record-immutable-slots record))
     (length (record-mutable-slots record))))

(defun slot-index (slot record)
  (let ((immutable (position slot (record-immutable-slots record) :key #'slot-definition-name))
        (mutable (position slot (record-mutable-slots record) :key #'slot-definition-name)))
    (cond
      ((not (null immutable)) immutable)
      ((not (null mutable)) (+ (length (record-immutable-slots record)) mutable))
      (t (error "~S isn't in ~S" slot record)))))

(defun slot-by-index (record index)
  (let ((immutable (length (record-immutable-slots record))))
    (if (< index immutable)
        (elt (record-immutable-slots record) index)
        (elt (record-mutable-slots record) (- index immutable)))))

(defun type-source (type) (if (record-p type) (record-source type) nil))

(defvar *tls-type*)

;;; Tuples
;; A tuple isn't a proper value, but serves to associate multiple outputs
;; of a node. A tuple is structured this way in order to share the key
;; vector between tuples with the same keys but different values.
;; Click uses indices which makes lookup by index O(1); here n is small
;; so I am not too concerned about having O(n) lookup.
(defstruct (tuple (:constructor %tuple (keys values)))
  (keys (error "keys") :type simple-vector)
  (values (error "values") :type simple-vector))
(defmacro tuple (&rest key-values)
  (loop for (k v) on key-values by #'cddr
        do (assert (keywordp k))
        collect k into keys
        collect v into values
        finally (return `(%tuple ',(coerce keys 'vector) (vector ,@values)))))
;; Some node might always have the same tuple type, so reusing the
;; same tuple to make EQ work is simple and fast.
(defmacro constant-tuple (&rest key-values)
  "Construct a tuple at load time."
  `(load-time-value (tuple ,@key-values)))
(defun tuple-lookup (tuple key)
  (loop for k across (tuple-keys tuple)
        for v across (tuple-values tuple)
        when (eql k key) do (return v)
          finally (error "no ~A in ~A" key tuple)))

;;; Union type
(defstruct (union-type (:constructor %union-type (types))) types)
(defvar *maximum-union-size* 5)

(defun union-type (types)
  (let* ((types (remove-duplicates (remove +bottom+ types)))
         (len (length types)))
    (cond
      ((= len 0) +bottom+)
      ((= len 1) (first types))
      ((> len *maximum-union-size*)
       (mumble "Too many types for a UNION-TYPE, making +TOP+")
       +top+)
      (t (%union-type types)))))

(trivia:defun-match ensure-union-list (type)
  ((union-type :types types) types)
  (_ (list type)))

;;; Exact/singleton type
(defstruct (exactly (:constructor exactly (value))) value)

;;; Type operations
(trivia:defun-match* type-union (type1 type2)
  ((t1 (eq t1)) t1)
  (((eq +top+) _) +top+)
  ((_ (eq +top+)) +top+)
  (((exactly :value v) (exactly :value (eq v))) (exactly v))
  (((eq +bottom+) other) other)
  ((other (eq +bottom+)) other)
  ((t1 t2) (union-type (union (ensure-union-list t1) (ensure-union-list t2)))))

(trivia:defun-match* type-intersect (type1 type2)
  ((t1 t2) (unless (type= t1 t2) (trivia.next:next)) t1)
  (((eq +bottom+) _) +bottom+)
  ((_ (eq +bottom+)) +bottom+)
  (((eq +top+) t2) t2)
  ((t1 (eq +top+)) t1)
  (((union-type :types types) t2)
   (union-type (loop for t1 in types collect (type-intersect t1 t2))))
  ((t1 (union-type :types types))
   (union-type (loop for t2 in types collect (type-intersect t1 t2))))
  ((t1 t2) (unless (type<= t1 t2) (trivia.next:next)) t1)
  ((t1 t2) (unless (type<= t2 t1) (trivia.next:next)) t2)
  (_ +bottom+))

(defun types-disjoint-p (type1 type2) (eq +bottom+ (type-intersect type1 type2)))

(trivia:defun-match* type= (type1 type2)
  ((t1 (eq t1)) t)
  (((exactly :value v1) (exactly :value v2)) (eq v1 v2))
  (((union-type :types t1) (union-type :types t2)) (alexandria:set-equal t1 t2 :test #'type=))
  ((_ _) nil))

(trivia:defun-match* type<= (type1 type2)
  ((t1 t2) (if (type= t1 t2) t (trivia.next:next)))
  (((exactly :value v) t2) (type<= (wider-type-of v) t2))
  (((union-type :types types) t2)
   (loop for t1 in types always (type<= t1 t2)))
  ((t1 (union-type :types types))
   (loop for t2 in types thereis (type<= t1 types))))

(trivia:defun-match* type-difference (type1 type2)
  ;; T1 <: T2 implies T1 - T2 = Bottom
  ((t1 t2) (unless (type<= t1 t2) (trivia.next:next)) +bottom+)
  (((union-type :types types) t2)
   (union-type (mapcar (lambda (t1) (type-difference t1 t2)) types)))
  ((t1 (union-type :types types))
   (union-type (mapcar (lambda (t2) (type-difference t1 t2)) types)))
  ((t1 _) t1))

(defgeneric wider-type-of (instance)
  (:method ((i integer)) +fixnum+)
  (:method (o) +top+))
(defgeneric method-return-type (method receiver arguments)
  (:method (method receiver arguments)
    (declare (ignore method receiver arguments))
    +top+))

(trivia:defun-match widen-away-exactly (type)
  "Widen EXACTLY types to an appropriate class."
  ((exactly :value value) (wider-type-of value))
  ((union-type :types types) (union-type (mapcar #'widen-away-exactly types)))
  (_ type))
