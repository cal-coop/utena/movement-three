(asdf:defsystem :movement-three-visualiser
  :depends-on (:movement-three :mcclim :mcclim-dot :clouseau :haaden-two-clouseau)
  :serial t
  :components ((:file "package")
               (:file "draw")
               (:file "draw-ir")
               (:file "draw-register-allocation")
               (:file "application")
               (:file "compilation-unit")))
