(in-package :movement-three-visualiser)

(defvar *draw-level* 4)

;;; This design is more like the IR visualiser in SICL/Cleavir.

;; WITH-PRESERVED-CURSOR-Y c.f. Clouseau, McCLIM/Apps/Clouseau/src/formatting.lisp
(defun call-with-preserved-cursor-y (thunk stream)
  (let ((old-y (nth-value 1 (clim:stream-cursor-position stream))))
    (prog1
        (funcall thunk stream)
      (setf (clim:stream-cursor-position stream)
            (values (clim:stream-cursor-position stream) old-y)))))

(defmacro with-preserved-cursor-y ((stream) &body body)
  (check-type stream symbol)
  `(call-with-preserved-cursor-y (lambda (,stream) ,@body) ,stream))

(defun title (name pane)
  (clim:with-text-face (pane :bold) (write-line name pane) (vspace 1/4 pane)))

(defun parbreak (pane) (terpri pane) (vspace 1/3 pane))

(defmacro three-row (stream left middle right)
  `(clim:formatting-row (,stream)
    (clim:formatting-cell (,stream :align-x :right) (write-string ,left ,stream))
    (clim:formatting-cell (,stream) (write-string ,middle ,stream))
    (clim:formatting-cell (,stream) ,right)))

;;; Node panel
(defun display-node (frame pane)
  (typecase (node-to-analyse frame)
    (mt::virtual-register
     (title (format nil "Virtual register #~D"
                    (mt::virtual-register-name (node-to-analyse frame)))
            pane)
     (draw-live-intervals-for-virtual-register (node-to-analyse frame) pane frame))
    (mt::node
     (draw-inputs-and-users (node-to-analyse frame) pane))))

(defun draw-inputs-and-users (node pane)
  (title (string-capitalize (type-of node)) pane)
  (let* ((info (gethash (type-of node) mt::*node-structures*))
         (inputs (mt::node-structure-inputs info)))
    (unless (null inputs)
      (title "Inputs" pane)
      (vspace 1/3 pane)
      (clim:formatting-table (pane)
        (loop for name in inputs
              for input across (mt::node-%inputs node)
              do (three-row pane
                            (string-capitalize name)
                            " = "
                            (draw-crossreference input pane))))
      (terpri pane)))
  (unless (null (mt::node-%rest-inputs node))
    (title "Rest Inputs" pane)
    (vspace 1/3 pane)
    (dolist (r (mt::node-%rest-inputs node)) (draw-crossreference r pane)))
  (unless (null (mt::node-control node))
    (clim:with-text-face (pane :bold) (write-string "Control:  " pane))
    (draw-crossreference (mt::node-control node) pane))
  (unless (null (mt::node-%users node))
    (title "Users" pane)
    (vspace 1/3 pane)
    (dolist (u (mt::node-%users node))
      (draw-crossreference u pane))))

(defun display-mumbles (frame pane)
  (clim:formatting-table (pane)
    (loop for (tick pass . mumble) in (reverse (mumble-log frame))
          do (clim:formatting-row (pane)
               (clim:formatting-cell (pane :align-x :right :align-y :bottom)
                 (draw-tick tick pane))
               (clim:formatting-cell (pane :align-x :right :align-y :bottom)
                 (clim:with-text-size (pane :smaller)
                   (write-string pass pane)))
               (clim:formatting-cell (pane)
                 (dolist (m mumble)
                   (etypecase m
                     (mt::node
                      (with-preserved-cursor-y (pane)
                        (clim:with-output-as-presentation (pane m 'node)
                          (draw-node m pane))))
                     (string (write-string m pane))
                     (t
                      (clim:with-output-as-presentation (pane m 'mumbled-object)
                        (let ((*print-level* 1))
                          (clim:with-text-family (pane :fix) (princ m pane))))))))))))

;;; Analysis panel
(defun display-analysis (frame pane)
  (typecase (node-to-analyse frame)
    ((or mt::virtual-register mt:register)
     (title "Live intervals:" pane)
     (draw-all-live-intervals (mt::code-cfg (ir frame)) (node-to-analyse frame) pane)
     (parbreak pane)
     (title "Allocation:" pane)
     (draw-register-allocation (mt::code-cfg (ir frame)) pane))
    (mt::node
     (let ((mt::*code* (ir frame))
           (node (node-to-analyse frame)))
       (title "Type:" pane)
       (draw-type
        (mt:result-for mt:type node)
        pane)
       (parbreak pane)
       (title "Dominators:" pane)
       (draw-dominators
        (mt:result-for mt:dominator (node-to-analyse frame))
        pane)
       (parbreak pane)
       (write-line
        (if (mt:needs-safepoint-p node)
            "Needs to safepoint after"
            "Doesn't need to safepoint after")
        pane)
       (parbreak pane)
       (draw-info node pane)))))

(defun vspace (height stream)
  (let ((old-y (nth-value 1 (clim:stream-cursor-position stream))))
    (setf (clim:stream-cursor-position stream)
          (values (clim:stream-cursor-position stream)
                  (+ (round (* (clim:stream-line-height stream) height)) old-y)))))

(defun draw-tick (tick stream)
  (unless (null tick)
    (clim:with-output-as-presentation (stream tick 'tick)
      (clim:with-drawing-options (stream
                                  :text-size :smaller
                                  :ink clim:+grey50+)
        (format stream "~D " tick)))))

;;; Compilation unit panel
(defun display-unit-functions (frame pane)
  (unless (null (unit frame))
    (draw-unit-functions (unit frame) pane)))

(defun draw-unit-functions (unit pane)
  (let ((mt:*compilation-unit* unit)
        (*draw-level* 2)
        (by-method (make-hash-table)))
    (mt:map-functions
     (lambda (info) (push info (gethash (mt:fun-method info) by-method))))
    (dolist (method (sort (alexandria:hash-table-keys by-method) #'string<
                          :key #'princ-to-string))
      (let ((functions (gethash method by-method)))
        (title (princ-to-string method) pane)
        (dolist (f functions)
          (clim:with-output-as-presentation (pane f 'function-info :single-box t)
            (clim:surrounding-output-with-border (pane)
              (dolist (ty (mt:fun-argument-types f))
                (draw-type ty pane)
                (terpri pane))
              (clim:with-text-face (pane :italic) (write-string "returns " pane))
              (draw-type (mt:fun-return-type f) pane)))
          (terpri pane))))))

;;; Drawing nodes
(defgeneric draw-node (node stream))
(defgeneric node-text (node))

(defmethod draw-node :around (node stream)
  (clim:with-drawing-options (stream
                              :text-size :smaller
                              :ink (if (selected-p node)
                                       clim:+blue+
                                       clim:+foreground-ink+))
    (call-next-method)))

(defmethod draw-node (node stream)
  (let ((background
          (cond
            ((eq (mt::node-control node) :dead)
             clim::+grey+)
            ((find 'mt::stop (mt::node-%users node) :key #'type-of)
             clim::+pink2+)
            (t clim::+background-ink+))))
    (clim:surrounding-output-with-border (stream :background background)
      (write-string (node-text node) stream))))

;; Most special pretty-printing of nodes just changes the text to something
;; more concise.
(defmethod node-text (node)
  (format nil "~:(~A~)~{ ~A~}"
          (type-of node)
          (coerce (mt::node-%settings node) 'list)))

(defvar *multi-new-limit* 2)
(defmethod node-text ((node mt::multi-new))
  (format nil "Multi-New ~{~A~^, ~}~A"
          (loop for type in (mt::multi-new-types node)
                for limit below *multi-new-limit*
                collect (source-name (mt::record-source type)))
          (let ((len (length (mt::multi-new-types node))))
            (if (> len *multi-new-limit*) (format nil " ... (~A)" len) ""))))

(defmethod node-text ((node mt::new))
  (format nil "New ~A"
          (source-name (mt::record-source (mt::new-type node)))))

(defmethod node-text ((node mt::pack))
  (format nil "Pack ~{~:(~A~)~^, ~}" (coerce (mt::pack-keys node) 'list)))

(defun memory-edge-name (label)
  (etypecase label
    (cons (format nil "~A:~D" (source-name (mt::record-source (car label))) (cdr label)))
    (symbol (string-capitalize label))))

(defmethod node-text ((node mt::memory-split))
  (format nil "Split ~A" (memory-edge-name (mt::memory-split-alias node))))

(defmethod node-text ((node mt::memory-merge))
  (format nil "Merge ~{~A~^, ~}"
          (mapcar #'memory-edge-name (mt::memory-merge-aliases node))))

(defun method-name (method)
  (etypecase method
    (haaden-two::method (haaden-two::name (haaden-two::info method)))
    (mt:function-info (method-name (mt:fun-method method)))
    (mt::fixnum-primop-method (mt::fixnum-primop-method-name method))
    (mt::runtime-method (mt::runtime-method-name method))))

(defmethod node-text ((node mt::method-start))
  (format nil "Method-Start ~A"
          (method-name (mt::method-start-method node))))

(defmethod node-text ((node mt::method-argument))
  (format nil "Method-Arg ~D"
          (mt::method-argument-index node)))

(defun translate-tail-p (tail-p)
  (ecase tail-p
    ((t) " tail")
    ((:suppressed) " suppressed-tail")
    ((nil) "")))

(defmethod node-text ((node mt::known-send))
  (format nil "Known-Send ~A~A"
          (method-name (mt::known-send-method node))
          (translate-tail-p (mt::known-send-tail-p node))))

(defmethod node-text ((node mt::unknown-send))
  (format nil "Unknown-Send~A"
          (translate-tail-p (mt::unknown-send-tail-p node))))

(defmethod node-text ((node mt::type-test))
  (flet ((type-name (type)
           (etypecase type
             (mt::exactly (format nil "eq? ~A" (mt::exactly-value type)))
             (mt::record (source-name (mt::record-source type)))
             (symbol (remove #\+ (string-capitalize type))))))
    (format nil "Type-Test ~A"
            (type-name (mt::type-test-type node)))))

;; Literal, Literal-Immediate and Project nodes get special colours as they're that common.
(defmethod draw-node ((node mt::literal) stream)
  (clim:surrounding-output-with-border (stream :background clim:+light-sky-blue+)
    (princ (mt::literal-value node) stream)))

(defmethod draw-node ((node mt::literal-immediate) stream)
  (clim:surrounding-output-with-border (stream :background clim:+light-green+)
    (princ (mt::literal-immediate-value node) stream)))

(defmethod draw-node ((node mt::project) stream)
  (clim:surrounding-output-with-border (stream :background clim:+light-goldenrod+)
    (write-string (string-capitalize (princ-to-string (mt::project-name node))) stream)))

(defmethod draw-node ((node mt::unreachable) stream)
  (clim:surrounding-output-with-border (stream :background clim:+magenta+)
    (write-string "Unreachable" stream)))

(defun draw-callers (callers stream)
  (dolist (caller callers)
    (format stream "~A:~D " (car caller) (cdr caller))))

(defgeneric draw-info (node stream))
(defmethod draw-info (node stream))

(defmethod draw-info ((node mt::known-send) stream)
  (title "Known Send:" stream)
  (format stream "Callers: ")
  (draw-callers (mt::known-send-callers node) stream))

(defmethod draw-info ((node mt::method-start) stream)
  (title "Method Start:" stream)
  (format stream "Callers: ")
  (draw-callers (mt::method-start-callers node) stream))

;;; Drawing types

(defgeneric draw-type (type stream))

(defmethod draw-type :around (type stream)
  (if (zerop *draw-level*)
      (write-string "..." stream)
      (let ((*draw-level* (1- *draw-level*)))
        (call-next-method))))

(defmethod draw-type (type stream)
  (clim:with-text-family (stream :fix)
    (prin1 type stream)))

(defmethod draw-type ((type symbol) stream)
  (write-string (remove #\+ (string-capitalize type)) stream))

(defmethod draw-type ((type types:exactly) stream)
  (write-string "Exactly " stream)
  (clim:with-text-family (stream :fix)
    (prin1 (types:exactly-value type) stream)))

(defmethod draw-type ((type types:tuple) stream)
  (clim:surrounding-output-with-border (stream)
    (clim:formatting-table (stream)
      (loop for k across (types:tuple-keys type)
            for v across (types:tuple-values type)
            do (three-row stream
                          (string-capitalize (princ-to-string k))
                          " = "
                          (draw-type v stream))))))

(defmethod draw-type ((type types:union-type) stream)
  (loop for (type . rest) on (types:union-type-types type)
        do (draw-type type stream)
           (unless (null rest) (write-string " or " stream))))

(defgeneric source-name (source))
(defmethod source-name (source)
  (let ((*print-case* :capitalize)) (princ-to-string source)))
(defmethod source-name ((source types:utena-type))
  (string-capitalize (princ-to-string (haaden-two::name (types:class-definition source)))))
(defmethod source-name ((source types:utena-class))
  (concatenate 'string (call-next-method) " class"))
(defmethod source-name ((source haaden-two::slot-definition))
  (string-capitalize (haaden-two::name (haaden-two::reader source))))

(defmethod draw-type ((type types:record) stream)
  (clim:surrounding-output-with-border (stream)
    (clim:with-text-face (stream :bold)
      (write-line (source-name (types:record-source type)) stream))
    (clim:formatting-table (stream)
      (flet ((draw-slot (slotd middle)
               (three-row stream
                          (source-name (types:slot-definition-name slotd))
                          middle
                          (draw-type (types:slot-definition-type slotd) stream))))
        (dolist (slotd (types:record-immutable-slots type))
          (draw-slot slotd " = "))
        (dolist (slotd (types:record-mutable-slots type))
          (draw-slot slotd " ← "))))))

;;; Drawing dominators

(defun draw-crossreference (node pane &key (newline t))
  (with-preserved-cursor-y (pane)
    (clim:with-output-as-presentation (pane node 'node)
      (draw-node node pane)))
  (when (typep node 'mt::project)
    (clim:with-text-face (pane :italic) (write-string "  from  " pane))
    (let ((from (mt::project-tuple node)))
      (with-preserved-cursor-y (pane)
        (clim:with-output-as-presentation (pane from 'node)
          (draw-node from pane)))))
  (when newline
    (terpri pane)
    (vspace 1/3 pane)))

(defun draw-dominators (dominators pane)
  (trivia:ematch dominators
    (nil (write-string "None" pane))
    ((cons _ nil))
    ((cons _ (and d (mt::node)))
     (draw-crossreference d pane)
     (draw-dominators (mt:result-for mt:dominator d) pane))
    ((types:tuple)
     (clim:surrounding-output-with-border (pane)
       (clim:formatting-table (pane)
         (loop for k across (types:tuple-keys dominators)
               for v across (types:tuple-values dominators)
               do (three-row pane
                             (string-capitalize (princ-to-string k))
                             " = "
                             (draw-dominators v pane))))))))
