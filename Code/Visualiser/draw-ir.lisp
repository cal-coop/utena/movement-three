(in-package :movement-three-visualiser)

(defvar *selected-test* (constantly nil))
(defun selected-p (node) (funcall *selected-test* node))

(defun draw-ir (code stream &key view)
  (ecase view
    ((:sea-of-nodes) (draw-graph code stream))
    ((:cfg :assembly)
     (unless (null (mt::code-cfg code))
       (draw-cfg code stream :show-assembly (eq view :assembly))))
    ((nil)
     (if (null (mt::code-cfg code))
         (draw-graph code stream)
         (draw-cfg code stream)))))

;; Drawing sea-of-nodes
(defun draw-graph (code stream)
  (let ((*print-level* 2)
        (mt::*code* code))
    (clim:format-graph-from-roots
     (remove-if (lambda (n)
                  (or (typep n '(or mt::stop mt::immutable-memory))
                      (endp (mt::node-%users n))))
                (mt::all-nodes code))
     (lambda (node stream)
       (clim:with-output-as-presentation (stream node 'node)
         (draw-node node stream)))
     (lambda (n)
       ;; Don't draw lines for all uses of IMMUTABLE-MEMORY,
       ;; there's way too many of them.
       (if (typep n 'mt::immutable-memory)
           '()
           (remove-if (lambda (u) (typep u 'mt::stop))
                      (mt::node-%users n))))
     :stream stream
     :graph-type :dot-digraph
     :orientation :vertical
     :merge-duplicates t)))

;; Drawing a control-flow-graph
(defun draw-cfg (code stream &key (show-assembly t))
  (clim:format-graph-from-roots
   (mt::cfg-blocks-by-schedule (mt::code-cfg code))
   (lambda (b s) (draw-basic-block b s :show-assembly show-assembly))
   #'mt::basic-block-successors
   :stream stream
   :graph-type :dot-digraph
   :orientation :vertical
   :merge-duplicates t))

(defun draw-basic-block (bb stream &key (show-assembly t))
  (flet ((node-title ()
           (title
            (format nil "Block~@[ #~D~]~@[ depth ~D~]~:[~; (cold)~]"
                    (mt::basic-block-index bb)
                    (mt::basic-block-loop-depth bb)
                    (mt::basic-block-cold-p bb))
            stream)))
    (clim:with-output-as-presentation (stream bb 'basic-block :single-box t)
      (clim:surrounding-output-with-border (stream :background clim:+background-ink+)
        (cond
          ((or (eq (mt::basic-block-instructions bb) :uncomputed) (not show-assembly))
           (unless (null (mt::basic-block-phis bb))
             (title "Phis" stream)
             (dolist (node (mt::basic-block-phis bb))
               (draw-crossreference node stream)))
           (unless (null (mt::basic-block-nodes bb)) (node-title))
           (dolist (node (mt::basic-block-nodes bb))
             (draw-crossreference node stream)))
          (t
           (node-title)
           (clim:formatting-table (stream)
             (dolist (instruction (mt::basic-block-instructions bb))
               (clim:formatting-row (stream)
                 (clim:formatting-cell (stream :align-x :right)
                   (let ((index (if (typep instruction 'mt:instruction)
                                    (mt::instruction-index instruction)
                                    nil)))
                     (unless (null index)
                       (let* ((index-highlight
                                (index-highlighted-p (mt::instruction-index instruction)))
                              (ink (ecase index-highlight
                                     ((t) clim:+blue+)
                                     ((:wide) clim:+blue-violet+)
                                     ((nil) clim:+grey50+))))
                         (clim:with-drawing-options (stream :text-size :smaller :ink ink)
                           (format stream "~D" (mt::instruction-index instruction)))))))
                 (clim:formatting-cell (stream)
                   (draw-instruction instruction stream)))))))))))

(defun draw-instruction (i stream)
  (trivia:ematch i
    ((mt:instruction :name name :arguments arguments)
     (clim:with-output-as-presentation (stream i 'instruction :single-box t)
       (clim:with-drawing-options (stream :text-face :bold
                                          :ink (if (selected-p i) clim:+blue+ clim:+black+))
         (format stream "~(~A~) " name))
       (loop for (argument . rest?) on arguments
             for n from 0
             do (draw-instruction-argument argument stream)
                (when rest? (write-string ", " stream))
                (when (and (eq name 'movement-three.arm:alloc)
                           (= n *multi-new-limit*))
                  (write-string "..." stream) (loop-finish))))
     (terpri stream))
    ((type list)
     (clim:with-text-family (stream :fix)
       (let ((*print-level* 4)
             (*package* (find-package :movement-three.cl-backend)))
         (prin1 i stream))))))

(defun index-highlighted-p (index)
  (cond
    ((not (typep clim:*application-frame* 'visualiser)) nil)
    (t
     (let ((cfg (mt::code-cfg (ir clim:*application-frame*)))
           (start most-positive-fixnum)
           (end 0))
       (dolist (i (mt::cfg-intervals cfg))
         (when (selected-p (mt::interval-vreg i))
           (when (mt::interval-intersects-p i index)
             (return-from index-highlighted-p t))
           (alexandria:minf start (mt::interval-end i))
           (alexandria:maxf end (mt::interval-start i))))
       (if (<= start index end) :wide nil)))))

(defun draw-instruction-argument (argument stream)
  (typecase argument
    (mt::virtual-register
     (clim:with-output-as-presentation (stream argument 'analysable)
       (clim:with-drawing-options (stream
                                   :ink (if (selected-p argument)
                                            clim:+blue+ clim:+orange+))
         (format stream "v~D" (mt::virtual-register-name argument)))))
    (mt::register
     (clim:with-output-as-presentation (stream argument 'analysable)
       (clim:with-drawing-options (stream :ink clim:+green3+)
         (write-string (mt:register-name argument) stream))))
    (mt::stack-location
     (clim:with-drawing-options (stream :ink clim:+green3+)
       (format stream "S~D" (mt::stack-location-index argument))))
    (mt::immediate
     (clim:with-drawing-options (stream :text-face :italic :ink clim:+purple+)
       (format stream "~A" (mt::immediate-value argument))))
    (mt::record
     (format stream "~A" (source-name (mt::record-source argument))))
    (mt::successor-label
     (format stream ".~(~A~)" (mt::successor-label-label argument)))
    (mt::basic-block
     (format stream ".B~D" (mt::basic-block-index argument)))
    (mt:function-info
     (draw-instruction-argument (mt:fun-method argument) stream))
    (integer (format stream "#~D" argument))
    (t (let ((*print-circle* t) (*print-level* 1)) (princ argument stream)))))
