(in-package :movement-three-visualiser)

;;; This is very influenced by Jan Moringen's IR visualiser for SBCL
;;; <https://github.com/scymtym/sbcl-ir-visualizer>.

(defvar *sample-function*
  "(lambda (x)
  (let ((:mutable y x)
        ((id z) z))
    (set! y 42)
    (id y)))")

(defun make-limit-slider (limit callback)
  (clim:make-pane :slider-pane
                  :max-value limit
                  :min-value 0
                  :value limit
                  :decimal-places 0
                  :number-of-quanta limit
                  :orientation :horizontal
                  :show-value-p t
                  :value-changed-callback callback))

(defclass output-pane (clim:application-pane
                       clouseau::scroll-position-preserving-mixin)
  ())

(clim:define-application-frame visualiser ()
  ((argument-types :initform nil :accessor argument-types)
   (source :initform *sample-function* :initarg :source :accessor source)
   (limit :initform 50 :accessor limit)
   (peephole-limit :initform 500 :accessor peephole-limit)
   (stage :initform :higher :accessor stage)
   (target :initform :arm :accessor target)
   (ir :initform "" :initarg :ir :accessor ir)
   (unit :initform nil :accessor unit)
   (node-to-analyse :initform nil :accessor node-to-analyse)
   (mumble-log :initform '() :accessor mumble-log))
  (:panes
   (editor :text-editor
           :width 200
           :text-style (clim:make-text-style :fix nil nil)
           :value *sample-function*
           :value-changed-callback (lambda (g v)
                                     (unless (null (source (clim:gadget-client g)))
                                       (setf (source (clim:gadget-client g)) v))))
   (limit (make-limit-slider 50 (lambda (g v) (setf (limit (clim:gadget-client g)) v))))
   (peephole-limit (make-limit-slider 2000 (lambda (g v) (setf (peephole-limit (clim:gadget-client g)) v))))
   (stage :option-pane :items '(:higher :lower :cfg :assembly) :value :higher
          :value-changed-callback (lambda (g v) (setf (stage (clim:gadget-client g)) v)))
   (target :option-pane :items (mt:all-targets) :value :arm
           :value-changed-callback (lambda (g v) (setf (target (clim:gadget-client g)) v)))
   (output :application-pane
           :width 200
           :display-function 'display-ir :end-of-page-action :allow)
   (node-info :application-pane
              :display-function 'display-node)
   (analysis :application-pane
             :width 500 :height 300
             :display-function 'display-analysis)
   (unit-functions :application-pane
                   :display-function 'display-unit-functions)
   (mumble :application-pane
           :display-function 'display-mumbles
           :end-of-line-action :allow
           :end-of-page-action :allow))
  (:layouts
   (default
    (clim:horizontally (:spacing 4)
      (1/3
       (clim:vertically (:spacing 4)
         (1/3 (clim:labelling (:label "Function") (clim:scrolling () editor)))
         (clim:tabling ()
           (list (clim:labelling (:label "Stage")) stage)
           (list (clim:labelling (:label "Target")) target)
           (list (clim:labelling (:label "Limit")) limit)
           (list (clim:labelling (:label "Peeps")) peephole-limit))
         (2/3
          (clim-tab-layout:with-tab-layout ('clim-tab-layout:tab-page)
            ("Node" (clim:scrolling () node-info))
            ("Analysis" (clim:scrolling () analysis))
            ("Compilation Unit" (clim:scrolling () unit-functions))
            ("Mumble" (clim:scrolling () mumble))))))
      (2/3 (clim:labelling (:label "IR") (clim:scrolling () output)))))))

(defun redisplay (frame pane-name)
  (let ((pane (clim:find-pane-named frame pane-name)))
    (unless (null pane)
      (clim:redisplay-frame-pane frame pane :force-p t))))

(defun invalidate (frame)
  (setf (mumble-log frame) '())
  (let ((mt::*mumble*
          (lambda (&rest r)
            (push r (mumble-log frame))))
        (*gensym-counter* 0)
        (start-time (get-internal-run-time)))
    ;; Recompile the method we were handed, either in IR
    ;; or textual form.
    (handler-case
        (mt:compile
         (if (null (source frame))
             (mt::compile-method
              (mt::code-method (ir frame))
              (mt::code-argument-types (ir frame)))
             (mt:parse-method-of
              (source frame)
              :argument-types (argument-types frame)))
         :limit (limit frame)
         :last-peep-limit (peephole-limit frame)
         :stage (stage frame)
         :target (target frame))
      (:no-error (code unit) (setf (ir frame) code (unit frame) unit))
      (error (e) (setf (ir frame) e (unit frame) nil)))
    (funcall mt::*mumble*
             0 "Utena"
             (format nil "Compilation took ~,3F seconds"
                     (/ (- (get-internal-run-time) start-time)
                        internal-time-units-per-second))))
  (setf (node-to-analyse frame) nil)
  (redisplay frame 'output)
  (redisplay frame 'mumble))

(macrolet ((def (accessor)
             `(defmethod (setf ,accessor) :after (new-value (frame visualiser))
                (declare (ignore new-value))
                (invalidate frame))))
  (def limit)
  (def peephole-limit)
  (def stage)
  (def source)
  (def target))

(defvar *app*)
(defun run (&key ir)
  (let ((frame (clim:make-application-frame
                'visualiser
                :ir ir
                :source (if (null ir) *sample-function* nil))))
    (bt:make-thread (lambda () (clim:run-frame-top-level frame)))
    (setf *app* frame)))

(defun display-ir (frame pane)
  (etypecase (ir frame)
    (error
     (clim:with-drawing-options (pane :text-face :italic :ink clim:+red+)
       (let ((*print-circle* t)
             (*print-level* 2))
         (princ (ir frame) pane))))
    (mt::code
        (let ((*selected-test* (lambda (n) (eq n (node-to-analyse frame)))))
          (draw-ir (ir frame) pane
                   :view (ecase (stage frame)
                           ((:higher :lower) :sea-of-nodes)
                           ((:cfg) :cfg)
                           ((:assembly) :assembly)))))))

;;; Presentations
(clim:define-presentation-type inspectable ())
(clim:define-presentation-type analysable () :inherit-from 'inspectable)
(clim:define-presentation-type node () :inherit-from 'analysable)
(clim:define-presentation-type basic-block () :inherit-from 'inspectable)
(clim:define-presentation-type instruction () :inherit-from 'analysable)
(clim:define-presentation-type mumbled-object () :inherit-from 'inspectable)
(clim:define-presentation-type function-info ())

(define-visualiser-command com-set-node-to-analyse ((node 'analysable))
  (setf (node-to-analyse clim:*application-frame*) node)
  (redisplay clim:*application-frame* 'analysis))

(clim:define-presentation-to-command-translator analyse-node
    (analysable com-set-node-to-analyse visualiser :gesture :select)
    (object)
  (list object))

(define-visualiser-command com-inspect ((node 'inspectable))
  (clouseau:inspect node :new-process t))

(clim:define-presentation-to-command-translator inspect-node
    (inspectable com-inspect visualiser :gesture nil)
    (object)
  (list object))

(define-visualiser-command com-replace-node ((old 'node) (new 'node))
  (let* ((frame clim:*application-frame*)
         (mt::*mumble* (lambda (&rest r) (push r (mumble-log frame))))
         (mt::*code* (ir frame)))
    (mt::replace-node old new)
    (when (eq (stage clim:*application-frame*) :higher)
      (mt::with-pass ("Replace" mt::*code*)
        (mt::peephole (ir frame) #'mt::peephole-higher)))))

(clim:define-drag-and-drop-translator replace-node
    (node clim:command node visualiser)
    (object destination-object)
  (if (eq object destination-object)
      `(com-set-node-to-analyse ,object)
      `(com-replace-node ,object ,destination-object)))

(define-visualiser-command com-make-node-unreachable ((node 'node))
  (let ((mt::*code* (ir clim:*application-frame*)))
    (com-replace-node node (mt::unreachable nil))))

(clim:define-presentation-to-command-translator make-node-unreachable
    (node com-make-node-unreachable visualiser :gesture nil)
    (object)
  (list object))

(define-visualiser-command com-invalidate ((node 'node))
  (let ((mt::*code* (ir clim:*application-frame*)))
    (mt::invalidate (list node))))

(clim:define-presentation-to-command-translator invalidate-analyses
    (node com-invalidate visualiser :gesture nil)
    (object)
  (list object))

(clim:define-presentation-type tick ())

(clim:define-presentation-to-command-translator set-optimisation-limit
    (tick com-set-optimisation-limit visualiser :gesture :select)
    (object)
  (list object))

(define-visualiser-command com-set-optimisation-limit ((limit 'tick))
  (setf (clim:gadget-value (clim:find-pane-named clim:*application-frame* 'limit)) limit
        (limit clim:*application-frame*) limit))

(defun render-ps (pathname &key (view :ir) (source t))
  (let ((clim:*application-frame* *app*))
    (with-open-file (ps pathname :direction :output :if-exists :supersede)
      (clim:with-output-to-postscript-stream (stream ps :device-type :eps)
        (when (and source (not (null (source *app*))))
          (clim:with-text-family (stream :fix)
            (write-line (source *app*) stream)))
        (ecase view
          (:ir
           (format stream "~%IR at ~:(~A~):~%" (stage *app*))
           (draw-ir (ir *app*) stream))
          (:analysis
           (display-analysis *app* stream)))))))
    
(defun render-png-from-code (pathname code
                             &rest rest
                             &key
                               (highlight-test (constantly nil))
                               stage target limit last-peep-limit)
  (declare (ignore stage target limit last-peep-limit))
  (let ((ps-name (format nil "~A.ps" pathname))
        (*selected-test* highlight-test))
    (with-open-file (ps ps-name :direction :output :if-exists :supersede)
      (clim:with-output-to-postscript-stream (stream ps :device-type :eps)
        (draw-ir
         (apply #'mt:compile
                (mt:parse-method-of code)
                (alexandria:remove-from-plist rest :highlight-test))
         stream)))
    (sb-ext:process-wait
     (sb-ext:run-program "/usr/bin/convert"
                         `("-density" "72" ,ps-name ,pathname)))))
