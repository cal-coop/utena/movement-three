(in-package :movement-three-visualiser)

(clim:define-application-frame compilation-unit-visualiser ()
  ((unit :initarg :unit :reader unit)
   (selected-function :initform nil :accessor selected-function))
  (:panes
   (output :application-pane
           :display-function 'display-function)
   (view :option-pane :items '(:sea-of-nodes :cfg :assembly) :value :assembly
                      :value-changed-callback (lambda (g v)
                                                (declare (ignore g v))
                                                (redisplay clim:*application-frame* 'output)))
   (function-selector output-pane
                      :display-function 'display-unit-functions))
  (:layouts
   (default
    (clim:horizontally (:spacing 4)
      (1/3 (clim:vertically () view (clim:+fill+ (clim:scrolling () function-selector))))
      (2/3 (clim:scrolling () output))))))

(defun view-compilation-unit (unit)
  (let ((frame (clim:make-application-frame 'compilation-unit-visualiser
                                            :width 1000
                                            :height 600
                                            :unit unit)))
    (bt:make-thread (lambda () (clim:run-frame-top-level frame)))
    (setf *app* frame)))

(defun display-function (frame pane)
  (unless (null (selected-function frame))
    (if (null (mt:fun-code (selected-function frame)))
        (write-string "No code!" pane)
        (draw-ir (mt:fun-code (selected-function frame)) pane
                 :view (clim:gadget-value (clim:find-pane-named frame 'view))))))

(define-compilation-unit-visualiser-command com-select-function ((function 'function-info))
  (setf (selected-function clim:*application-frame*) function))

(clim:define-presentation-to-command-translator select-function
    (function-info com-select-function compilation-unit-visualiser :gesture :select)
    (object)
  (list object))

(define-compilation-unit-visualiser-command com-view-function ((function 'function-info))
  (run :ir (mt:fun-code function)))

(clim:define-presentation-to-command-translator view-function
    (function-info com-view-function compilation-unit-visualiser :gesture nil)
    (object)
  (list object))
