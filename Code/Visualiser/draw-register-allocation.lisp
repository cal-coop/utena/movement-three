(in-package :movement-three-visualiser)

(defun draw-live-intervals-for-virtual-register (node pane frame)
  (let* ((intervals (mt::cfg-intervals
                     (mt::code-cfg (ir frame))))
         (intervals (remove node intervals
                            :test-not #'eq
                            :key #'mt::interval-vreg)))
    (dolist (i intervals)
      (format pane "~&[~D, ~D]"
              (mt::interval-start i)
              (mt::interval-end i)))))

(defun virtual-register-position (vreg)
  (etypecase vreg
    (mt:register (mt:register-index vreg))
    (mt::virtual-register (+ 32 (mt::virtual-register-name vreg)))))

(defvar *diagram-scale* 1)
(defun draw-all-live-intervals (cfg selected pane)
  (clim:with-room-for-graphics (pane :first-quadrant nil)
    (clim:with-scaling (pane *diagram-scale*)
      ;; Draw basic block boundaries
      (loop with size = (+ 33
                           (reduce #'max (alexandria:hash-table-keys (mt::cfg-register-allocation cfg))
                                   :key #'mt::virtual-register-name
                                   :initial-value 0))
            for b across (mt::cfg-blocks-by-schedule cfg)
            for end = (reduce #'max (mt::basic-block-instructions b)
                              :key (lambda (i) (or (mt::instruction-index i) 0)))
            do (clim:draw-line* pane 0 end size end
                                :ink (if (mt::basic-block-cold-p b) clim:+light-blue+ clim:+gray+)))
      ;; Draw intervals
      (dolist (interval (mt::cfg-intervals cfg))
        (let* ((vreg (mt::interval-vreg interval))
               (x (virtual-register-position vreg)))
          (clim:draw-rectangle* pane
                                x (mt::interval-start interval)
                                (1+ x) (1+ (mt::interval-end interval))
                                :ink (cond
                                       ((typep vreg 'mt:register) clim:+green3+)
                                       ((eq vreg selected) clim:+blue+)
                                       (t clim:+black+))))))))

(defvar *ink-base* 6)
(defun ink-for-vreg-name (name)
  (clim:make-rgb-color
   (/ (mod name *ink-base*) *ink-base*)
   (/ (mod (floor name *ink-base*) *ink-base*) *ink-base*)
   (/ (mod (floor name (expt *ink-base* 2)) *ink-base*) *ink-base*)))

(defun draw-register-allocation (cfg pane)
  (clim:with-room-for-graphics (pane :first-quadrant nil)
    (clim:with-scaling (pane *diagram-scale*)
      (let ((size (reduce #'max (mt::cfg-intervals cfg) :key #'mt::interval-end)))
        (clim:draw-line* pane
                         mt:+register-count+ 0
                         mt:+register-count+ size
                         :ink clim:+gray+))
      (dolist (interval (mt::cfg-intervals cfg))
        (let ((vreg (mt::interval-vreg interval)))
          (when (typep vreg 'mt::virtual-register)
            (let* ((reg (gethash vreg (mt::cfg-register-allocation cfg)))
                   (ink (ink-for-vreg-name (mt::virtual-register-name vreg)))
                   (x
                     (etypecase reg
                       (mt:register (mt:register-index reg))
                       (mt::stack-location
                        (+ mt:+register-count+ (mt:stack-location-index reg))))))
              (clim:draw-rectangle* pane
                                    x (mt::interval-start interval)
                                    (1+ x) (1+ (mt::interval-end interval))
                                    :ink ink))))))))
