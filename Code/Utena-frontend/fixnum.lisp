(in-package :movement-three)

(defstruct fixnum-primop-method kind name)

(defvar *utena-to-cl-primops*
  (let ((table (make-hash-table)))
    (flet ((add (kind alist)
             (loop for (utena cl) in alist
                   do (setf (gethash utena table) (make-fixnum-primop-method :kind kind :name cl))))
           (add-sourcerer (source)
             (let* ((method (sourcerer:compile-method source))
                    (name (haaden-two:name (haaden-two:info method))))
               (setf (gethash name table) (sourcerer:compile-method source)))))
      (add :binary '((utena::+ +) (utena::- -) (utena::* *) (utena::/ floor)
                     (utena::bit-and logand) (utena::bit-or logior) (utena::bit-xor logxor)))
      (add :unary '((utena::bit-not lognot)))
      (add :test '((utena::< <) (utena::<= <=) (utena::= =)))
      (add :flipped-test '((utena::>= <) (utena::> <=) (utena::/= =)))
      (add-sourcerer "(define self :self)")
      (add-sourcerer "(define (abs) (if (< 0) (-: 0 self) self))")
      (add-sourcerer "(define (mod divisor) (- (* (/ divisor))))")
      table)))

(defmethod map-methods (function (type (eql +fixnum+)))
  (maphash function *utena-to-cl-primops*))

(defmethod lookup-method (name source (type (eql '+fixnum+)) receiver)
  (declare (ignore source))
  (let ((method (gethash name *utena-to-cl-primops*)))
    (if (null method) :does-not-understand (values method receiver))))

(defmethod method-size ((m fixnum-primop-method)) 0)
(defmethod method-argument-count ((m fixnum-primop-method))
  (ecase (fixnum-primop-method-kind m) ((:binary :flipped-test :test) 2) ((:unary) 1)))

(defmethod compile-method-body ((m fixnum-primop-method) control memory arg-count get-arg)
  (ecase (fixnum-primop-method-kind m)
    ((:binary :flipped-test :test)
     (assert (= arg-count 2))
     (let* ((test (type-test control '+fixnum+ (funcall get-arg 1)))
            (fail (crash (project nil :no test) :argument-not-fixnum)))
       (add-inputs (code-stop *code*) fail)
       (ecase (fixnum-primop-method-kind m)
         (:binary
          (let ((success (fixnum-binary-op nil (fixnum-primop-method-name m)
                                           (funcall get-arg 0) (project nil :value test))))
            (values (project nil :value success) (project nil :yes test) memory)))
         ;; There's two ways to de-canonicalise tests unfortunately. One is to not flip
         ;; > to <= and >= to < and such; another is to flip both the LHS/RHS and the
         ;; control outputs.
         ((:flipped-test :test)
          (multiple-value-bind (lhs rhs)
              (if (eq (fixnum-primop-method-kind m) :flipped-test)
                  (values (project nil :value test) (funcall get-arg 0))
                  (values (funcall get-arg 0) (project nil :value test)))
          (let* ((success (fixnum-test (project nil :yes test) (fixnum-primop-method-name m)
                                       lhs rhs))
                 (join (region nil (project nil :yes success) (project nil :no success))))
            (values (phi join +top+ (literal nil haaden-two:*true*) (literal nil haaden-two:*false*))
                    join memory)))))))
      (:unary
       (assert (= arg-count 1))
       (let ((op (fixnum-unary-op nil (fixnum-primop-method-name m) (funcall get-arg 0))))
         (values (project nil :value op) control memory)))))
