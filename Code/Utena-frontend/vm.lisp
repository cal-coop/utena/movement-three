(in-package :movement-three)

(defstruct vm-primop-method name argument-count compiler)

(defvar *vm-primops*
  (let ((table (make-hash-table)))
    (flet ((add (name argument-count compiler)
             (setf (gethash name table)
                   (make-vm-primop-method
                    :name name
                    :argument-count argument-count
                    :compiler compiler))))
      (add 'utena::eq?
           2
           (lambda (control memory get-arg)
             (let* ((test (eq-test control (funcall get-arg 0) (funcall get-arg 1)))
                    (merge (region nil (project nil :yes test) (project nil :no test))))
               (values (phi merge +top+ (literal nil haaden-two:*true*) (literal nil haaden-two:*false*))
                       merge memory)))))
    table))

(defvar *vm-type* (wider-type-of haaden-two:*vm*))
(defvar *vm-class-definition* (haaden-two:class-definition (haaden-two:class haaden-two:*vm*)))
(defmethod map-methods (function (type (eql *vm-type*)))
  (maphash function *vm-primops*))
(defmethod map-methods (function (type (eql *vm-class-definition*)))
  (maphash function *vm-primops*))

(defmethod compile-method-body ((m vm-primop-method) control memory arg-count get-arg)
  (assert (= (vm-primop-method-argument-count m) arg-count))
  (funcall (vm-primop-method-compiler m) control memory get-arg))

(defmethod method-argument-count ((m vm-primop-method)) (vm-primop-method-argument-count m))
