(in-package :movement-three)

;; The type of a Utena instance, defined by a sequence of enclosing classes.
(defclass utena-type ()
  ((definition :initarg :definition :reader class-definition)
   (enclosing-type :initarg :enclosing-type :reader enclosing-type)))
(defmethod print-object ((type utena-type) stream)
  (print-unreadable-object (type stream :type t :identity t)
    (princ (haaden-two::name (class-definition type)) stream)))

(defclass utena-instance (utena-type) ())
(defclass utena-class (utena-type) ())

(defmethod lookup-method (name (source utena-instance) type receiver)
  (let* ((definition (class-definition source))
         (method (gethash name (haaden-two::message-table definition))))
    (cond
      ((not (null method)) (values method receiver))
      ((null (enclosing-type source)) (values :does-not-understand nil))
      (t
       (lookup-method name
                      (type-source (enclosing-type source))
                      (enclosing-type source)
                      (enclosing-object receiver))))))

(defmethod lookup-method (name (source utena-class) type receiver)
  (let ((definition (class-definition source)))
    (if (eq name (haaden-two::constructor-name definition))
        (values (haaden-two::constructor-method definition) receiver)
        nil)))

(defmethod method-return-type ((method haaden-two::constructor-method) receiver arguments)
  (trivia:match (widen-away-exactly receiver)
    ((record :source (and class (type utena-class)))
     (instance-type (class-definition class) (enclosing-type class)))
    (_ +top+)))

(defvar *mutable-definitions* (make-hash-table))

(defun prototype-class-definition (class)
  "Make a mutable version of an immutable class definition."
  (assert (not (haaden-two::mutable-p class)))
  (or (gethash class *mutable-definitions*)
      (setf (gethash class *mutable-definitions*)
            (make-instance 'haaden-two:class-definition
              :slots (haaden-two::slots class)
              :name (list :prototype (haaden-two:name class))
              :mutable t
              :methods (haaden-two::methods class)
              :constructor-name (haaden-two::constructor-name class)
              :constructor-arguments (haaden-two::constructor-arguments class)
              :source (haaden-two::source class)))))

(defvar *instance-types* (make-hash-table :test 'equal))

(defun instance-type (definition enclosing)
  (when (eq enclosing '+bottom+) (break "really now"))
  (labels ((compute-slot-definition (slot) (slot-definition slot +top+))
           (partition-slots ()
             (let ((enc-slot
                     (if (null enclosing) '() (list (slot-definition 'enclosing-object enclosing))))
                   (slots (haaden-two::slots definition)))
               (if (haaden-two::mutable-p definition)
                   (values enc-slot (mapcar #'compute-slot-definition slots))
                   (values (append enc-slot (mapcar #'compute-slot-definition slots)) '()))))
           (compute-instance-type ()
             (multiple-value-call
                 #'record
               (make-instance 'utena-instance
                 :definition definition :enclosing-type enclosing)
               (partition-slots))))
    (or (gethash (cons definition enclosing) *instance-types*)
        (setf (gethash (cons definition enclosing) *instance-types*)
              (compute-instance-type)))))

(defun activation-record-types (method receiver-type)
  (let* ((definition (haaden-two::ar-class-definition method))
         (recursive-p (haaden-two::recursive-p method))
         (enclosing-type
           (if recursive-p receiver-type (enclosing-type (record-source receiver-type)))))
    (values (instance-type definition enclosing-type)
            (class-type definition enclosing-type))))

(defvar *class-types* (make-hash-table :test 'equal))

(defun class-type (definition enclosing)
  (or (gethash (cons definition enclosing) *class-types*)
      (setf (gethash (cons definition enclosing) *class-types*)
            (record
             (make-instance 'utena-class
               :definition definition :enclosing-type enclosing)
             (if (null enclosing)
                 '()
                 (list (slot-definition 'enclosing-object enclosing)))
             '()))))

(defun instance<-class-type (class-type &key (prototype nil))
  (let ((def (class-definition (record-source class-type))))
    (instance-type (if (and prototype (not (haaden-two::mutable-p def)))
                       (prototype-class-definition def)
                       def)
                   (enclosing-type (record-source class-type)))))

(defmethod wider-type-of ((instance haaden-two::instance))
  (let* ((class (haaden-two::class instance))
         (enclosing (haaden-two::enclosing-object class)))
    (instance-type (haaden-two::class-definition class)
                   (if (null enclosing) nil (wider-type-of enclosing)))))
(defmethod wider-type-of ((class haaden-two:class))
  (class-type (haaden-two:class-definition class)
              (if (null (haaden-two::enclosing-object class))
                  nil
                  (wider-type-of (haaden-two::enclosing-object class)))))
