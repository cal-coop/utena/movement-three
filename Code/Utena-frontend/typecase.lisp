(in-package :movement-three)

(defun utena-typecaser (name)
  (case name
    (utena::if
     (list (exactly haaden-two::*true*)
           (exactly haaden-two::*false*)))
    ((utena::+ utena::- utena::* utena::/
      utena::mod utena::abs
      utena::bit-and utena::bit-not utena::bit-or utena::bit-xor
      utena::< utena::<= utena::= utena::/= utena::>= utena::>)
     (list +fixnum+))
    (otherwise '())))

(pushnew 'utena-typecaser *typecasers*)
