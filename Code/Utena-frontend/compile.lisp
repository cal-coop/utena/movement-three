(in-package :movement-three)

(defvar *trivial-method-size* 5
  "The number of instructions to not count in a code method, as such a small method is trivial.")

(defvar *environments* '())
(defvar *environment-type*)
(defvar *method*)
(defvar *pc*)

(defmethod method-argument-count ((m haaden-two:method))
  ;; 1+ for the receiver.
  (1+ (length (haaden-two::arguments m))))

(defun parse-method-of (source &key (name 'utena::value) argument-types)
  ;; Turn Utena/Sourcerer warnings into mumbles.
  (let ((*ticks* 0)
        (*pass* "Utena"))
    (handler-bind ((warning (lambda (e)
                              (mumble (princ-to-string e))
                              (muffle-warning e))))
      (let* ((result (sourcerer:evaluate source))
             (method (haaden-two::lookup-method result name :public)))
        (when (null method)
          (error "~A does not understand the message ~A." result name))
        (unless (null argument-types)
          (assert (= (length (haaden-two::arguments method)) (length argument-types))))
        (compile-method
         method
         (cons (widen-away-exactly (exactly result))
               (or argument-types
                   (loop for a in (haaden-two::arguments method) collect +top+))))))))

;;; Code method compilation
(defgeneric compile-instruction (instruction data-stack environment control memory)
  (:documentation "Compile a Utena instruction, yielding a new data stack, control and memory."))

(defmethod compile-instruction ((i haaden-two:literal) ds e c m)
  (declare (ignore e))
  (values (cons (literal nil (haaden-two::value i)) ds) c m))

(defmethod compile-instruction ((i haaden-two:drop) ds e c m)
  (declare (ignore e))
  (assert (not (endp ds)))
  (values (rest ds) c m))

(defmethod compile-instruction ((i haaden-two:make-class) ds e c m)
  (values
   (cons (new* nil (class-type (haaden-two::class-definition i) (result-for type e)) m e) ds)
   c m))

(defun compile-send (arguments receiver name access tail-p control memory stack)
  ;; When inlining a non-tail send would produce a tail send, we cannot
  ;; really emit a tail send as the sender of the non-tail send has to continue
  ;; execution. Instead we emit a "suppressed" tail send which will never perform
  ;; a tail call with the concrete machine, but will behave like a tail send with inlining.
  ;; Inlining a suppressed tail send may produce more suppressed tail sends too.
  ;; (And this NIL / :SUPPRESSED / T ternary kinda stinks, at least from the perspective
  ;; of adding it to code expecting a NIL / T binary.)
  (let* ((emit-tail-p (cond
                        ((not tail-p) nil)
                        ((member *emit-tail-sends* '(nil :suppressed)) :suppressed)
                        (t t)))
         ;; If we do emit a tail send, there is no way for the native stack to
         ;; involve this code again, so there's no need for a VM state here.
         ;; But we do need to make the right VM state when we aren't
         ;; actually going to tail send.
         (state (if (eq emit-tail-p t)
                    (vm-state nil *inline-depth*)
                    (apply #'vm-state nil *inline-depth*
                           (if tail-p (rest *environments*) *environments*))))
         (callers (if tail-p *callers* (acons *method* *pc* *callers*)))
         (op (apply #'unknown-send control access emit-tail-p callers
                    state memory name receiver arguments)))
    (cond
      ((eq emit-tail-p t)
       (add-inputs (code-stop *code*) op)
       (values (list (unreachable nil)) (unreachable nil) nil))
      (t
       (values (cons (project nil :value op) stack) (project nil :control op) (project nil :memory op))))))

(defun enclosing-object (object) (read-slot nil 0 (immutable) object))

(defmethod compile-instruction ((i haaden-two:target-send) ds e c m)
  (declare (ignore e))
  (compile-send (reverse (loop repeat (haaden-two::argument-count i) collect (pop ds)))
                (pop ds) (pop ds) :public (haaden-two::tail-p i) c m ds))

(defmethod compile-instruction ((i haaden-two:self-send) ds e c m)
  (let ((arguments (reverse (loop repeat (haaden-two::argument-count i) collect (pop ds))))
        (name (pop ds)))
    ;; Inline accessors early, to avoid making rubbish nodes.
    (trivia:match name
      ((literal :value name)
       (multiple-value-bind (method receiver)
           (lookup-method name (type-source *environment-type*) *environment-type* e)
         (when (typep method '(or haaden-two::accessor-method haaden-two::self-method))
           (multiple-value-bind (value control memory)
               (compile-method-body method c m
                                    (1+ (length arguments))
                                    (lambda (n) (values (nth n (cons receiver arguments))
                                                        (if (zerop n) (result-for type receiver) +top+))))
             (return-from compile-instruction (values (cons value ds) control memory)))))))
    (compile-send arguments e name :private (haaden-two::tail-p i) c m ds)))

(defun method-prologue (method control memory get-arg)
  (multiple-value-bind (receiver receiver-type) (funcall get-arg 0)
    (let* ((control (method-control control))
           (arguments (loop for n from 1
                            for arg in (haaden-two::arguments method)
                            collect (multiple-value-bind (value type)
                                        (funcall get-arg n)
                                      (method-argument control type n value))))
           (recursive-p (haaden-two::recursive-p method)))
      (multiple-value-bind (instance-type class-type)
          (activation-record-types method receiver-type)
        ;; XX: nasty to use (IMMUTABLE) here, we don't use memory
        ;; for New of immutable though
        (let* ((receiver (method-argument control receiver-type 0 receiver))
               (enclosing (if recursive-p receiver (enclosing-object receiver)))
               (ar (apply #'new*
                          nil
                          instance-type
                          (immutable) enclosing arguments))
               (start (apply #'method-start
                             control
                             method *callers*
                             (apply #'vm-state nil *inline-depth* ar *environments*)
                             (method-argument control +memory+ :memory memory)
                             receiver arguments)))
          (values (project nil :control start) ar (project nil :memory start)))))))

(define-meter *code-methods-compiled* higher code-methods-compiled)
(defmethod compile-method-body ((m haaden-two:code-method) start memory args get-arg)
  (assert (plusp args))
  (meter-tick *code-methods-compiled*)
  (multiple-value-bind (control env memory) (method-prologue m start memory get-arg)
    (let ((stack '())
          (*environment-type* (result-for type env))
          (*environments* (cons env *environments*))
          (*method* m))
      (loop for i in (haaden-two::instructions m)
            for *pc* from 0
            do (multiple-value-setq (stack control memory) (compile-instruction i stack env control memory)))
      (values (first stack) control memory))))

(defmethod method-size ((m haaden-two:code-method))
  (trivia:match (haaden-two::instructions m)
    ;; Constants are also negative-cost to inline.
    ((list (haaden-two:literal)) 0)
    (more
     (max 3
          (- (loop for i in more
                   sum (etypecase i
                         ((or haaden-two:drop haaden-two:literal) 0)
                         (haaden-two:self-send 1)
                         (haaden-two:make-class 1)
                         (haaden-two:target-send 2)))
             *trivial-method-size*)))))

;;; Accessor compilation
(defmethod compile-method-body ((method haaden-two::accessor-method) start memory args get-arg)
  (assert (plusp args))
  (multiple-value-bind (receiver receiver-type) (funcall get-arg 0)
    (let* ((slot (haaden-two::slot-definition method))
           (index (slot-index slot receiver-type)))
      (ecase (haaden-two::action method)
        (:reader (values
                  (read-slot nil index (split-memory receiver-type index memory) receiver)
                  start memory))
        (:writer
         (let ((value (funcall get-arg 1)))
           (values value start
                   (with-mutated-split (split receiver-type index memory)
                     (write-slot start index split receiver value)))))))))

(defmethod method-size ((m haaden-two::accessor-method)) 0)

(defmethod compile-method-body ((method haaden-two:self-method) start memory args get-arg)
  (assert (= args 1))
  (values (funcall get-arg 0) start memory))

(defmethod method-size ((m haaden-two::self-method)) 0)

;;; Constructor compilation
(defun clone-prototype (class-layout memory instance prototype-type)
  (let* ((layout (instance<-class-type class-layout)))
    (apply #'new* nil layout memory
           (loop for index from 0
                 for nil in (record-immutable-slots layout)
                 collect (read-slot nil index (split-memory prototype-type index memory) instance)))))

(defmethod compile-method-body ((method haaden-two::constructor-method) start memory args get-arg)
  (assert (plusp args))
  (multiple-value-bind (class layout) (funcall get-arg 0)
    ;; Pretty easy when there's no slots.
    (let ((def (class-definition (record-source layout))))
      (when (endp (haaden-two::slots def))
        (multiple-value-bind (instance memory)
            (new* (haaden-two::mutable-p def) (instance<-class-type layout)
                  memory (enclosing-object class))
          (return-from compile-method-body (values instance start memory)))))
    (let* ((source (record-source layout))
           (def (class-definition source))
           (type (instance<-class-type layout :prototype t))
           (control start)
           (arguments (loop for n from 1
                            for nil in (haaden-two::constructor-arguments def)
                            collect (funcall get-arg n))))
      (multiple-value-bind (instance memory) (new* t type memory (enclosing-object class))
        ;; Put down unbound markers.
        (dotimes (i (length (record-mutable-slots type)))
          (setf memory
                (with-mutated-split (split type (1+ i) memory)
                  (write-slot start (1+ i) split instance (literal nil '+unbound+)))))
        ;; Fence between the unbound writes and user writes.
        (setf instance (construct-fence nil memory instance))
        (loop for index from 1
              for slot in (record-mutable-slots type)
              for send = (apply #'known-send
                                control
                                (haaden-two::init-method (slot-definition-name slot))
                                nil
                                (acons method index *callers*)
                                nil
                                (apply #'vm-state nil *inline-depth* *environments*)
                                memory instance arguments)
              do (setf control (project nil :control send)
                       memory (project nil :memory send)
                       memory (with-mutated-split (split type index memory)
                                (write-slot start index split instance (project nil :value send)))))
        (if (haaden-two::mutable-p def)
            (values instance control memory)
            (values (clone-prototype layout memory instance type) control memory))))))

(defmethod method-size ((m haaden-two::constructor-method))
  (length (haaden-two::slots (haaden-two::class-definition m))))
