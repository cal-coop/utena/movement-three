(defpackage :movement-three.arm
  (:export ;; Instructions
           #:mov #:add #:sub #:mul #:div
           #:b #:cmp #:blt #:ble #:beq #:bne #:bge #:bgt
           #:bl #:ret
           #:and #:orr #:eor #:not #:asr #:lsl #:tbz #:tbnz
           #:ldr #:ldp #:str #:stp
           ;; Pseudo-instructions
           #:alloc #:test #:write-barrier #:branch-instance-of #:branch-not-instance-of
           ;; Registers
           #:*temporaries* #:*gprs*
           #:*sp* #:*lr* #:*fp* #:*tls*))

(defpackage :movement-three.cfg
  (:export #:basic-block #:make-basic-block
           #:basic-block-predecessors #:basic-block-successors
           #:basic-block-index #:basic-block-instructions
           #:control-flow-graph #:cfg-blocks-by-schedule))

(defpackage :movement-three.types
  (:export ;; Base types
           #:+top+ #:+bottom+ #:+fixnum+
           #:+control+ #:+memory+ #:+state+
           #:*tls-type*
           ;; Aggregate types
           #:slot-definition #:slot-definition-name #:slot-definition-type
           #:record #:record-source #:record-immutable-slots #:record-mutable-slots
           #:record-bytes #:record-slot-count #:slot-index #:slot-by-index
           #:tuple #:%tuple #:constant-tuple #:tuple-lookup #:tuple-keys #:tuple-values
           #:union-type #:union-type-types
           #:exactly #:exactly-value
           ;; Type arithmetic
           #:type-union #:type-intersect #:types-disjoint-p
           #:type= #:type<= #:type-difference #:wider-type-of
           ;; Sources
           #:utena-type #:utena-class #:utena-instance
           #:class-definition #:enclosing-type))

(defpackage :movement-three
  (:use :cl :movement-three.cfg :movement-three.types)
  (:shadow cl:compile cl:with-compilation-unit)
  (:export ;; Frontend
           #:parse-method-of #:compile #:compile-all-methods
           #:code-argument-types #:code-return-type
           ;; Analyses
           #:result-for #:type #:dominator #:needs-safepoint-p
           ;; Meters
           #:with-meters #:print-meters #:reset-meters
           #:define-meter #:define-time-meter #:define-histogram-meter
           #:meter-tick #:with-timing #:histogram-add
           ;; Backend
           #:instruction #:inst #:instruction-name #:instruction-arguments
           #:immediate #:immediate-value
           #:register #:register-index #:register-name #:+register-count+
           #:stack-location #:stack-location-index
           #:all-targets #:define-backend
           #:code-cfg
           ;; Compilation unit
           #:*compilation-unit* #:make-compilation-unit
           #:*code*
           #:map-functions #:find-function
           #:function-info #:fun-argument-types #:fun-code #:fun-method #:fun-return-type)
  (:local-nicknames (#:arm #:movement-three.arm)))

(defpackage :movement-three.cl-backend
  (:use :cl)
  (:local-nicknames
   (#:mt #:movement-three)
   (#:type #:movement-three.types)))
