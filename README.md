# [Movement Three](https://www.youtube.com/watch?v=JE1H5FymBaw)

This is a batch compiler for Utena which is a successor to
[haaden-two-son](https://gitlab.com/cal-coop/utena/haaden-two-son).

This borrows the "frontend" from [Hååden Two](https://gitlab.com/cal-coop/utena/haaden-two)
which needs to be installed to use Movement Three. 
## Things to do

Start the IR visualiser:

```lisp
(ql:quickload :movement-three-visualiser)
(movement-three-visualiser:run)
```

Gander at meters:

```lisp
(movement-three:with-meters ()
  (movement-three:optimise (movement-three:compile-method-of "...")))
```

## Thanks

Influences may or may not be seen from
[Cliff Click's AA](https://github.com/cliffclick/aa) and
[C2](https://github.com/openjdk/jdk/tree/master/src/hotspot/share/opto),
[Craig Chambers's Self-91 compiler](https://www.cs.tufts.edu/~nr/cs257/archive/craig-chambers/thesis.pdf),
[demanded abstract interpretation](https://arxiv.org/pdf/2104.01270.pdf),
[Jan Moringen's SBCL IR visualiser](https://github.com/scymtym/sbcl-ir-visualizer),
[SeaOfNodes/Simple](https://github.com/SeaOfNodes/Simple),
and [SICL](https://github.com/robert-strandh/SICL).
